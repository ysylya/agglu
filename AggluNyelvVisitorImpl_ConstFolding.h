#ifndef AGGLUNYELV_AGGLUNYELVVISITORIMPL_CONSTFOLDING_H
#define AGGLUNYELV_AGGLUNYELVVISITORIMPL_CONSTFOLDING_H

#include "AggluNyelvVisitorImpl.h"

namespace agglunyelv
{
    
    class AggluNyelvVisitorImpl_ConstFolding : public AggluNyelvVisitorImpl
    {
    public:
        explicit AggluNyelvVisitorImpl_ConstFolding(antlr4::TokenStream * tokens);
        
        virtual std::any visitProg(AggluNyelvParser::ProgContext * ctx) override;
//
//        virtual std::any visitVariableDefinition(AggluNyelvParser::VariableDefinitionContext * ctx) override;
//        virtual std::any visitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext *ctx) override;
//        virtual std::any visitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext * ctx) override;
//        virtual std::any visitVariableAssignment(AggluNyelvParser::VariableAssignmentContext * ctx) override;
//
//        virtual std::any visitVariableValue(AggluNyelvParser::VariableValueContext * ctx) override;
//
//        virtual std::any visitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext * ctx) override;
//        virtual std::any visitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext * ctx) override;
//
        virtual std::any visitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext * ctx) override;
        virtual std::any visitExprGrpMul(AggluNyelvParser::ExprGrpMulContext * ctx) override;
        virtual std::any visitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext * ctx) override;
        virtual std::any visitExprGrpInt(AggluNyelvParser::ExprGrpIntContext * ctx) override;
//        virtual std::any visitExprGrpFun(AggluNyelvParser::ExprGrpFunContext * ctx) override;
//        virtual std::any visitExprGrpArr(AggluNyelvParser::ExprGrpArrContext * ctx) override;
//        virtual std::any visitExprGrpSym(AggluNyelvParser::ExprGrpSymContext * ctx) override;
        virtual std::any visitExprGrpPar(AggluNyelvParser::ExprGrpParContext * ctx) override;
//
//        virtual std::any visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * ctx) override;
//        virtual std::any visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * ctx) override;
//        virtual std::any visitFunctionParameters(AggluNyelvParser::FunctionParametersContext * ctx) override;
//        virtual std::any visitFunctionReturn(AggluNyelvParser::FunctionReturnContext * ctx) override;
//        virtual std::any visitFunctionBlock(AggluNyelvParser::FunctionBlockContext * ctx) override;
//
//        virtual std::any visitFunctionArgument(AggluNyelvParser::FunctionArgumentContext * ctx) override;
//        virtual std::any visitFunctionCall(AggluNyelvParser::FunctionCallContext * ctx) override;
//
//        virtual std::any visitStatement(AggluNyelvParser::StatementContext * ctx) override;
//
//        virtual std::any visitBlockPart(AggluNyelvParser::BlockPartContext * ctx) override;
//
//        virtual std::any visitBlock(AggluNyelvParser::BlockContext * ctx) override;
    
    private:
        antlr4::TokenStreamRewriter rewriter;
    };
    
} // agglunyelv

#endif //AGGLUNYELV_AGGLUNYELVVISITORIMPL_CONSTFOLDING_H
