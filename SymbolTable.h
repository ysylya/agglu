#ifndef AGGLUNYELV_SYMBOLTABLE_H
#define AGGLUNYELV_SYMBOLTABLE_H

#include <string>
#include <vector>

namespace agglunyelv
{
    
    struct Symbol
    {
        const std::string name;
        const std::string type;
        const bool is_mutable { true };
        
        const int arr_size { 1 };
    
        const int num_of_params { 0 };
    };
    
    class Scope
    {
    public:
        int size() const;
        // Returns false and doesn't push
        // if the scope already has a symbol named like the argument
        [[nodiscard]] bool push(const Symbol & symbol);
//        bool find(const Symbol & symbol);
        Symbol * findByName(const std::string & name);
        void pop();
        const Symbol * top() const;
        Symbol * top();
        
        int getOffsetInCurrentScope(const Symbol * const symbol, const int idx_agglu = 1) const;
    
        void print(const size_t indent = 0) const;
    
        void setReachableStatus(const bool is_reachable);
        [[nodiscard]] bool isReachable() const;

    private:
        // Using std::vector as stack so the conversion to assembly will be easier
        // since vector elements can be indexed,
        // which is relatively similar to how a stack ptr is offseted on value reading.
        std::vector<Symbol> m_stack;
        
        bool m_reachable { true };
    };
    
    class SymbolTable
    {
    public:
        SymbolTable();
        
        void enterScope();
        void exitScope();
    
        // Returns false and doesn't push
        // if the current scope already has a symbol named like the argument
        [[nodiscard]] bool pushToCurrentScope(const Symbol & symbol);
//        bool find(const Symbol & symbol);
        Symbol * findByName(const std::string & name);
        Symbol * findByNameInCurrentScope(const std::string & name);
//        void pop();
        const Symbol * top() const;
        Symbol * top();
        
        // Returns n >= 0, if the parameter symbol is in the current scope.
        // In that case, n means the offset from the current scope's stack bottom
        // (i.e. how many symbols are "below" it).
        // If the symbol is not found in the current scope, it returns -1.
        int getOffsetInCurrentScope(const Symbol * const symbol) const;
        int getOffset(const Symbol * const symbol, const int idx_agglu = 1) const;

        void print() const;
    
        const int getNumOfScopelocals();
        
        const Symbol * topFromParentScope();
        
        void flagParentScopeUnreachable();
        void flagParentScopeReachable();
        
    private:
        std::vector<Scope> m_stack_scopes;
    };
    
} // agglunyelv

#endif //AGGLUNYELV_SYMBOLTABLE_H
