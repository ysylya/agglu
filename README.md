# A programming language - based on agglutination

### Intro

Developing new programming languages is still demanded even today, since new needs, trends, and ideas are constantly appearing.

To demonstrate this, it is enough to think of the languages emerged in the last 10 years, such as Julia or Rust. Their role is ever increasing, the popularity of the former is rapidly growing among researchers, while the latter made its way into the Linux kernel (which even C++ could not achieve). Other than these, there are a lot of so-called domain-specific languages (DSLs) created which are optimized for a very specific set of problems, their structure is built with the logic of the domain in mind, therefore they have a high level of expressiveness in their respective fields.

One of the common properties among the most widely used programming languages is that they are all based on English, their vocabulary and grammar too. There are a lot of advantages regarding this fact, since the structure of English is perfectly suitable for linguistic analysis and processing, therefore it is a great basis for a common language understood both by humans and computers as well. On the other hand, a disadvantage could be for example that little children who are not native English speakers and are new to programming, might experience unnecessary extra difficulties in the process of studying.

Hungarian language is one of the so-called agglutinating (gluing) languages, and its logical structure is quite different from English: while the former has standalone words for modifying the meaning of others, in the case of the latter, they are connecting to these other words in a form of suffixes. How suitable would a human language based on those kind of rules be for creating a programming language?

In this project, I am exploring the ways and possibilities of creating a programming language based on agglutination.

### Code sample

```
rajzolj_haromszoget!(magasság {egész, állandó}) :=
{
    minden szélesség-re {egész, változó} 1-től magasság-ig 1-esével
    {
        minden szóköz_i-re {egész, változó} 1-től magasság-szélesség-ig 1-esével
        {
            alap.irj_ki_szoveget!(" ");
        }

        minden csillag-ra {egész, változó} 1-től szélesség*2-1-ig 1-esével
        {
            alap.irj_ki_szoveget!("*");
        }

        alap.irj_ki_szoveget!("\n");
    }
}
```

### Output of rajzolj_haromszoget!(10)

```

         *
        ***
       *****
      *******
     *********
    ***********
   *************
  ***************
 *****************
*******************
```

