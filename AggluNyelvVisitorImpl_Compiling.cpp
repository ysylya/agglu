#include "AggluNyelvVisitorImpl_Compiling.h"

namespace agglunyelv
{
    AggluNyelvVisitorImpl_Compiling::AggluNyelvVisitorImpl_Compiling(const SymbolTable & symbol_table_every_function)
        : symbol_table_every_function { symbol_table_every_function }
    {
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitProg(AggluNyelvParser::ProgContext * ctx)
    {
        std::string result;
        
        for (antlr4::tree::ParseTree * child : ctx->children)
        {
            result += std::any_cast<std::string>(visit(child));
        }

//        symbol_table.print();
    
        codegen_mips.writeToFile(result);
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitVariableDefinition(AggluNyelvParser::VariableDefinitionContext * ctx)
    {
        std::string result;
        
        Symbol symbol { std::any_cast<Symbol>(visit(ctx->variableDeclaration())) };
    
        if (symbol.arr_size != static_cast<int>(ctx->variableValue().size()))
        {
            log_error(ctx, "A létrehozandó tömb \"", symbol.name, "\" mérete nem egyezik a létrehozó paraméterek számával.");
        }
        
        bool success { symbol_table.pushToCurrentScope(symbol) };
        if (success)
        {
            for (int i { 0 }; i < symbol.arr_size; ++i)
            {
                // TODO type check - should pass the code and the type as pairs
                result += std::any_cast<std::string>(visit(ctx->variableValue()[i]));
                result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
            }
        }
        else
        {
            log_error(ctx, "Szimbólumnév \"", symbol.name, "\" már használatban van.");
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext * ctx)
    {
        Symbol symbol_props { std::any_cast<Symbol>(visit(ctx->variableDeclarationProperties())) };
        
        Symbol symbol {
            .name = ctx->SYMBOL_NAME()->toString(),
            .type = symbol_props.type,
            .is_mutable = symbol_props.is_mutable,
            .arr_size = symbol_props.arr_size
        };

        return symbol;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext * ctx)
    {
        // returning a symbol which only contains the properties data
        Symbol properties {
            .name = "",
            .type = ctx->TYPE() ? ctx->TYPE()->toString() : ctx->SYMBOL_NAME()->toString(),
            .is_mutable = getIsMutableFromStdString(ctx->CONSTNESS()->toString()),
            .arr_size = ctx->INTEGER() ? std::stoi(ctx->INTEGER()->toString()) : 1
        };
        
        return properties;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitVariableAssignment(AggluNyelvParser::VariableAssignmentContext * ctx)
    {
        std::string result;
    
        for (size_t i { 0 }; i < ctx->variableValue().size(); ++i)
        {
            const std::string symbol_name { ctx->SYMBOL_NAME()->toString() };
            Symbol * symbol { symbol_table.findByName(symbol_name) };
            if (symbol)
            {
                if (symbol->is_mutable)
                {
                    // TODO type check
            
                    result += std::any_cast<std::string>(visit(ctx->variableValue()[i]));
                    result += codegen_mips.genChangeWordFromRegisterToStack("$a0",
                                                                            symbol_table.getOffset(symbol, static_cast<int>(i + 1)));
                }
                else
                {
                    log_error(ctx, "A(z) \"", symbol_name, "\" nevű változó értéke állandó (konstans).");
                }
            }
            else
            {
                log_error(ctx, "A(z) \"", symbol_name, "\" nevű változó nem található.");
            }
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitVariableValue(AggluNyelvParser::VariableValueContext * ctx)
    {
        std::string result;
    
        if (ctx->expression())
        {
            result += std::any_cast<std::string>(visit(ctx->expression())); // expr value is in $a0
        }
        else if (ctx->text())
        {
            result += std::any_cast<std::string>(visit(ctx->text())); // expr value is in $a0
        }
        else if (ctx->boolean())
        {
            result += std::any_cast<std::string>(visit(ctx->boolean())); // expr value is in $a0
        }
        else if (ctx->OP_DEFAULT_VALUE())
        {
            log_error(ctx, "Az alapértelmezett érték operátor még nincs implementálva.");
        }
        else
        {
            log_error(ctx, "Itt hibásan adtál meg valamit...");
        }
    
        return result;
        
//        log_line(ctx->children.at(0)->getText());
//        log_line(std::any_cast<std::string>(visit(ctx->children.at(0))));
//        log_line(std::any_cast<std::string>(visit(ctx->children.at(0))));
//        return visitChildren(ctx);
//        return std::string{"visitVariableValue"};


//        ExpressionResult variableValue;
//        if (ctx->children.empty())
//        {
//            // TODO send the error msg from here instead from ANTLR
//        }
//        else
//        {
//            variableValue = std::any_cast<ExpressionResult>(visitChildren(ctx));
//        }
//
//        if (variableValue.bad_bit)
//        {
//            // TODO maybe stopping?
//        }
//
//        return variableValue.result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext * ctx)
    {
        const std::string name { ctx->SYMBOL_NAME()->toString() };
        
        // TODO what if an array in an outer scope has the i. element...
        Symbol * symbol { symbol_table.findByName(name) };
        if (symbol == nullptr)
        {
            log_error(ctx, "A \"", name, "\" nevű változó nem található.");
        }
        else if (symbol->arr_size < std::stoi(ctx->INTEGER()->toString()))
        {
            log_error(ctx, "A \"", name, "\" nevű változónak nincs ", ctx->INTEGER()->toString(), ". eleme.");
        }
        
        return symbol;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext * ctx)
    {
        std::string result;
        
        Symbol * symbol { std::any_cast<Symbol *>(visit(ctx->arrayElementAccess())) };
    
        if (symbol)
        {
            if (symbol->is_mutable)
            {
                // TODO type check
        
                result += std::any_cast<std::string>(visit(ctx->variableValue()));
                const int offset { symbol_table.getOffset(symbol, std::stoi(ctx->arrayElementAccess()->INTEGER()->toString()))};
                result += codegen_mips.genChangeWordFromRegisterToStack("$a0", offset);
            }
            else
            {
                log_error(ctx, "A(z) \"", symbol->name, "\" nevű változó értéke állandó (konstans).");
            }
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext * ctx)
    {
        std::string expr {
                std::any_cast<std::string>(visit(ctx->expression()))
        }; // result is at $a0

        return codegen_mips.genExprNeg(expr); // result is at $a0;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpMul(AggluNyelvParser::ExprGrpMulContext * ctx)
    {
        std::string expr1 {
                std::any_cast<std::string>(visit(ctx->expression(0)))
        }; // result is at $a0
        
        std::string expr2 {
                std::any_cast<std::string>(visit(ctx->expression(1)))
        }; // result is at $a0
        
        std::string result;
        
        if (ctx->OP_MULTIPLICATION())
        {
            result = codegen_mips.genExprMul(expr1, expr2); // result is at $a0
        }
        else if (ctx->OP_DIVISION())
        {
            result = codegen_mips.genExprDiv(expr1, expr2); // result is at $a0
        }
        
        return result;

//        visit(ctx->expression(0)); // result is at $a0
//
//        codegen_mips.genStoreWordFromRegisterToStack("$a0");
//
//        visit(ctx->expression(1)); // result is at $a0
//
//        codegen_mips.genPopWordFromStackAndLoadToRegister("$t0");
//
//        if (ctx->OP_MULTIPLICATION())
//        {
//            codegen_mips.genExprMul("$t0", "$a0"); // result is at $a0
//        }
//        else if (ctx->OP_DIVISION())
//        {
//            codegen_mips.genExprDiv("$t0", "$a0"); // result is at $a0
//        }
//
//        return defaultResult();

//        ExpressionResult solution;
//
//        const ExpressionResult left  { std::any_cast<ExpressionResult>(visit(ctx->expression(0))) };
//        const ExpressionResult right { std::any_cast<ExpressionResult>(visit(ctx->expression(1))) };
//
//        solution.bad_bit |= left.bad_bit | right.bad_bit;
//
//        if (ctx->OP_MULTIPLICATION())
//        {
//            solution.result =
//                    code_generator_target_c.getOpMul(left.result,
//                                                     right.result);
//        }
//        else if (ctx->OP_DIVISION())
//        {
//            solution.result =
//                    code_generator_target_c.getOpDiv(left.result,
//                                                     right.result);
//        }
//
//        return solution;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext * ctx)
    {
        std::string expr1 {
                std::any_cast<std::string>(visit(ctx->expression(0)))
        }; // result is at $a0
        
        std::string expr2 {
                std::any_cast<std::string>(visit(ctx->expression(1)))
        }; // result is at $a0
        
        std::string result;
        
        if (ctx->OP_ADDITION())
        {
            result = codegen_mips.genExprAdd(expr1, expr2); // result is at $a0
        }
        else if (ctx->OP_SUBTRACTION())
        {
            result = codegen_mips.genExprSub(expr1, expr2); // result is at $a0
        }
        
        return result;

//        ExpressionResult solution;
//
//        const ExpressionResult left  { std::any_cast<ExpressionResult>(visit(ctx->expression(0))) };
//        const ExpressionResult right { std::any_cast<ExpressionResult>(visit(ctx->expression(1))) };
//
//        solution.bad_bit |= left.bad_bit | right.bad_bit;
//
//        if (ctx->OP_ADDITION())
//        {
//            solution.result =
//                    code_generator_target_c.getOpAdd(left.result,
//                                                     right.result);
//        }
//        else if (ctx->OP_SUBTRACTION())
//        {
//            solution.result =
//                    code_generator_target_c.getOpSub(left.result,
//                                                     right.result);
//        }
//
//        return solution;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpInt(AggluNyelvParser::ExprGrpIntContext * ctx)
    {
        // TODO conversion success check
        return codegen_mips.genExprInt(ctx->INTEGER()->toString());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpFun(AggluNyelvParser::ExprGrpFunContext * ctx)
    {
        // explicit call for future compatibility
        // it has the same effect as calling visitChildren()
        return visit(ctx->functionCall());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpArr(AggluNyelvParser::ExprGrpArrContext * ctx)
    {
        std::string result;
        
        Symbol * symbol { std::any_cast<Symbol *>(visit(ctx->arrayElementAccess())) };
        if (symbol)
        {
            if (symbol->is_mutable)
            {
                // TODO type check
                const int offset { symbol_table.getOffset(symbol, std::stoi(ctx->arrayElementAccess()->INTEGER()->toString()))};
                result += codegen_mips.genLoadWordFromStackToRegister("$a0", offset);
            }
            else
            {
                log_error(ctx, "A(z) \"", symbol->name, "\" nevű változó értéke állandó (konstans).");
            }
        }
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpSym(AggluNyelvParser::ExprGrpSymContext * ctx)
    {
        std::string result;
        
        const Symbol * symbol { symbol_table.findByName(ctx->SYMBOL_NAME()->toString()) };
        if (symbol)
        {
            result +=
                codegen_mips.genLoadWordFromStackToRegister(
                        "$a0",
                        symbol_table.getOffset(symbol)
                );
        }
        else
        {
            log_error(ctx, "Szimbólum nem található \"", ctx->SYMBOL_NAME()->toString(), "\" néven.");
        }

        return result;


//        ExpressionResult solution;
//
//        const Symbol * symbol { symbol_table.findByName(ctx->SYMBOL_NAME()->toString()) };
//
//        if (symbol)
//        {
//            solution.result = symbol->name;
//        }
//        else
//        {
//            solution.bad_bit = true;
//            log_error(ctx, "Szimbólum nem található \"", ctx->SYMBOL_NAME()->toString(), "\" néven.");
//        }
//
//        return solution;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitExprGrpPar(AggluNyelvParser::ExprGrpParContext * ctx)
    {
        return visit(ctx->expression());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitStructDefinition(AggluNyelvParser::StructDefinitionContext * ctx)
    {
        std::cout << "ctx->SYMBOL_NAME()" << std::endl;
        std::cout << ctx->SYMBOL_NAME()->toString() << std::endl;
        std::cout << ctx->children.size() << std::endl;
        for (auto & c : ctx->children)
        {
            std::cout << "\t" << c->toString() << std::endl;
        }
        return visitChildren(ctx);
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * ctx)
    {
        const std::string fun_name { ctx->SYMBOL_NAME()->toString() };

        std::string result;
//        bool success { symbol_table.pushToCurrentScope(symbol_fun) };
        if (symbol_table_every_function.findByName(fun_name))
        {
            symbol_table.enterScope();
    
            visit(ctx->functionParameters());
    
            result += codegen_mips.genFunctionBegin(fun_name);
            result += std::any_cast<std::string>(visit(ctx->functionBlock()));
            result += codegen_mips.genFunctionEnd();
    
            symbol_table.exitScope();
        }
        else
        {
            log_error(ctx, "Függvény \"", fun_name, "\" nem található.");
        }
        
        return result;
    }
    
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * ctx)
    {
        std::string result;
        
        if (symbol_table_every_function.findByName("main"))
        {
            symbol_table.enterScope();
            
            result += codegen_mips.genLabel("main");

            // TODO tmp! initing main must be moved
    
//            result += codegen_mips.genPrintSp();
            result += codegen_mips.genMainBegin();
            result += std::any_cast<std::string>(visit(ctx->functionBlock()));
            result += codegen_mips.genMainEnd();
    
            symbol_table.exitScope();
        }
        else
        {
            log_error(ctx, "Nem található kezdjük!() függvény.");
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionParameters(AggluNyelvParser::FunctionParametersContext * ctx)
    {
        // for every variable
        for (AggluNyelvParser::VariableDeclarationContext * var : ctx->variableDeclaration())
        {
            // for every element (in a possible array)
            const Symbol symbol_param { std::any_cast<Symbol>(visit(var)) };
            
            // pushing into the table so in visitFunctionDefinition we have the correct offsets
            bool success { symbol_table.pushToCurrentScope(symbol_param) };
            if (success == false)
            {
                log_error(ctx, "Szimbólumnév \"", symbol_param.name, "\" már használatban van.");
            }
        }
        
        return defaultResult();
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionReturn(AggluNyelvParser::FunctionReturnContext * ctx)
    {
        std::string result;
    
        for (size_t i { 0 }; i < ctx->variableValue().size(); ++i)
        {
            result += std::any_cast<std::string>(visit(ctx->variableValue()[i]));
            result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
        }
    
        return result;
        
//        std::any result { defaultResult() };
//
//        if (ctx->expression())
//        {
//            result = visit(ctx->expression());
//            ExpressionResult expr { std::any_cast<ExpressionResult>(result) };
//        }
//        else if (ctx->TEXT())
//        {
//            // TODO string
//        }
//        else
//        {
//            // TODO the character number in the error msg is incorrect
//            log_error(ctx, "Az eredmény megadása hibás.");
//        }
//
//        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionBlock(AggluNyelvParser::FunctionBlockContext * ctx)
    {
        symbol_table.flagParentScopeUnreachable();
        
        std::string result;
        
        const int num_of_local_vars_beg { symbol_table.getNumOfScopelocals() };
    
        for (AggluNyelvParser::BlockPartContext * block_part_context : ctx->blockPart())
        {
            result += std::any_cast<std::string>(visit(block_part_context));
        }
        
        if (ctx->functionReturn())
        {
            result += std::any_cast<std::string>(visit(ctx->functionReturn()));
        }
    
        // popping the local variables from the stack
        const int num_of_local_vars_end { symbol_table.getNumOfScopelocals() };
        const int num_of_local_vars_added_here { num_of_local_vars_end - num_of_local_vars_beg };
        if (num_of_local_vars_added_here > 0)
        {
            result += codegen_mips.genPopWordsFromStack(num_of_local_vars_added_here);
        }
    
        symbol_table.flagParentScopeReachable();
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionArgument(AggluNyelvParser::FunctionArgumentContext * ctx)
    {
        std::string result;
    
        if (ctx->SYMBOL_NAME()) // possibly an array
        {
            const Symbol * symbol { symbol_table.findByName(ctx->SYMBOL_NAME()->toString()) };
            if (symbol)
            {
                for (int i { 0 }; i < symbol->arr_size; ++i)
                {
                    const int offset { symbol_table.getOffset(symbol, i + 1) };
                    result += codegen_mips.genLoadWordFromStackToRegister("$a0", offset);
                    result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
                }
            }
            else
            {
                log_error(ctx, "A(z) \"", symbol->name, "\" nevű változó nem található.");
            }
        }
        else if (ctx->variableValue())
        {
            result += std::any_cast<std::string>(visit(ctx->variableValue()));
            result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
        }

        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionCall(AggluNyelvParser::FunctionCallContext * ctx)
    {
        // test if function exists and in scope
        std::string fun_name { ctx->SYMBOL_NAME()->toString() };
        if (ctx->PREFIX_STD())
        {
            fun_name = prefixStd() + fun_name;
        }
        Symbol * symbol_fun { symbol_table_every_function.findByName(fun_name) };
        
        std::string result;
        
        if (symbol_fun == nullptr)
        {
            log_error(ctx, "Függvény \"", fun_name, "\" nem található.");
            return result;
        }
    
//        log_line(fun_name, " ", ctx->expression().size(), " ", ctx->TEXT().size());
    
        // calculating the parameters
        std::string generated_param_code;
        int num_of_arg_slots { 0 }; // this includes place for array elems
        if (symbol_fun->num_of_params == static_cast<int>(ctx->functionArgument().size()))
        {
            for (size_t i { 0 }; i < ctx->functionArgument().size(); ++i)
            {
                generated_param_code += std::any_cast<std::string>(visit(ctx->functionArgument()[i]));
                
                if (ctx->functionArgument()[i]->SYMBOL_NAME()) // might be an array
                {
                    const Symbol * param { symbol_table.findByName(ctx->functionArgument()[i]->SYMBOL_NAME()->toString()) };
                    if (param)
                    {
                        num_of_arg_slots += param->arr_size;
                    }
                }
                else
                {
                    num_of_arg_slots += 1;
                }
            }
        }
        else
        {
            log_error(ctx, "A függvény argumentumainak száma nem egyezik.");
            return result;
        }
        
        // calling the function
        result = codegen_mips.genBuildFrameAndJumpToFunction(symbol_fun->name, num_of_arg_slots, generated_param_code);
    
        return result;
        
        
//        for (size_t i { 0 }; i < ctx->expression().size(); ++i)
//        {
//            ExpressionResult arg { std::any_cast<ExpressionResult>(visit(ctx->expression()[i])) };
//            // TODO param type check
//            symbol_fun->parameters[i].value = arg.result;
//
////            // name mangling
////            std::string symbol_expr_name {
////                std::string{} +
////                "__param_" +
////                symbol_fun->name +
////                "_" +
////                std::to_string(i)
////            };
////            Symbol symbol_expr {
////                symbol_expr_name,
////                symbol_fun->parameters[i].type,
////                symbol_fun->parameters[i].is_mutable,
////                arg.result
////            };
////            if (symbol_table.pushToCurrentScope(symbol_expr) == false)
////            {
////                log_error(ctx, "This shouldn't happen. Name mangling for param ", i, " in function ", symbol_fun->name, " was failed.");
////            }
//        }
//
//        return ExpressionResult {symbol_fun->name, false};
////        return defaultResult();
    }
    
//    std::any AggluNyelvVisitorImpl_Compiling::visitFunctionArgument(AggluNyelvParser::FunctionArgumentContext * ctx)
//    {
//        std::string result;
//
//        if (ctx->expression())
//        {
//            result += std::any_cast<std::string>(visit(ctx->expression())); // expr value is in $a0
//            result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
//        }
//        else if (ctx->text())
//        {
//            result += std::any_cast<std::string>(visit(ctx->text())); // expr value is in $a0
//            result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
//        }
//        else if (ctx->boolean())
//        {
//            result += std::any_cast<std::string>(visit(ctx->boolean())); // expr value is in $a0
//            result += codegen_mips.genStoreWordFromRegisterToStack("$a0");
//        }
//
//        return result;
//    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitStatement(AggluNyelvParser::StatementContext * ctx)
    {
        std::string statement_body_result;
        
        if (ctx->statementBody())
        {
            statement_body_result = std::any_cast<std::string>(visit(ctx->statementBody()));
        }
        
        return statement_body_result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitBlockPart(AggluNyelvParser::BlockPartContext * ctx)
    {
        std::string block_result;
        
        for (antlr4::tree::ParseTree * child : ctx->children)
        {
            block_result += std::any_cast<std::string>(visit(child));
        }
        
        return block_result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitBlock(AggluNyelvParser::BlockContext * ctx)
    {
        std::string result;
        
        const int num_of_local_vars_beg { symbol_table.getNumOfScopelocals() };
    
        for (AggluNyelvParser::BlockPartContext * block_part_context : ctx->blockPart())
        {
            result += std::any_cast<std::string>(visit(block_part_context));
        }
        
        // popping the local variables from the stack
        const int num_of_local_vars_end { symbol_table.getNumOfScopelocals() };
        const int num_of_local_vars_added_here { num_of_local_vars_end - num_of_local_vars_beg };
        if (num_of_local_vars_added_here > 0)
        {
            result += codegen_mips.genPopWordsFromStack(num_of_local_vars_added_here);
        }
    
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitBoolean(AggluNyelvParser::BooleanContext * ctx)
    {
        std::string result;
        
        if (ctx->BOOLEAN_TRUE())
        {
            result += codegen_mips.genBoolean(true);
        }
        else if (ctx->BOOLEAN_FALSE())
        {
            result += codegen_mips.genBoolean(false);
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLogicExprCompare(AggluNyelvParser::LogicExprCompareContext * ctx)
    {
        std::string logic_expr1 {
                std::any_cast<std::string>(visit(ctx->logicalExpression(0)))
        }; // result is at $a0
    
        std::string logic_expr2 {
                std::any_cast<std::string>(visit(ctx->logicalExpression(1)))
        }; // result is at $a0
        
        std::string result;
        
        if (ctx->opLogicalComparison()->OP_COMP_EQUALS())
        {
            result += codegen_mips.genLogicExprCompareEqual(logic_expr1, logic_expr2);
        }
        else if (ctx->opLogicalComparison()->OP_COMP_NOTEQUALS())
        {
            result += codegen_mips.genLogicExprCompareNotEqual(logic_expr1, logic_expr2);
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext * ctx)
    {
        return visit(ctx->logicalExpression());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext * ctx)
    {
        std::string expr1 {
                std::any_cast<std::string>(visit(ctx->expression(0)))
        }; // result is at $a0
    
        std::string expr2 {
                std::any_cast<std::string>(visit(ctx->expression(1)))
        }; // result is at $a0
    
        std::string result;
    
        if (ctx->opComparison()->OP_COMP_EQUALS())
        {
            result += codegen_mips.genLogicExprCompExprEqual(expr1, expr2);
        }
        else if (ctx->opComparison()->OP_COMP_NOTEQUALS())
        {
            result += codegen_mips.genLogicExprCompExprNotEqual(expr1, expr2);
        }
        else if (ctx->opComparison()->OP_COMP_LESS_THAN())
        {
            result += codegen_mips.genLogicExprCompExprLessThan(expr1, expr2);
        }
        else if (ctx->opComparison()->OP_COMP_LESS_OR_EQUAL_THAN())
        {
            result += codegen_mips.genLogicExprCompExprLessOrEqualThan(expr1, expr2);
        }
        else if (ctx->opComparison()->OP_COMP_GREATER_THAN())
        {
            result += codegen_mips.genLogicExprCompExprGreaterThan(expr1, expr2);
        }
        else if (ctx->opComparison()->OP_COMP_GREATER_OR_EQUAL_THAN())
        {
            result += codegen_mips.genLogicExprCompExprGreaterOrEqualThan(expr1, expr2);
        }
        
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLogicExprNot(AggluNyelvParser::LogicExprNotContext * ctx)
    {
        std::string logic_expr {
                std::any_cast<std::string>(visit(ctx->logicalExpression()))
        }; // result is at $a0
    
        return codegen_mips.genLogicExprNot(logic_expr);
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLogicExprOr(AggluNyelvParser::LogicExprOrContext * ctx)
    {
        std::string logic_expr1 {
                std::any_cast<std::string>(visit(ctx->logicalExpression(0)))
        }; // result is at $a0
    
        std::string logic_expr2 {
                std::any_cast<std::string>(visit(ctx->logicalExpression(1)))
        }; // result is at $a0
        
        return codegen_mips.genLogicExprOr(logic_expr1, logic_expr2);;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLogicExprAnd(AggluNyelvParser::LogicExprAndContext * ctx)
    {
        std::string logic_expr1 {
                std::any_cast<std::string>(visit(ctx->logicalExpression(0)))
        }; // result is at $a0
    
        std::string logic_expr2 {
                std::any_cast<std::string>(visit(ctx->logicalExpression(1)))
        }; // result is at $a0
    
        return codegen_mips.genLogicExprAnd(logic_expr1, logic_expr2);;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitConditional(AggluNyelvParser::ConditionalContext * ctx)
    {
        using CondParts = CodeGeneratorTargetMips::ConditionalParts;
        
        // an if branch is guaranteed at this point
        CondParts cond_parts_if {
            std::any_cast<CondParts>(visit(ctx->conditionalIf()))
        };

        std::vector<CondParts> cond_parts_elifs;
        for (AggluNyelvParser::ConditionalElifContext * conditional_elif_context : ctx->conditionalElif())
        {
            cond_parts_elifs.push_back(
                std::any_cast<CondParts>(visit(conditional_elif_context))
            );
        }
    
        CondParts cond_parts_else;
        if (ctx->conditionalElse())
        {
            cond_parts_else = std::any_cast<CondParts>(visit(ctx->conditionalElse()));
        }
        
        return codegen_mips.genConditional(cond_parts_if, cond_parts_elifs, cond_parts_else);
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitConditionalIf(AggluNyelvParser::ConditionalIfContext * ctx)
    {
        CodeGeneratorTargetMips::ConditionalParts conditional_parts {
            std::any_cast<std::string>(visit(ctx->logicalExpression())),
            std::any_cast<std::string>(visit(ctx->block()))
        };
        
        return conditional_parts;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitConditionalElif(AggluNyelvParser::ConditionalElifContext * ctx)
    {
        return visit(ctx->conditionalIf());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitConditionalElse(AggluNyelvParser::ConditionalElseContext * ctx)
    {
        CodeGeneratorTargetMips::ConditionalParts conditional_parts {
            "",
            std::any_cast<std::string>(visit(ctx->block()))
        };
        
        return conditional_parts;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLoop(AggluNyelvParser::LoopContext * ctx)
    {
        Symbol symbol_loopvar { std::any_cast<Symbol>(visit(ctx->loopvarDeclaration())) };
    
        std::string result;
    
        symbol_table.enterScope();
    
        bool success { symbol_table.pushToCurrentScope(symbol_loopvar) };
        if (success)
        {
            // pushing 2 placeholder symbol to the table
            // because we store not just the loopvar itself
            // but the end value and the stepping value
            success = symbol_table.pushToCurrentScope(Symbol {});
            if (success == false)
            {
                log_error("Unable to add placeholder symbol.");
            }
            success = symbol_table.pushToCurrentScope(Symbol {});
            if (success == false)
            {
                log_error("Unable to add placeholder symbol.");
            }
            
            if (symbol_loopvar.type == symbolTypeInt())
            {
                // TODO type check - should pass the code and the type as pairs
                const std::string beg {
                    std::any_cast<std::string>(visit(ctx->loopvarValueBeg()))
                };
                const std::string end {
                    std::any_cast<std::string>(visit(ctx->loopvarValueEnd()))
                };
                const std::string step {
                    std::any_cast<std::string>(visit(ctx->loopvarValueStep()))
                };
                const std::string block {
                    std::any_cast<std::string>(visit(ctx->block()))
                };
                
                result += codegen_mips.genLoop(symbol_loopvar,beg,end,step, block);
                
//                if ((beg < end && step > 0) || (beg > end && step < 0))
//                {
//                    std::string block { std::any_cast<std::string>(visit(ctx->block())) };
//                    result += codegen_mips.genLoop(symbol_loopvar,beg,end,step, block);
//                }
//                else
//                {
//                    log_error(ctx, "A ciklus a megadott feltételekkel nem tudja elérni a célját.");
//                }
            }
            else
            {
                log_error(ctx, "A ciklusok csak egészekkel működnek jelenleg.");
            }
        }
        else
        {
            log_error(ctx, "Szimbólumnév \"", symbol_loopvar.name, "\" már használatban van.");
        }
        
        symbol_table.exitScope();
    
        return result;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext * ctx)
    {
        Symbol symbol_props { std::any_cast<Symbol>(visit(ctx->variableDeclarationProperties())) };
        
        // TODO check for array, not supported yet!
        
        Symbol symbol {
            .name = ctx->SYMBOL_NAME()->toString(),
            .type = symbol_props.type,
            .is_mutable = symbol_props.is_mutable
        };
    
        return symbol;
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext * ctx)
    {
        return visit(ctx->expression());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext * ctx)
    {
        return visit(ctx->expression());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext * ctx)
    {
        return visit(ctx->expression());
    }
    
    std::any AggluNyelvVisitorImpl_Compiling::visitText(AggluNyelvParser::TextContext * ctx)
    {
        return codegen_mips.genStoreTextAndPutAddrIntoA0(ctx->TEXT()->toString());
    }
    
    
} // agglunyelv