#include "CodeGenerator.h"

#include <utility>
#include "common.h"

namespace agglunyelv
{
    CodeGeneratorTargetMips::CodeGeneratorTargetMips()
    {
        out_file.open("prog.mips");
        
        std::string beginning;
        
        beginning += ".text\n\n";
        beginning += genStdFunctions();
    
        out_file << beginning;
    }
    
    CodeGeneratorTargetMips::~CodeGeneratorTargetMips()
    {
        std::string ending;
        
//        ending += genComment("print the last thing from $a0");
//        ending += genInstr("jal", "print_int_from_a0");

//        ending += genPrintSp();
        
        
        ending += "\n.data\n\n";
        ending += genLabelAndText("str_error_inf_loop", R"("Hiba! Végtelen ciklus!")");
        
        for (size_t i { 0 }; i < texts.size(); ++i)
        {
            ending += genLabelAndText(std::string { "text_" } + std::to_string(i + 1), texts[i]);
        }
        
        out_file << ending;
    }
    
    void CodeGeneratorTargetMips::writeToFile(const std::string & data)
    {
        out_file << data;
    }
    
    std::string CodeGeneratorTargetMips::genInstr(const std::string & instr,
                                                  const std::string & param1,
                                                  const std::string & param2,
                                                  const std::string & param3)
    {
        std::string result;
    
        result += "    " + instr;
    
        if (param1.empty() == false)
        {
            result += " " + param1;
        }
    
        if (param2.empty() == false)
        {
            result += ", " + param2;
        }
    
        if (param3.empty() == false)
        {
            result += ", " + param3;
        }
    
        result += "\n";
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLabel(const std::string & label)
    {
        return label + ":\n";
    }
    
    std::string CodeGeneratorTargetMips::genComment(const std::string & comment)
    {
//        return std::string{} + "    # " + comment + "\n";
        return std::string{} + "# " + comment + "\n";
    }
    
    std::string CodeGeneratorTargetMips::genBlockBorder()
    {
        return std::string { '\n' };
    }
    
    std::string CodeGeneratorTargetMips::genLabelAndText(const std::string & label, const std::string & text)
    {
//        return label + ": .asciiz    " + text.substr(0, text.size() - 1) + "\\n\"\n";
        return label + ": .asciiz    " + text + "\n";
    }
    
    std::string CodeGeneratorTargetMips::genStoreWordFromRegisterToStack(const std::string & registr)
    {
        std::string result;
        
        // pushing to the top of the stack
        result += genInstr("sw", registr, "0($sp)");
        // moving the stack pointer to the new top
        result += genInstr("addiu", "$sp", "$sp", "-4");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genPopWordFromStackAndLoadToRegister(const std::string & registr)
    {
        std::string result;
        
        // reading from the stack - 4 bytes from the pointer is the top
        result += genInstr("lw", registr, "4($sp)");
        // "popping" the stack by moving the pointer
        result += genInstr("addiu", "$sp", "$sp", "4");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLoadWordFromStackToRegister(const std::string & registr,
                                                                        const int offset_from_scope_start)
    {
        // stack grows towards lower addresses by 4
        int offset_byte_from_fp { -4 * (offset_from_scope_start + offset_in_frame_before_params) };
        return genInstr("lw", registr, std::to_string(offset_byte_from_fp) + "($fp)");
    }
    
    std::string CodeGeneratorTargetMips::genChangeWordFromRegisterToStack(const std::string & registr,
                                                                          const int offset_from_scope_start)
    {
        // stack grows towards lower addresses by 4
        int offset_byte_from_fp { -4 * (offset_from_scope_start + offset_in_frame_before_params) };
        return genInstr("sw", registr, std::to_string(offset_byte_from_fp) + "($fp)");
    }
    
    std::string CodeGeneratorTargetMips::genFunctionBegin(const std::string & fun_name)
    {
        std::string result;
        
        result += genLabel(fun_name);
        
        // storing the return address on the stack
        result += genInstr("sw", "$ra", "-4($fp)");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genFunctionEnd()
    {
        std::string result;
        
        // restoring the saved return address
        result += genInstr("lw", "$ra", "-4($fp)");

        // jump to register
        result += genInstr("jr", "$ra");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genMainBegin()
    {
        std::string result;
        
        result += genInstr("move", "$fp", "$sp");
        result += genInstr("addiu", "$sp", "$sp", std::to_string(-4 * offset_in_frame_before_params));
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genMainEnd()
    {
        std::string result;
    
        result += genComment("syscall: exit");
        result += genInstr("li", "$v0", "10");
        result += genInstr("syscall");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genBuildFrameAndJumpToFunction(const std::string & fun_name,
                                                                        const int fun_num_of_arg_slots,
                                                                        const std::string & generated_param_code)
    {
        std::string result;
    
        result += genBlockBorder();
        
        result += genComment("genBuildFrameAndJumpToFunction");
    
        // saving the old frame pointer on the top of the stack
        result += genInstr("sw", "$fp", "0($sp)");
        
        // moving the stack pointer to the new top
        result += genInstr("addiu", "$sp", "$sp", std::to_string(-4 * offset_in_frame_before_params));
        
        // inserting earlier generated parameter code (code is pushing params to stack)
        result += genComment("pushing params");
        result += generated_param_code;
    
        // setting the frame pointer to the stack pointer
        // (to point to the old fp, start of the frame)
        const int offset_byte_from_sp_to_frame_beg { 4 * (fun_num_of_arg_slots + offset_in_frame_before_params) };
        result += genInstr("move", "$fp", "$sp");
        result += genInstr("addiu", "$fp", "$fp", std::to_string(offset_byte_from_sp_to_frame_beg));
    
        // jump and link
        result += genInstr("jal", fun_name);
        
        // setting $sp to $fp to pop everything related to this frame from the stack
        result += genInstr("move", "$sp", "$fp");
        // restoring the old frame pointer
        result += genInstr("lw", "$fp", "0($fp)");
        
        result += genBlockBorder();
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genExprNeg(const std::string & expr)
    {
        std::string result;
        
        result += expr;
        result += genInstr("neg", "$a0", "$a0");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genExprInt(const std::string & value)
    {
        return genInstr("li", "$a0", value);
    }
    
    std::string CodeGeneratorTargetMips::genExprMul(const std::string & expr1,
                                                    const std::string & expr2)
    {
        std::string result;
    
        result += expr1;
        
        result += genStoreWordFromRegisterToStack("$a0");
        
        result += expr2;
        
        result += genPopWordFromStackAndLoadToRegister("$t0");
        
        result += genInstr("mult", "$t0", "$a0");
        result += genInstr("mflo", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genExprDiv(const std::string & expr1,
                                                    const std::string & expr2)
    {
        std::string result;
    
        result += expr1;
    
        result += genStoreWordFromRegisterToStack("$a0");
    
        result += expr2;
    
        result += genPopWordFromStackAndLoadToRegister("$t0");
        
        result += genInstr("div", "$t0", "$a0");
        result += genInstr("mflo", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genExprAdd(const std::string & expr1,
                                                    const std::string & expr2)
    {
        std::string result;
    
        result += expr1;
        result += genStoreWordFromRegisterToStack("$a0");
        result += expr2;
        result += genPopWordFromStackAndLoadToRegister("$t0");
        result += genInstr("add", "$a0", "$t0", "$a0");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genExprSub(const std::string & expr1,
                                                    const std::string & expr2)
    {
        std::string result;
    
        result += expr1;
        result += genStoreWordFromRegisterToStack("$a0");
        result += expr2;
        result += genPopWordFromStackAndLoadToRegister("$t0");
        result += genInstr("sub", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genPopWordsFromStack(const int num_of_words)
    {
        int offset_byte_from_sp { 4 * num_of_words };
        return genInstr("addiu", "$sp", "$sp", std::to_string(offset_byte_from_sp));
    }
    
    std::string CodeGeneratorTargetMips::genMoveRegister1ToRegister2(const std::string & registr1,
                                                                     const std::string & registr2)
    {
        return genInstr("move", registr1, registr2);
    }
    
    std::string CodeGeneratorTargetMips::genPrintSp()
    {
        std::string result;
    
        result += genInstr("move", "$a0", "$sp");
        result += genInstr("jal", "std.debug_int_a0");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genStdFunctions()
    {
        std::string result;
    
        result += genStdPrintInt();
        result += genStdReadInt();
        result += genStdDebugInt_a0();
        result += genStdPrintString();
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genStdPrintInt()
    {
        std::string result;
        
        result += genLabel(FUN_STD_PRINT_INT);
        // set to print int
        result += genInstr("li", "$v0", "1");
        // saving $a0 - this function preserves not just the stack, but $a0 as well
        result += genInstr("move", "$t0", "$a0");
        result += genInstr("lw", "$a0", "4($sp)");
        result += genInstr("syscall");
        // set to print char
        result += genInstr("li", "$v0", "11");
        // printing line break
        result += genInstr("li", "$a0", "10");
        result += genInstr("syscall");
        // restoring $a0
        result += genInstr("move", "$a0", "$t0");
        result += genInstr("jr", "$ra");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genStdReadInt()
    {
        std::string result;
        
        result += genLabel(FUN_STD_READ_INT);
        // set to read int
        result += genInstr("li", "$v0", "5");
        result += genInstr("syscall");
        result += genInstr("move", "$a0", "$v0");
        result += genInstr("jr", "$ra");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genStdDebugInt_a0()
    {
        std::string result;
    
        result += genLabel("std.debug_int_a0");
        result += genInstr("li", "$v0", "1");
        result += genInstr("syscall");
        result += genInstr("li", "$v0", "11");
        result += genInstr("move", "$t0", "$a0");
        result += genInstr("li", "$a0", "10");
        result += genInstr("syscall");
        // TODO $a0 should be saved on the stack, so $t0 is preserved too
        result += genInstr("move", "$a0", "$t0");
        result += genInstr("jr", "$ra");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genStdPrintString()
    {
        std::string result;
        
        result += genLabel(FUN_STD_PRINT_STR);
        // set to print str
        result += genInstr("li", "$v0", "4");
        result += genInstr("syscall");

        result += genInstr("jr", "$ra");
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genBoolean(const bool is_true)
    {
        return genInstr("li", "$a0", is_true ? "1" : "0");
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompareEqual(const std::string & logic_expr1,
                                                                  const std::string & logic_expr2)
    {
        std::string result;
    
        result += logic_expr1; // result is at $a0
        result += genStoreWordFromRegisterToStack("$a0");
        result += logic_expr2; // result is at $a0
        result += genPopWordFromStackAndLoadToRegister("$t0");
        result += genInstr("seq", "$t0", "$a0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompareNotEqual(const std::string & logic_expr1,
                                                                     const std::string & logic_expr2)
    {
        std::string result;
    
        result += logic_expr1; // result is at $a0
        result += genStoreWordFromRegisterToStack("$a0");
        result += logic_expr2; // result is at $a0
        result += genPopWordFromStackAndLoadToRegister("$t0");
        result += genInstr("sne", "$t0", "$a0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genConditional(const ConditionalParts & cond_parts_if,
                                                        const std::vector<ConditionalParts> & cond_parts_elifs,
                                                        const ConditionalParts & cond_parts_else)
    {
        const int id_cond { getNextIdConditional() };
        int id_cond_part  { 1 };
    
        std::string result;
    
        result += genConditionalPart(cond_parts_if, id_cond, id_cond_part);
        id_cond_part += 1;
        
        for (const ConditionalParts & cond_parts_elif : cond_parts_elifs)
        {
            result += genConditionalPart(cond_parts_elif, id_cond, id_cond_part);
            id_cond_part += 1;
        }
        
        result += genComment("else");
        result += cond_parts_else.block;
        
        result += genLabel(genConditionalEndLabelName(id_cond));
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genConditionalPart(const ConditionalParts & cond_parts,
                                                            const int id_cond,
                                                            const int id_cond_part)
    {
        std::string result;
    
        std::string label_name_beg {
                std::string { "cond_" } + std::to_string(id_cond) + "_" + std::to_string(id_cond_part)
        };
        std::string label_name_end {
                std::string { "cond_" } + std::to_string(id_cond) + "_" + std::to_string(id_cond_part) + "_end"
        };
        std::string label_end {genConditionalEndLabelName(id_cond) };
    
        result += genBlockBorder();
        result += genComment("genConditionalPart");
        result += genLabel(label_name_beg);
        result += cond_parts.predicate; // $a0 == 1 for enter
        result += genInstr("beq", "$a0", "$zero", label_name_end);
        result += cond_parts.block;
        result += genInstr("b", label_end);
        result += genLabel(label_name_end);
        result += genBlockBorder();
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genConditionalEndLabelName(const int id_cond)
    {
        return std::string { "cond_" } + std::to_string(id_cond) + "_end";
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompExprEqual(const std::string & expr1,
                                                                   const std::string & expr2)
    {
        std::string result;
    
        result += genLogicExprBase(expr1, expr2);
        result += genInstr("seq", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompExprNotEqual(const std::string & expr1,
                                                                      const std::string & expr2)
    {
        std::string result;
    
        result += genLogicExprBase(expr1, expr2);
        result += genInstr("sne", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompExprLessThan(const std::string & expr1,
                                                                      const std::string & expr2)
    {
        std::string result;
    
        result += genLogicExprBase(expr1, expr2);
        result += genInstr("slt", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompExprLessOrEqualThan(const std::string & expr1,
                                                                             const std::string & expr2)
    {
        std::string result;
    
        result += genLogicExprBase(expr1, expr2);
        result += genInstr("sle", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompExprGreaterThan(const std::string & expr1,
                                                                         const std::string & expr2)
    {
        std::string result;
    
        result += genLogicExprBase(expr1, expr2);
        result += genInstr("sgt", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprCompExprGreaterOrEqualThan(const std::string & expr1,
                                                                                const std::string & expr2)
    {
        std::string result;
    
        result += genLogicExprBase(expr1, expr2);
        result += genInstr("sge", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprBase(const std::string & expr1, const std::string & expr2)
    {
        std::string result;
    
        result += expr1; // result is at $a0
        result += genStoreWordFromRegisterToStack("$a0");
        result += expr2; // result is at $a0
        result += genPopWordFromStackAndLoadToRegister("$t0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprNot(const std::string & logic_expr)
    {
        std::string result;
    
        result += logic_expr;
        result += genInstr("not", "$a0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprOr(const std::string & logic_expr1, const std::string & logic_expr2)
    {
        std::string result;
    
        result += genLogicExprBase(logic_expr1, logic_expr2);
        result += genInstr("or", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genLogicExprAnd(const std::string & logic_expr1, const std::string & logic_expr2)
    {
        std::string result;
    
        result += genLogicExprBase(logic_expr1, logic_expr2);
        result += genInstr("and", "$a0", "$t0", "$a0");
    
        return result;
    }
    
    int CodeGeneratorTargetMips::getNextIdConditional()
    {
        id_conditional += 1;
        return id_conditional;
    }
    
    int CodeGeneratorTargetMips::getNextIdLoop()
    {
        id_loop += 1;
        return id_loop;
    }
    
    int CodeGeneratorTargetMips::getNextIdText()
    {
        id_text += 1;
        return id_text;
    }
    
    std::string CodeGeneratorTargetMips::genLoop(const Symbol & symbol_loopvar,
                                                 const std::string & loopvar_value_beg,
                                                 const std::string & loopvar_value_end,
                                                 const std::string & loopvar_value_step,
                                                 const std::string & block)
    {
        std::string result;
    
        std::string label_name { std::string {"loop_" } + std::to_string(getNextIdLoop()) };
        std::string label_name_end { label_name + "_end" };
        std::string label_name_err { label_name + "_error" };
    
        result += genComment("loop");
        // pushing the loopvar data onto the stack
        result += loopvar_value_beg;
        result += genStoreWordFromRegisterToStack("$a0"); // 12($sp)
        result += loopvar_value_end;
        result += genStoreWordFromRegisterToStack("$a0"); // 8($sp)
        result += loopvar_value_step;
        result += genStoreWordFromRegisterToStack("$a0"); // 4($sp)
        // runtime check for possible infinite loop
        result += genComment("runtime check (inf loop)");
        // 1) beg < end && step < 0
        result += genInstr("lw", "$a0", "12($sp)"); // beg
        result += genInstr("lw", "$t0", "8($sp)");  // end
        result += genInstr("slt", "$a0", "$a0", "$t0"); // beg < end ?
        result += genStoreWordFromRegisterToStack("$a0");
        result += genInstr("lw", "$a0", "8($sp)"); // step
        result += genInstr("slt", "$a0", "$a0", "0"); // step < 0
        result += genPopWordFromStackAndLoadToRegister("$t0");
        result += genInstr("and", "$a0", "$a0", "$t0");
        result += genInstr("bne", "$a0", "$zero", label_name_err);
        // 2) beg > end && step > 0
        result += genInstr("lw", "$a0", "12($sp)"); // beg
        result += genInstr("lw", "$t0", "8($sp)");  // end
        result += genInstr("sgt", "$a0", "$a0", "$t0"); // beg > end ?
        result += genStoreWordFromRegisterToStack("$a0");
        result += genInstr("lw", "$a0", "8($sp)"); // step
        result += genInstr("sgt", "$a0", "$a0", "0"); // step > 0
        result += genPopWordFromStackAndLoadToRegister("$t0");
        result += genInstr("and", "$a0", "$a0", "$t0");
        result += genInstr("bne", "$a0", "$zero", label_name_err);
        // 3) step == 0
        result += genInstr("lw", "$a0", "4($sp)"); // step
        result += genInstr("seq", "$a0", "$a0", "0"); // step == 0
        result += genInstr("bne", "$a0", "$zero", label_name_err);
        // potential error message - jump over if the runtime checks went well
        result += genInstr("b", label_name);
        result += genLabel(label_name_err);
        // print out an error to notify the user
//        result += genInstr("la", "$a0", "str_error_inf_loop");
//        result += genInstr("jal", FUN_STD_PRINT_STR);
        result += genInstr("b", label_name_end);
        // start of the loop
        result += genLabel(label_name);
        // loop body
        result += genComment("loop body beg");
        result += block;
        result += genComment("loop body end");
        // stepping the loopvar - stack should be the same as before the block
        result += genInstr("lw", "$a0", "12($sp)");
        result += genInstr("lw", "$t0", "4($sp)");
        result += genInstr("add", "$a0", "$a0", "$t0");
        result += genInstr("sw", "$a0", "12($sp)");
        // check whether to jump
        result += genInstr("lw", "$t0", "8($sp)");
        result += genInstr("abs", "$t0", "$t0");
        result += genInstr("abs", "$a0", "$a0");
        result += genInstr("ble", "$a0", "$t0", label_name);
        // end of loop
        result += genLabel(label_name_end);
        // pop the loopvar data
        result += genPopWordsFromStack(3);
        
        return result;
    }
    
    std::string CodeGeneratorTargetMips::genStoreTextAndPutAddrIntoA0(const std::string & text)
    {
        texts.push_back(text);
        std::string text_label { std::string { "text_" } + std::to_string(getNextIdText()) };
        return genInstr("la", "$a0", text_label);;
    }
    
} // agglunyelv