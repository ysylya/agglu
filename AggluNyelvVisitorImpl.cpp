#include "AggluNyelvVisitorImpl.h"

#include <fstream>

namespace agglunyelv
{
    std::any AggluNyelvVisitorImpl::visitProg(AggluNyelvParser::ProgContext * ctx)
    {
        antlr4::misc::Interval interval {
            ctx->getStart()->getStartIndex(),
            ctx->getStop()->getStopIndex()
        };

        return ctx->getStart()->getInputStream()->getText(interval);
//        return ctx->getText();
    }
} // agglunyelv