#ifndef AGGLUNYELV_CODEGENERATOR_H
#define AGGLUNYELV_CODEGENERATOR_H

#include "SymbolTable.h"

#include <fstream>
#include <string>
#include <vector>

namespace agglunyelv
{
    class CodeGeneratorTargetMips
    {
    public:
        CodeGeneratorTargetMips();
        ~CodeGeneratorTargetMips();
        
        // the genConditional() function need these parts separately,
        // so it generate the correct skeleton
        struct ConditionalParts
        {
            std::string predicate;
            std::string block;
        };
    
        void writeToFile(const std::string & data);
    
        std::string genInstr(const std::string & instr,
                             const std::string & param1 = "",
                             const std::string & param2 = "",
                             const std::string & param3 = "");
    
        std::string genLabel(const std::string & label);
        std::string genComment(const std::string & comment);
        std::string genBlockBorder();
        
        std::string genStoreWordFromRegisterToStack(const std::string & registr);
        std::string genPopWordFromStackAndLoadToRegister(const std::string & registr);
        std::string genLoadWordFromStackToRegister(const std::string & registr,
                                                   const int offset_from_scope_start);
        std::string genChangeWordFromRegisterToStack(const std::string & registr,
                                                     const int offset_from_scope_start);
        std::string genFunctionBegin(const std::string & fun_name);
        std::string genFunctionEnd();
        // since main has no caller to setup the stack frame, it needs to be done in the function
        // so other parts are working correctly (e.g. finding variables relative to $fp)
        std::string genMainBegin();
        std::string genMainEnd();
    
        std::string genBuildFrameAndJumpToFunction(const std::string & fun_name,
                                                   const int fun_num_of_arg_slots,
                                                   const std::string & generated_param_code);
    
        std::string genExprNeg(const std::string & expr);
        std::string genExprInt(const std::string & value);
        std::string genExprMul(const std::string & expr1, const std::string & expr2);
        std::string genExprDiv(const std::string & expr1, const std::string & expr2);
        std::string genExprAdd(const std::string & expr1, const std::string & expr2);
        std::string genExprSub(const std::string & expr1, const std::string & expr2);
    
        std::string genPopWordsFromStack(const int num_of_words);
        std::string genMoveRegister1ToRegister2(const std::string & registr1,
                                                const std::string & registr2);
        
        std::string genPrintSp();
    
        std::string genBoolean(const bool is_true);
        
        std::string genLogicExprCompareEqual(const std::string & logic_expr1,
                                             const std::string & logic_expr2);
        std::string genLogicExprCompareNotEqual(const std::string & logic_expr1,
                                                const std::string & logic_expr2);

        std::string genConditional(const ConditionalParts & cond_parts_if,
                                   const std::vector<ConditionalParts> & cond_parts_elifs,
                                   const ConditionalParts & cond_parts_else);
    
        std::string genLogicExprCompExprEqual(const std::string & expr1, 
                                              const std::string & expr2);
    
        std::string genLogicExprCompExprNotEqual(const std::string & expr1, 
                                                 const std::string & expr2);
    
        std::string genLogicExprCompExprLessThan(const std::string & expr1, 
                                                 const std::string & expr2);
    
        std::string genLogicExprCompExprLessOrEqualThan(const std::string & expr1, 
                                                        const std::string & expr2);
    
        std::string genLogicExprCompExprGreaterThan(const std::string & expr1,
                                                    const std::string & expr2);
    
        std::string genLogicExprCompExprGreaterOrEqualThan(const std::string & expr1,
                                                           const std::string & expr2);
    
        std::string genLogicExprNot(const std::string & logic_expr);
        std::string genLogicExprOr(const std::string & logic_expr1, const std::string & logic_expr2);
        std::string genLogicExprAnd(const std::string & logic_expr1, const std::string & logic_expr2);
    
        std::string genLoop(const Symbol & symbol_loopvar,
                            const std::string & loopvar_value_beg,
                            const std::string & loopvar_value_end,
                            const std::string & loopvar_value_step,
                            const std::string & block);
        
        std::string genStoreTextAndPutAddrIntoA0(const std::string & text);

    private:
        std::ofstream out_file;
        
        // text is inserted into to the end of the file (to the .data part)
        // so we need to store it until then
        std::vector<std::string> texts;
    
        // the stack grows on function call because of these
        static constexpr int step_for_old_fp { 1 };
        static constexpr int step_for_ra     { 1 };
        static constexpr int offset_in_frame_before_params {step_for_old_fp + step_for_ra };
        
        // TODO extract it to a class
        // these IDs help generating unique label names for the different types of branches
        int id_conditional { 0 };
        int getNextIdConditional();
        int id_loop { 0 };
        int getNextIdLoop();
        int id_text { 0 };
        int getNextIdText();
        
        std::string genStdFunctions();
        std::string genStdPrintInt();
        std::string genStdReadInt();
        std::string genStdDebugInt_a0();
        std::string genStdPrintString();
    
        std::string genConditionalPart(const ConditionalParts & cond_parts,
                                       const int id_cond,
                                       const int id_cond_part);
        std::string genConditionalEndLabelName(const int id_cond);
        
        std::string genLogicExprBase(const std::string & logic_expr1,
                                     const std::string & logic_expr2);
        
        std::string genLabelAndText(const std::string & label,
                                    const std::string & text);
    };
    
} // agglunyelv

#endif //AGGLUNYELV_CODEGENERATOR_H
