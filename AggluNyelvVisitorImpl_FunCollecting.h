#ifndef AGGLUNYELV_AGGLUNYELVVISITORIMPL_FUNCOLLECTING_H
#define AGGLUNYELV_AGGLUNYELVVISITORIMPL_FUNCOLLECTING_H

#include "AggluNyelvVisitorImpl.h"

namespace agglunyelv
{
    
    class AggluNyelvVisitorImpl_FunCollecting : public AggluNyelvVisitorImpl
    {
    public:
        virtual std::any visitProg(AggluNyelvParser::ProgContext * ctx) override;

        virtual std::any visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * ctx) override;
        virtual std::any visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * ctx) override;
    
    private:
        void insertStdFunctions();
        void insertStdPrintInt();
        void insertStdReadInt();
        void insertStdPrintString();
    };
    
} // agglunyelv

#endif //AGGLUNYELV_AGGLUNYELVVISITORIMPL_FUNCOLLECTING_H
