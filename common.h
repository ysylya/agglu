#ifndef AGGLUNYELV_COMMON_H
#define AGGLUNYELV_COMMON_H

#include "AggluNyelvBaseVisitor.h"

#include <iostream>


template <typename T>
inline void log_line(const T & to_be_logged)
{
    std::cout << to_be_logged << std::endl;
}

template <typename T, typename ... U>
inline void log_line(const T & to_be_logged, const U & ... more_to_be_logged)
{
    std::cout << to_be_logged;
    log_line(more_to_be_logged ...);
}


template <typename T>
inline void log_error(const T & to_be_logged)
{
    std::cerr << to_be_logged << std::endl;
}

template <
        typename T,
        typename std::enable_if_t<
                not std::is_base_of_v<
                        antlr4::ParserRuleContext,
                        std::remove_pointer_t<T>
                >, int> = 0,
        typename ... Args
>
inline void log_error(const T & to_be_logged, const Args & ... more_to_be_logged)
{
    std::cerr << to_be_logged;
    log_error(more_to_be_logged ...);
}

template <typename ... Args>
inline void log_error(const antlr4::ParserRuleContext * ctx, const Args & ... more_to_be_logged)
{
    std::cerr
        << "Hiba ("
        << ctx->start->getLine()
        << ". sor, "
        << (ctx->start->getCharPositionInLine() + 1)
        << ". karakter): ";
    
    log_error(more_to_be_logged ...);
}

inline bool getIsMutableFromStdString(const std::string & str)
{
    return str == std::string{"változó"};
}

inline std::string prefixFunctionType()
{
    return "__fun:";
}

inline std::string prefixStd()
{
    return "alap.";
}

static inline const std::string FUN_STD_PRINT_INT { prefixStd() + "irj_ki_egeszt" };
static inline const std::string FUN_STD_READ_INT  { prefixStd() + "olvass_be_egeszt" };
static inline const std::string FUN_STD_PRINT_STR { prefixStd() + "irj_ki_szoveget" };


inline std::string symbolTypeInt()
{
    return "egész";
}


#endif //AGGLUNYELV_COMMON_H
