#include "AggluNyelvVisitorImpl_FunCollecting.h"

namespace agglunyelv
{
    std::any AggluNyelvVisitorImpl_FunCollecting::visitProg(AggluNyelvParser::ProgContext * ctx)
    {
        insertStdFunctions();
        
        for (antlr4::tree::ParseTree * fun : ctx->functionDefinition())
        {
            visit(fun);
        }
    
        if (ctx->functionDefinitionMain().empty())
        {
            log_error("Nem található kezdjük!() függvény.");
        }
//        else if (ctx->functionDefinitionMain().size() > 1)
//        {
//            log_error("Már van egy kezdjük!() függvényed.");
//        }
        else
        {
            // detecting more than one main in the visitor to get the line numbers as well
            for (antlr4::tree::ParseTree * fun : ctx->functionDefinitionMain())
            {
                visit(fun);
            }
        }

//        symbol_table.print();
        return symbol_table;
    }
    
    void AggluNyelvVisitorImpl_FunCollecting::insertStdFunctions()
    {
        insertStdPrintInt();
        insertStdReadInt();
        insertStdPrintString();
    }
    
    void AggluNyelvVisitorImpl_FunCollecting::insertStdPrintInt()
    {
        Symbol symbol_std_print_int {
                .name = FUN_STD_PRINT_INT,
                .type = prefixFunctionType(),
                .is_mutable = true,
                .arr_size = 1,
                .num_of_params = 1
        };
        
        bool success { symbol_table.pushToCurrentScope(symbol_std_print_int) };
        if (success == false)
        {
            log_error("Az ", FUN_STD_PRINT_INT, " függvény beillesztése nem sikerült.");
        }
    }
    
    void AggluNyelvVisitorImpl_FunCollecting::insertStdReadInt()
    {
        Symbol symbol_std_read_int {
                .name = FUN_STD_READ_INT,
                .type = prefixFunctionType(),
                .is_mutable = true,
                .arr_size = 1,
                .num_of_params = 0
        };
        
        bool success { symbol_table.pushToCurrentScope(symbol_std_read_int) };
        if (success == false)
        {
            log_error("Az ", FUN_STD_READ_INT, " függvény beillesztése nem sikerült.");
        }
    }
    
    void AggluNyelvVisitorImpl_FunCollecting::insertStdPrintString()
    {
        Symbol symbol_std_print_string {
                .name = FUN_STD_PRINT_STR,
                .type = prefixFunctionType(),
                .is_mutable = true,
                .arr_size = 1,
                .num_of_params = 1
        };
        
        bool success { symbol_table.pushToCurrentScope(symbol_std_print_string) };
        if (success == false)
        {
            log_error("Az ", FUN_STD_PRINT_STR, " függvény beillesztése nem sikerült.");
        }
    }
    
    
    std::any AggluNyelvVisitorImpl_FunCollecting::visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * ctx)
    {
        const std::string fun_name { ctx->SYMBOL_NAME()->toString() };
        
        std::string fun_type {prefixFunctionType() };
        bool fun_mut { true };
        
        if (ctx->functionReturnProperties() && ctx->functionReturnProperties()->variableDeclarationProperties())
        {
            AggluNyelvParser::VariableDeclarationPropertiesContext * varProps {
                    ctx->functionReturnProperties()->variableDeclarationProperties()
            };
            
            fun_type += varProps->TYPE() ? varProps->TYPE()->toString() : varProps->SYMBOL_NAME()->toString();
            fun_mut  = getIsMutableFromStdString(varProps->CONSTNESS()->toString());
        }
        
        const int fun_num_of_params { static_cast<int>(ctx->functionParameters()->variableDeclaration().size()) };
        
        Symbol symbol_fun {
                .name = fun_name,
                .type = fun_type,
                .is_mutable = fun_mut,
                .arr_size = 1,
                .num_of_params = fun_num_of_params
        };
        
        std::string result;
        bool success { symbol_table.pushToCurrentScope(symbol_fun) };
        if (success == false)
        {
            log_error(ctx, "Szimbólumnév \"", fun_name, "\" már használatban van.");
        }
        
        return result;
    }
    
    
    std::any AggluNyelvVisitorImpl_FunCollecting::visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * ctx)
    {
        Symbol symbol_main {
                .name = "main",
                .type = prefixFunctionType() + "int",
                .is_mutable = true
        };
        
        std::string result;
        bool success { symbol_table.pushToCurrentScope(symbol_main) };
        if (success == false)
        {
            log_error(ctx, "Már van egy kezdjük!() függvényed.");
        }
        
        return result;
    }
    
} // agglunyelv