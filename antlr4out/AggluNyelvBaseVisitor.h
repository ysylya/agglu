
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1

#pragma once


#include "antlr4-runtime.h"
#include "AggluNyelvVisitor.h"


/**
 * This class provides an empty implementation of AggluNyelvVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  AggluNyelvBaseVisitor : public AggluNyelvVisitor {
public:

  virtual std::any visitProg(AggluNyelvParser::ProgContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStructDefinition(AggluNyelvParser::StructDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionArgument(AggluNyelvParser::FunctionArgumentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionCall(AggluNyelvParser::FunctionCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionParameters(AggluNyelvParser::FunctionParametersContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionReturnProperties(AggluNyelvParser::FunctionReturnPropertiesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionReturn(AggluNyelvParser::FunctionReturnContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionBlock(AggluNyelvParser::FunctionBlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBlock(AggluNyelvParser::BlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBlockPart(AggluNyelvParser::BlockPartContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitConditional(AggluNyelvParser::ConditionalContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitConditionalIf(AggluNyelvParser::ConditionalIfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitConditionalElif(AggluNyelvParser::ConditionalElifContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitConditionalElse(AggluNyelvParser::ConditionalElseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLoop(AggluNyelvParser::LoopContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStatementBody(AggluNyelvParser::StatementBodyContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStatement(AggluNyelvParser::StatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprNot(AggluNyelvParser::LogicExprNotContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprBoolean(AggluNyelvParser::LogicExprBooleanContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprCompare(AggluNyelvParser::LogicExprCompareContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprAnd(AggluNyelvParser::LogicExprAndContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprOr(AggluNyelvParser::LogicExprOrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitLogicExprFun(AggluNyelvParser::LogicExprFunContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitOpLogicalComparison(AggluNyelvParser::OpLogicalComparisonContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitOpComparison(AggluNyelvParser::OpComparisonContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpInt(AggluNyelvParser::ExprGrpIntContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpFun(AggluNyelvParser::ExprGrpFunContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpArr(AggluNyelvParser::ExprGrpArrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpPar(AggluNyelvParser::ExprGrpParContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpSym(AggluNyelvParser::ExprGrpSymContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExprGrpMul(AggluNyelvParser::ExprGrpMulContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableDefinition(AggluNyelvParser::VariableDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableAssignment(AggluNyelvParser::VariableAssignmentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableValue(AggluNyelvParser::VariableValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBoolean(AggluNyelvParser::BooleanContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitText(AggluNyelvParser::TextContext *ctx) override {
    return visitChildren(ctx);
  }


};

