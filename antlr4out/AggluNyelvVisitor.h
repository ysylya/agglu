
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1

#pragma once


#include "antlr4-runtime.h"
#include "AggluNyelvParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by AggluNyelvParser.
 */
class  AggluNyelvVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by AggluNyelvParser.
   */
    virtual std::any visitProg(AggluNyelvParser::ProgContext *context) = 0;

    virtual std::any visitStructDefinition(AggluNyelvParser::StructDefinitionContext *context) = 0;

    virtual std::any visitFunctionArgument(AggluNyelvParser::FunctionArgumentContext *context) = 0;

    virtual std::any visitFunctionCall(AggluNyelvParser::FunctionCallContext *context) = 0;

    virtual std::any visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext *context) = 0;

    virtual std::any visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext *context) = 0;

    virtual std::any visitFunctionParameters(AggluNyelvParser::FunctionParametersContext *context) = 0;

    virtual std::any visitFunctionReturnProperties(AggluNyelvParser::FunctionReturnPropertiesContext *context) = 0;

    virtual std::any visitFunctionReturn(AggluNyelvParser::FunctionReturnContext *context) = 0;

    virtual std::any visitFunctionBlock(AggluNyelvParser::FunctionBlockContext *context) = 0;

    virtual std::any visitBlock(AggluNyelvParser::BlockContext *context) = 0;

    virtual std::any visitBlockPart(AggluNyelvParser::BlockPartContext *context) = 0;

    virtual std::any visitConditional(AggluNyelvParser::ConditionalContext *context) = 0;

    virtual std::any visitConditionalIf(AggluNyelvParser::ConditionalIfContext *context) = 0;

    virtual std::any visitConditionalElif(AggluNyelvParser::ConditionalElifContext *context) = 0;

    virtual std::any visitConditionalElse(AggluNyelvParser::ConditionalElseContext *context) = 0;

    virtual std::any visitLoop(AggluNyelvParser::LoopContext *context) = 0;

    virtual std::any visitLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext *context) = 0;

    virtual std::any visitLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext *context) = 0;

    virtual std::any visitLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext *context) = 0;

    virtual std::any visitLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext *context) = 0;

    virtual std::any visitStatementBody(AggluNyelvParser::StatementBodyContext *context) = 0;

    virtual std::any visitStatement(AggluNyelvParser::StatementContext *context) = 0;

    virtual std::any visitLogicExprNot(AggluNyelvParser::LogicExprNotContext *context) = 0;

    virtual std::any visitLogicExprBoolean(AggluNyelvParser::LogicExprBooleanContext *context) = 0;

    virtual std::any visitLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext *context) = 0;

    virtual std::any visitLogicExprCompare(AggluNyelvParser::LogicExprCompareContext *context) = 0;

    virtual std::any visitLogicExprAnd(AggluNyelvParser::LogicExprAndContext *context) = 0;

    virtual std::any visitLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext *context) = 0;

    virtual std::any visitLogicExprOr(AggluNyelvParser::LogicExprOrContext *context) = 0;

    virtual std::any visitLogicExprFun(AggluNyelvParser::LogicExprFunContext *context) = 0;

    virtual std::any visitOpLogicalComparison(AggluNyelvParser::OpLogicalComparisonContext *context) = 0;

    virtual std::any visitOpComparison(AggluNyelvParser::OpComparisonContext *context) = 0;

    virtual std::any visitExprGrpInt(AggluNyelvParser::ExprGrpIntContext *context) = 0;

    virtual std::any visitExprGrpFun(AggluNyelvParser::ExprGrpFunContext *context) = 0;

    virtual std::any visitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext *context) = 0;

    virtual std::any visitExprGrpArr(AggluNyelvParser::ExprGrpArrContext *context) = 0;

    virtual std::any visitExprGrpPar(AggluNyelvParser::ExprGrpParContext *context) = 0;

    virtual std::any visitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext *context) = 0;

    virtual std::any visitExprGrpSym(AggluNyelvParser::ExprGrpSymContext *context) = 0;

    virtual std::any visitExprGrpMul(AggluNyelvParser::ExprGrpMulContext *context) = 0;

    virtual std::any visitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext *context) = 0;

    virtual std::any visitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext *context) = 0;

    virtual std::any visitVariableDefinition(AggluNyelvParser::VariableDefinitionContext *context) = 0;

    virtual std::any visitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext *context) = 0;

    virtual std::any visitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext *context) = 0;

    virtual std::any visitVariableAssignment(AggluNyelvParser::VariableAssignmentContext *context) = 0;

    virtual std::any visitVariableValue(AggluNyelvParser::VariableValueContext *context) = 0;

    virtual std::any visitBoolean(AggluNyelvParser::BooleanContext *context) = 0;

    virtual std::any visitText(AggluNyelvParser::TextContext *context) = 0;


};

