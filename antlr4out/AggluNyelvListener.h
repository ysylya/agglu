
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1

#pragma once


#include "antlr4-runtime.h"
#include "AggluNyelvParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by AggluNyelvParser.
 */
class  AggluNyelvListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterProg(AggluNyelvParser::ProgContext *ctx) = 0;
  virtual void exitProg(AggluNyelvParser::ProgContext *ctx) = 0;

  virtual void enterStructDefinition(AggluNyelvParser::StructDefinitionContext *ctx) = 0;
  virtual void exitStructDefinition(AggluNyelvParser::StructDefinitionContext *ctx) = 0;

  virtual void enterFunctionArgument(AggluNyelvParser::FunctionArgumentContext *ctx) = 0;
  virtual void exitFunctionArgument(AggluNyelvParser::FunctionArgumentContext *ctx) = 0;

  virtual void enterFunctionCall(AggluNyelvParser::FunctionCallContext *ctx) = 0;
  virtual void exitFunctionCall(AggluNyelvParser::FunctionCallContext *ctx) = 0;

  virtual void enterFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext *ctx) = 0;
  virtual void exitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext *ctx) = 0;

  virtual void enterFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext *ctx) = 0;
  virtual void exitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext *ctx) = 0;

  virtual void enterFunctionParameters(AggluNyelvParser::FunctionParametersContext *ctx) = 0;
  virtual void exitFunctionParameters(AggluNyelvParser::FunctionParametersContext *ctx) = 0;

  virtual void enterFunctionReturnProperties(AggluNyelvParser::FunctionReturnPropertiesContext *ctx) = 0;
  virtual void exitFunctionReturnProperties(AggluNyelvParser::FunctionReturnPropertiesContext *ctx) = 0;

  virtual void enterFunctionReturn(AggluNyelvParser::FunctionReturnContext *ctx) = 0;
  virtual void exitFunctionReturn(AggluNyelvParser::FunctionReturnContext *ctx) = 0;

  virtual void enterFunctionBlock(AggluNyelvParser::FunctionBlockContext *ctx) = 0;
  virtual void exitFunctionBlock(AggluNyelvParser::FunctionBlockContext *ctx) = 0;

  virtual void enterBlock(AggluNyelvParser::BlockContext *ctx) = 0;
  virtual void exitBlock(AggluNyelvParser::BlockContext *ctx) = 0;

  virtual void enterBlockPart(AggluNyelvParser::BlockPartContext *ctx) = 0;
  virtual void exitBlockPart(AggluNyelvParser::BlockPartContext *ctx) = 0;

  virtual void enterConditional(AggluNyelvParser::ConditionalContext *ctx) = 0;
  virtual void exitConditional(AggluNyelvParser::ConditionalContext *ctx) = 0;

  virtual void enterConditionalIf(AggluNyelvParser::ConditionalIfContext *ctx) = 0;
  virtual void exitConditionalIf(AggluNyelvParser::ConditionalIfContext *ctx) = 0;

  virtual void enterConditionalElif(AggluNyelvParser::ConditionalElifContext *ctx) = 0;
  virtual void exitConditionalElif(AggluNyelvParser::ConditionalElifContext *ctx) = 0;

  virtual void enterConditionalElse(AggluNyelvParser::ConditionalElseContext *ctx) = 0;
  virtual void exitConditionalElse(AggluNyelvParser::ConditionalElseContext *ctx) = 0;

  virtual void enterLoop(AggluNyelvParser::LoopContext *ctx) = 0;
  virtual void exitLoop(AggluNyelvParser::LoopContext *ctx) = 0;

  virtual void enterLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext *ctx) = 0;
  virtual void exitLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext *ctx) = 0;

  virtual void enterLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext *ctx) = 0;
  virtual void exitLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext *ctx) = 0;

  virtual void enterLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext *ctx) = 0;
  virtual void exitLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext *ctx) = 0;

  virtual void enterLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext *ctx) = 0;
  virtual void exitLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext *ctx) = 0;

  virtual void enterStatementBody(AggluNyelvParser::StatementBodyContext *ctx) = 0;
  virtual void exitStatementBody(AggluNyelvParser::StatementBodyContext *ctx) = 0;

  virtual void enterStatement(AggluNyelvParser::StatementContext *ctx) = 0;
  virtual void exitStatement(AggluNyelvParser::StatementContext *ctx) = 0;

  virtual void enterLogicExprNot(AggluNyelvParser::LogicExprNotContext *ctx) = 0;
  virtual void exitLogicExprNot(AggluNyelvParser::LogicExprNotContext *ctx) = 0;

  virtual void enterLogicExprBoolean(AggluNyelvParser::LogicExprBooleanContext *ctx) = 0;
  virtual void exitLogicExprBoolean(AggluNyelvParser::LogicExprBooleanContext *ctx) = 0;

  virtual void enterLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext *ctx) = 0;
  virtual void exitLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext *ctx) = 0;

  virtual void enterLogicExprCompare(AggluNyelvParser::LogicExprCompareContext *ctx) = 0;
  virtual void exitLogicExprCompare(AggluNyelvParser::LogicExprCompareContext *ctx) = 0;

  virtual void enterLogicExprAnd(AggluNyelvParser::LogicExprAndContext *ctx) = 0;
  virtual void exitLogicExprAnd(AggluNyelvParser::LogicExprAndContext *ctx) = 0;

  virtual void enterLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext *ctx) = 0;
  virtual void exitLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext *ctx) = 0;

  virtual void enterLogicExprOr(AggluNyelvParser::LogicExprOrContext *ctx) = 0;
  virtual void exitLogicExprOr(AggluNyelvParser::LogicExprOrContext *ctx) = 0;

  virtual void enterLogicExprFun(AggluNyelvParser::LogicExprFunContext *ctx) = 0;
  virtual void exitLogicExprFun(AggluNyelvParser::LogicExprFunContext *ctx) = 0;

  virtual void enterOpLogicalComparison(AggluNyelvParser::OpLogicalComparisonContext *ctx) = 0;
  virtual void exitOpLogicalComparison(AggluNyelvParser::OpLogicalComparisonContext *ctx) = 0;

  virtual void enterOpComparison(AggluNyelvParser::OpComparisonContext *ctx) = 0;
  virtual void exitOpComparison(AggluNyelvParser::OpComparisonContext *ctx) = 0;

  virtual void enterExprGrpInt(AggluNyelvParser::ExprGrpIntContext *ctx) = 0;
  virtual void exitExprGrpInt(AggluNyelvParser::ExprGrpIntContext *ctx) = 0;

  virtual void enterExprGrpFun(AggluNyelvParser::ExprGrpFunContext *ctx) = 0;
  virtual void exitExprGrpFun(AggluNyelvParser::ExprGrpFunContext *ctx) = 0;

  virtual void enterExprGrpNeg(AggluNyelvParser::ExprGrpNegContext *ctx) = 0;
  virtual void exitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext *ctx) = 0;

  virtual void enterExprGrpArr(AggluNyelvParser::ExprGrpArrContext *ctx) = 0;
  virtual void exitExprGrpArr(AggluNyelvParser::ExprGrpArrContext *ctx) = 0;

  virtual void enterExprGrpPar(AggluNyelvParser::ExprGrpParContext *ctx) = 0;
  virtual void exitExprGrpPar(AggluNyelvParser::ExprGrpParContext *ctx) = 0;

  virtual void enterExprGrpAdd(AggluNyelvParser::ExprGrpAddContext *ctx) = 0;
  virtual void exitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext *ctx) = 0;

  virtual void enterExprGrpSym(AggluNyelvParser::ExprGrpSymContext *ctx) = 0;
  virtual void exitExprGrpSym(AggluNyelvParser::ExprGrpSymContext *ctx) = 0;

  virtual void enterExprGrpMul(AggluNyelvParser::ExprGrpMulContext *ctx) = 0;
  virtual void exitExprGrpMul(AggluNyelvParser::ExprGrpMulContext *ctx) = 0;

  virtual void enterArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext *ctx) = 0;
  virtual void exitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext *ctx) = 0;

  virtual void enterArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext *ctx) = 0;
  virtual void exitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext *ctx) = 0;

  virtual void enterVariableDefinition(AggluNyelvParser::VariableDefinitionContext *ctx) = 0;
  virtual void exitVariableDefinition(AggluNyelvParser::VariableDefinitionContext *ctx) = 0;

  virtual void enterVariableDeclaration(AggluNyelvParser::VariableDeclarationContext *ctx) = 0;
  virtual void exitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext *ctx) = 0;

  virtual void enterVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext *ctx) = 0;
  virtual void exitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext *ctx) = 0;

  virtual void enterVariableAssignment(AggluNyelvParser::VariableAssignmentContext *ctx) = 0;
  virtual void exitVariableAssignment(AggluNyelvParser::VariableAssignmentContext *ctx) = 0;

  virtual void enterVariableValue(AggluNyelvParser::VariableValueContext *ctx) = 0;
  virtual void exitVariableValue(AggluNyelvParser::VariableValueContext *ctx) = 0;

  virtual void enterBoolean(AggluNyelvParser::BooleanContext *ctx) = 0;
  virtual void exitBoolean(AggluNyelvParser::BooleanContext *ctx) = 0;

  virtual void enterText(AggluNyelvParser::TextContext *ctx) = 0;
  virtual void exitText(AggluNyelvParser::TextContext *ctx) = 0;


};

