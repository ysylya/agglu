
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1

#pragma once


#include "antlr4-runtime.h"
#include "AggluNyelvListener.h"


/**
 * This class provides an empty implementation of AggluNyelvListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  AggluNyelvBaseListener : public AggluNyelvListener {
public:

  virtual void enterProg(AggluNyelvParser::ProgContext * /*ctx*/) override { }
  virtual void exitProg(AggluNyelvParser::ProgContext * /*ctx*/) override { }

  virtual void enterStructDefinition(AggluNyelvParser::StructDefinitionContext * /*ctx*/) override { }
  virtual void exitStructDefinition(AggluNyelvParser::StructDefinitionContext * /*ctx*/) override { }

  virtual void enterFunctionArgument(AggluNyelvParser::FunctionArgumentContext * /*ctx*/) override { }
  virtual void exitFunctionArgument(AggluNyelvParser::FunctionArgumentContext * /*ctx*/) override { }

  virtual void enterFunctionCall(AggluNyelvParser::FunctionCallContext * /*ctx*/) override { }
  virtual void exitFunctionCall(AggluNyelvParser::FunctionCallContext * /*ctx*/) override { }

  virtual void enterFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * /*ctx*/) override { }
  virtual void exitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * /*ctx*/) override { }

  virtual void enterFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * /*ctx*/) override { }
  virtual void exitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * /*ctx*/) override { }

  virtual void enterFunctionParameters(AggluNyelvParser::FunctionParametersContext * /*ctx*/) override { }
  virtual void exitFunctionParameters(AggluNyelvParser::FunctionParametersContext * /*ctx*/) override { }

  virtual void enterFunctionReturnProperties(AggluNyelvParser::FunctionReturnPropertiesContext * /*ctx*/) override { }
  virtual void exitFunctionReturnProperties(AggluNyelvParser::FunctionReturnPropertiesContext * /*ctx*/) override { }

  virtual void enterFunctionReturn(AggluNyelvParser::FunctionReturnContext * /*ctx*/) override { }
  virtual void exitFunctionReturn(AggluNyelvParser::FunctionReturnContext * /*ctx*/) override { }

  virtual void enterFunctionBlock(AggluNyelvParser::FunctionBlockContext * /*ctx*/) override { }
  virtual void exitFunctionBlock(AggluNyelvParser::FunctionBlockContext * /*ctx*/) override { }

  virtual void enterBlock(AggluNyelvParser::BlockContext * /*ctx*/) override { }
  virtual void exitBlock(AggluNyelvParser::BlockContext * /*ctx*/) override { }

  virtual void enterBlockPart(AggluNyelvParser::BlockPartContext * /*ctx*/) override { }
  virtual void exitBlockPart(AggluNyelvParser::BlockPartContext * /*ctx*/) override { }

  virtual void enterConditional(AggluNyelvParser::ConditionalContext * /*ctx*/) override { }
  virtual void exitConditional(AggluNyelvParser::ConditionalContext * /*ctx*/) override { }

  virtual void enterConditionalIf(AggluNyelvParser::ConditionalIfContext * /*ctx*/) override { }
  virtual void exitConditionalIf(AggluNyelvParser::ConditionalIfContext * /*ctx*/) override { }

  virtual void enterConditionalElif(AggluNyelvParser::ConditionalElifContext * /*ctx*/) override { }
  virtual void exitConditionalElif(AggluNyelvParser::ConditionalElifContext * /*ctx*/) override { }

  virtual void enterConditionalElse(AggluNyelvParser::ConditionalElseContext * /*ctx*/) override { }
  virtual void exitConditionalElse(AggluNyelvParser::ConditionalElseContext * /*ctx*/) override { }

  virtual void enterLoop(AggluNyelvParser::LoopContext * /*ctx*/) override { }
  virtual void exitLoop(AggluNyelvParser::LoopContext * /*ctx*/) override { }

  virtual void enterLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext * /*ctx*/) override { }
  virtual void exitLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext * /*ctx*/) override { }

  virtual void enterLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext * /*ctx*/) override { }
  virtual void exitLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext * /*ctx*/) override { }

  virtual void enterLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext * /*ctx*/) override { }
  virtual void exitLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext * /*ctx*/) override { }

  virtual void enterLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext * /*ctx*/) override { }
  virtual void exitLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext * /*ctx*/) override { }

  virtual void enterStatementBody(AggluNyelvParser::StatementBodyContext * /*ctx*/) override { }
  virtual void exitStatementBody(AggluNyelvParser::StatementBodyContext * /*ctx*/) override { }

  virtual void enterStatement(AggluNyelvParser::StatementContext * /*ctx*/) override { }
  virtual void exitStatement(AggluNyelvParser::StatementContext * /*ctx*/) override { }

  virtual void enterLogicExprNot(AggluNyelvParser::LogicExprNotContext * /*ctx*/) override { }
  virtual void exitLogicExprNot(AggluNyelvParser::LogicExprNotContext * /*ctx*/) override { }

  virtual void enterLogicExprBoolean(AggluNyelvParser::LogicExprBooleanContext * /*ctx*/) override { }
  virtual void exitLogicExprBoolean(AggluNyelvParser::LogicExprBooleanContext * /*ctx*/) override { }

  virtual void enterLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext * /*ctx*/) override { }
  virtual void exitLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext * /*ctx*/) override { }

  virtual void enterLogicExprCompare(AggluNyelvParser::LogicExprCompareContext * /*ctx*/) override { }
  virtual void exitLogicExprCompare(AggluNyelvParser::LogicExprCompareContext * /*ctx*/) override { }

  virtual void enterLogicExprAnd(AggluNyelvParser::LogicExprAndContext * /*ctx*/) override { }
  virtual void exitLogicExprAnd(AggluNyelvParser::LogicExprAndContext * /*ctx*/) override { }

  virtual void enterLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext * /*ctx*/) override { }
  virtual void exitLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext * /*ctx*/) override { }

  virtual void enterLogicExprOr(AggluNyelvParser::LogicExprOrContext * /*ctx*/) override { }
  virtual void exitLogicExprOr(AggluNyelvParser::LogicExprOrContext * /*ctx*/) override { }

  virtual void enterLogicExprFun(AggluNyelvParser::LogicExprFunContext * /*ctx*/) override { }
  virtual void exitLogicExprFun(AggluNyelvParser::LogicExprFunContext * /*ctx*/) override { }

  virtual void enterOpLogicalComparison(AggluNyelvParser::OpLogicalComparisonContext * /*ctx*/) override { }
  virtual void exitOpLogicalComparison(AggluNyelvParser::OpLogicalComparisonContext * /*ctx*/) override { }

  virtual void enterOpComparison(AggluNyelvParser::OpComparisonContext * /*ctx*/) override { }
  virtual void exitOpComparison(AggluNyelvParser::OpComparisonContext * /*ctx*/) override { }

  virtual void enterExprGrpInt(AggluNyelvParser::ExprGrpIntContext * /*ctx*/) override { }
  virtual void exitExprGrpInt(AggluNyelvParser::ExprGrpIntContext * /*ctx*/) override { }

  virtual void enterExprGrpFun(AggluNyelvParser::ExprGrpFunContext * /*ctx*/) override { }
  virtual void exitExprGrpFun(AggluNyelvParser::ExprGrpFunContext * /*ctx*/) override { }

  virtual void enterExprGrpNeg(AggluNyelvParser::ExprGrpNegContext * /*ctx*/) override { }
  virtual void exitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext * /*ctx*/) override { }

  virtual void enterExprGrpArr(AggluNyelvParser::ExprGrpArrContext * /*ctx*/) override { }
  virtual void exitExprGrpArr(AggluNyelvParser::ExprGrpArrContext * /*ctx*/) override { }

  virtual void enterExprGrpPar(AggluNyelvParser::ExprGrpParContext * /*ctx*/) override { }
  virtual void exitExprGrpPar(AggluNyelvParser::ExprGrpParContext * /*ctx*/) override { }

  virtual void enterExprGrpAdd(AggluNyelvParser::ExprGrpAddContext * /*ctx*/) override { }
  virtual void exitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext * /*ctx*/) override { }

  virtual void enterExprGrpSym(AggluNyelvParser::ExprGrpSymContext * /*ctx*/) override { }
  virtual void exitExprGrpSym(AggluNyelvParser::ExprGrpSymContext * /*ctx*/) override { }

  virtual void enterExprGrpMul(AggluNyelvParser::ExprGrpMulContext * /*ctx*/) override { }
  virtual void exitExprGrpMul(AggluNyelvParser::ExprGrpMulContext * /*ctx*/) override { }

  virtual void enterArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext * /*ctx*/) override { }
  virtual void exitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext * /*ctx*/) override { }

  virtual void enterArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext * /*ctx*/) override { }
  virtual void exitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext * /*ctx*/) override { }

  virtual void enterVariableDefinition(AggluNyelvParser::VariableDefinitionContext * /*ctx*/) override { }
  virtual void exitVariableDefinition(AggluNyelvParser::VariableDefinitionContext * /*ctx*/) override { }

  virtual void enterVariableDeclaration(AggluNyelvParser::VariableDeclarationContext * /*ctx*/) override { }
  virtual void exitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext * /*ctx*/) override { }

  virtual void enterVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext * /*ctx*/) override { }
  virtual void exitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext * /*ctx*/) override { }

  virtual void enterVariableAssignment(AggluNyelvParser::VariableAssignmentContext * /*ctx*/) override { }
  virtual void exitVariableAssignment(AggluNyelvParser::VariableAssignmentContext * /*ctx*/) override { }

  virtual void enterVariableValue(AggluNyelvParser::VariableValueContext * /*ctx*/) override { }
  virtual void exitVariableValue(AggluNyelvParser::VariableValueContext * /*ctx*/) override { }

  virtual void enterBoolean(AggluNyelvParser::BooleanContext * /*ctx*/) override { }
  virtual void exitBoolean(AggluNyelvParser::BooleanContext * /*ctx*/) override { }

  virtual void enterText(AggluNyelvParser::TextContext * /*ctx*/) override { }
  virtual void exitText(AggluNyelvParser::TextContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

