
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1


#include "AggluNyelvLexer.h"


using namespace antlr4;



using namespace antlr4;

namespace {

struct AggluNyelvLexerStaticData final {
  AggluNyelvLexerStaticData(std::vector<std::string> ruleNames,
                          std::vector<std::string> channelNames,
                          std::vector<std::string> modeNames,
                          std::vector<std::string> literalNames,
                          std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), channelNames(std::move(channelNames)),
        modeNames(std::move(modeNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  AggluNyelvLexerStaticData(const AggluNyelvLexerStaticData&) = delete;
  AggluNyelvLexerStaticData(AggluNyelvLexerStaticData&&) = delete;
  AggluNyelvLexerStaticData& operator=(const AggluNyelvLexerStaticData&) = delete;
  AggluNyelvLexerStaticData& operator=(AggluNyelvLexerStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> channelNames;
  const std::vector<std::string> modeNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

std::once_flag agglunyelvlexerLexerOnceFlag;
AggluNyelvLexerStaticData *agglunyelvlexerLexerStaticData = nullptr;

void agglunyelvlexerLexerInitialize() {
  assert(agglunyelvlexerLexerStaticData == nullptr);
  auto staticData = std::make_unique<AggluNyelvLexerStaticData>(
    std::vector<std::string>{
      "COMMENT", "BLOCK_OPEN", "BLOCK_CLOSE", "OP_ASSIGN", "OP_LISTING", 
      "OP_DEFAULT_VALUE", "OP_DOT", "OP_ARR_ELEM_SELECTOR_DECOR", "OP_COMP_EQUALS", 
      "OP_COMP_NOTEQUALS", "OP_COMP_LESS_THAN", "OP_COMP_LESS_OR_EQUAL_THAN", 
      "OP_COMP_GREATER_THAN", "OP_COMP_GREATER_OR_EQUAL_THAN", "OP_EXPONENTATION", 
      "OP_MULTIPLICATION", "OP_DIVISION", "OP_ADDITION", "OP_SUBTRACTION", 
      "OP_MODULO", "TERMINATOR", "PREFIX_STD", "TYPE", "CONSTNESS", "FUN_DECOR", 
      "FUN_MAIN_NAME", "PARENTHESIS_OPEN", "PARENTHESIS_CLOSE", "FUN_RETURN", 
      "STRUCT_DECLR", "LOOPVAR_VALUE_DEF_BEG", "LOOPVAR_VALUE_DEF_END", 
      "LOOPVAR_VALUE_BEG", "LOOPVAR_VALUE_END", "LOOPVAR_VALUE_STEP", "CONDITIONAL_IF", 
      "CONDITIONAL_ELSE", "LOGICAL_AND", "LOGICAL_OR", "LOGICAL_NOT", "NEWLINE", 
      "WHITESPACE", "ESC_QUOTATION_MARK", "BOOLEAN_TRUE", "BOOLEAN_FALSE", 
      "INTEGER", "TEXT", "SYMBOL_NAME", "VOWEL_SMALL"
    },
    std::vector<std::string>{
      "DEFAULT_TOKEN_CHANNEL", "HIDDEN"
    },
    std::vector<std::string>{
      "DEFAULT_MODE"
    },
    std::vector<std::string>{
      "", "", "'{'", "'}'", "':='", "','", "'...'", "'.'", "'eleme'", "'=='", 
      "'!='", "'<'", "'<='", "'>'", "'>='", "'^'", "'*'", "'/'", "'+'", 
      "'-'", "'%'", "';'", "'alap.'", "", "", "'!'", "'kezdj\\u00FCk'", 
      "'('", "')'", "'eredm\\u00E9ny'", "'tervrajza'", "'minden'", "", "", 
      "'-ig'", "", "'ha'", "'am\\u00FAgy'", "'\\u00E9s'", "'vagy'", "'nem'", 
      "", "' '", "'\\\"'", "'igaz'", "'hamis'"
    },
    std::vector<std::string>{
      "", "COMMENT", "BLOCK_OPEN", "BLOCK_CLOSE", "OP_ASSIGN", "OP_LISTING", 
      "OP_DEFAULT_VALUE", "OP_DOT", "OP_ARR_ELEM_SELECTOR_DECOR", "OP_COMP_EQUALS", 
      "OP_COMP_NOTEQUALS", "OP_COMP_LESS_THAN", "OP_COMP_LESS_OR_EQUAL_THAN", 
      "OP_COMP_GREATER_THAN", "OP_COMP_GREATER_OR_EQUAL_THAN", "OP_EXPONENTATION", 
      "OP_MULTIPLICATION", "OP_DIVISION", "OP_ADDITION", "OP_SUBTRACTION", 
      "OP_MODULO", "TERMINATOR", "PREFIX_STD", "TYPE", "CONSTNESS", "FUN_DECOR", 
      "FUN_MAIN_NAME", "PARENTHESIS_OPEN", "PARENTHESIS_CLOSE", "FUN_RETURN", 
      "STRUCT_DECLR", "LOOPVAR_VALUE_DEF_BEG", "LOOPVAR_VALUE_DEF_END", 
      "LOOPVAR_VALUE_BEG", "LOOPVAR_VALUE_END", "LOOPVAR_VALUE_STEP", "CONDITIONAL_IF", 
      "CONDITIONAL_ELSE", "LOGICAL_AND", "LOGICAL_OR", "LOGICAL_NOT", "NEWLINE", 
      "WHITESPACE", "ESC_QUOTATION_MARK", "BOOLEAN_TRUE", "BOOLEAN_FALSE", 
      "INTEGER", "TEXT", "SYMBOL_NAME"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,0,48,351,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
  	6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,2,14,
  	7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,7,20,2,21,
  	7,21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,2,27,7,27,2,28,
  	7,28,2,29,7,29,2,30,7,30,2,31,7,31,2,32,7,32,2,33,7,33,2,34,7,34,2,35,
  	7,35,2,36,7,36,2,37,7,37,2,38,7,38,2,39,7,39,2,40,7,40,2,41,7,41,2,42,
  	7,42,2,43,7,43,2,44,7,44,2,45,7,45,2,46,7,46,2,47,7,47,2,48,7,48,1,0,
  	1,0,5,0,102,8,0,10,0,12,0,105,9,0,1,0,1,0,1,1,1,1,1,2,1,2,1,3,1,3,1,3,
  	1,4,1,4,1,5,1,5,1,5,1,5,1,6,1,6,1,7,1,7,1,7,1,7,1,7,1,7,1,8,1,8,1,8,1,
  	9,1,9,1,9,1,10,1,10,1,11,1,11,1,11,1,12,1,12,1,13,1,13,1,13,1,14,1,14,
  	1,15,1,15,1,16,1,16,1,17,1,17,1,18,1,18,1,19,1,19,1,20,1,20,1,21,1,21,
  	1,21,1,21,1,21,1,21,1,22,1,22,1,22,1,22,1,22,1,22,1,22,1,22,1,22,1,22,
  	1,22,1,22,1,22,1,22,1,22,1,22,1,22,1,22,1,22,1,22,3,22,186,8,22,1,23,
  	1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,3,23,
  	202,8,23,1,24,1,24,1,25,1,25,1,25,1,25,1,25,1,25,1,25,1,25,1,26,1,26,
  	1,27,1,27,1,28,1,28,1,28,1,28,1,28,1,28,1,28,1,28,1,28,1,29,1,29,1,29,
  	1,29,1,29,1,29,1,29,1,29,1,29,1,29,1,30,1,30,1,30,1,30,1,30,1,30,1,30,
  	1,31,1,31,1,31,1,31,1,31,1,31,3,31,250,8,31,1,32,1,32,1,32,1,32,1,32,
  	1,32,1,32,1,32,3,32,260,8,32,1,33,1,33,1,33,1,33,1,34,1,34,1,34,1,34,
  	1,34,1,34,1,34,1,34,1,34,1,34,3,34,276,8,34,1,35,1,35,1,35,1,36,1,36,
  	1,36,1,36,1,36,1,36,1,37,1,37,1,37,1,38,1,38,1,38,1,38,1,38,1,39,1,39,
  	1,39,1,39,1,40,4,40,300,8,40,11,40,12,40,301,1,40,1,40,1,41,1,41,1,41,
  	1,41,1,42,1,42,1,42,1,43,1,43,1,43,1,43,1,43,1,44,1,44,1,44,1,44,1,44,
  	1,44,1,45,4,45,325,8,45,11,45,12,45,326,1,46,1,46,1,46,5,46,332,8,46,
  	10,46,12,46,335,9,46,1,46,1,46,1,47,4,47,340,8,47,11,47,12,47,341,1,47,
  	5,47,345,8,47,10,47,12,47,348,9,47,1,48,1,48,1,333,0,49,1,1,3,2,5,3,7,
  	4,9,5,11,6,13,7,15,8,17,9,19,10,21,11,23,12,25,13,27,14,29,15,31,16,33,
  	17,35,18,37,19,39,20,41,21,43,22,45,23,47,24,49,25,51,26,53,27,55,28,
  	57,29,59,30,61,31,63,32,65,33,67,34,69,35,71,36,73,37,75,38,77,39,79,
  	40,81,41,83,42,85,43,87,44,89,45,91,46,93,47,95,48,97,0,1,0,6,1,0,10,
  	10,2,0,10,10,13,13,1,0,48,57,19,0,65,90,95,95,97,122,193,193,201,201,
  	205,205,211,211,214,214,218,218,220,220,225,225,233,233,237,237,243,243,
  	246,246,250,250,252,252,336,337,368,369,20,0,48,57,65,90,95,95,97,122,
  	193,193,201,201,205,205,211,211,214,214,218,218,220,220,225,225,233,233,
  	237,237,243,243,246,246,250,250,252,252,336,337,368,369,14,0,97,97,101,
  	101,105,105,111,111,117,117,225,225,233,233,237,237,243,243,246,246,250,
  	250,252,252,337,337,369,369,362,0,1,1,0,0,0,0,3,1,0,0,0,0,5,1,0,0,0,0,
  	7,1,0,0,0,0,9,1,0,0,0,0,11,1,0,0,0,0,13,1,0,0,0,0,15,1,0,0,0,0,17,1,0,
  	0,0,0,19,1,0,0,0,0,21,1,0,0,0,0,23,1,0,0,0,0,25,1,0,0,0,0,27,1,0,0,0,
  	0,29,1,0,0,0,0,31,1,0,0,0,0,33,1,0,0,0,0,35,1,0,0,0,0,37,1,0,0,0,0,39,
  	1,0,0,0,0,41,1,0,0,0,0,43,1,0,0,0,0,45,1,0,0,0,0,47,1,0,0,0,0,49,1,0,
  	0,0,0,51,1,0,0,0,0,53,1,0,0,0,0,55,1,0,0,0,0,57,1,0,0,0,0,59,1,0,0,0,
  	0,61,1,0,0,0,0,63,1,0,0,0,0,65,1,0,0,0,0,67,1,0,0,0,0,69,1,0,0,0,0,71,
  	1,0,0,0,0,73,1,0,0,0,0,75,1,0,0,0,0,77,1,0,0,0,0,79,1,0,0,0,0,81,1,0,
  	0,0,0,83,1,0,0,0,0,85,1,0,0,0,0,87,1,0,0,0,0,89,1,0,0,0,0,91,1,0,0,0,
  	0,93,1,0,0,0,0,95,1,0,0,0,1,99,1,0,0,0,3,108,1,0,0,0,5,110,1,0,0,0,7,
  	112,1,0,0,0,9,115,1,0,0,0,11,117,1,0,0,0,13,121,1,0,0,0,15,123,1,0,0,
  	0,17,129,1,0,0,0,19,132,1,0,0,0,21,135,1,0,0,0,23,137,1,0,0,0,25,140,
  	1,0,0,0,27,142,1,0,0,0,29,145,1,0,0,0,31,147,1,0,0,0,33,149,1,0,0,0,35,
  	151,1,0,0,0,37,153,1,0,0,0,39,155,1,0,0,0,41,157,1,0,0,0,43,159,1,0,0,
  	0,45,185,1,0,0,0,47,201,1,0,0,0,49,203,1,0,0,0,51,205,1,0,0,0,53,213,
  	1,0,0,0,55,215,1,0,0,0,57,217,1,0,0,0,59,226,1,0,0,0,61,236,1,0,0,0,63,
  	249,1,0,0,0,65,259,1,0,0,0,67,261,1,0,0,0,69,265,1,0,0,0,71,277,1,0,0,
  	0,73,280,1,0,0,0,75,286,1,0,0,0,77,289,1,0,0,0,79,294,1,0,0,0,81,299,
  	1,0,0,0,83,305,1,0,0,0,85,309,1,0,0,0,87,312,1,0,0,0,89,317,1,0,0,0,91,
  	324,1,0,0,0,93,328,1,0,0,0,95,339,1,0,0,0,97,349,1,0,0,0,99,103,5,35,
  	0,0,100,102,8,0,0,0,101,100,1,0,0,0,102,105,1,0,0,0,103,101,1,0,0,0,103,
  	104,1,0,0,0,104,106,1,0,0,0,105,103,1,0,0,0,106,107,6,0,0,0,107,2,1,0,
  	0,0,108,109,5,123,0,0,109,4,1,0,0,0,110,111,5,125,0,0,111,6,1,0,0,0,112,
  	113,5,58,0,0,113,114,5,61,0,0,114,8,1,0,0,0,115,116,5,44,0,0,116,10,1,
  	0,0,0,117,118,5,46,0,0,118,119,5,46,0,0,119,120,5,46,0,0,120,12,1,0,0,
  	0,121,122,5,46,0,0,122,14,1,0,0,0,123,124,5,101,0,0,124,125,5,108,0,0,
  	125,126,5,101,0,0,126,127,5,109,0,0,127,128,5,101,0,0,128,16,1,0,0,0,
  	129,130,5,61,0,0,130,131,5,61,0,0,131,18,1,0,0,0,132,133,5,33,0,0,133,
  	134,5,61,0,0,134,20,1,0,0,0,135,136,5,60,0,0,136,22,1,0,0,0,137,138,5,
  	60,0,0,138,139,5,61,0,0,139,24,1,0,0,0,140,141,5,62,0,0,141,26,1,0,0,
  	0,142,143,5,62,0,0,143,144,5,61,0,0,144,28,1,0,0,0,145,146,5,94,0,0,146,
  	30,1,0,0,0,147,148,5,42,0,0,148,32,1,0,0,0,149,150,5,47,0,0,150,34,1,
  	0,0,0,151,152,5,43,0,0,152,36,1,0,0,0,153,154,5,45,0,0,154,38,1,0,0,0,
  	155,156,5,37,0,0,156,40,1,0,0,0,157,158,5,59,0,0,158,42,1,0,0,0,159,160,
  	5,97,0,0,160,161,5,108,0,0,161,162,5,97,0,0,162,163,5,112,0,0,163,164,
  	5,46,0,0,164,44,1,0,0,0,165,166,5,101,0,0,166,167,5,103,0,0,167,168,5,
  	233,0,0,168,169,5,115,0,0,169,186,5,122,0,0,170,171,5,115,0,0,171,172,
  	5,122,0,0,172,173,5,246,0,0,173,174,5,118,0,0,174,175,5,101,0,0,175,186,
  	5,103,0,0,176,177,5,105,0,0,177,178,5,103,0,0,178,179,5,97,0,0,179,180,
  	5,122,0,0,180,181,5,104,0,0,181,182,5,97,0,0,182,183,5,109,0,0,183,184,
  	5,105,0,0,184,186,5,115,0,0,185,165,1,0,0,0,185,170,1,0,0,0,185,176,1,
  	0,0,0,186,46,1,0,0,0,187,188,5,118,0,0,188,189,5,225,0,0,189,190,5,108,
  	0,0,190,191,5,116,0,0,191,192,5,111,0,0,192,193,5,122,0,0,193,202,5,243,
  	0,0,194,195,5,225,0,0,195,196,5,108,0,0,196,197,5,108,0,0,197,198,5,97,
  	0,0,198,199,5,110,0,0,199,200,5,100,0,0,200,202,5,243,0,0,201,187,1,0,
  	0,0,201,194,1,0,0,0,202,48,1,0,0,0,203,204,5,33,0,0,204,50,1,0,0,0,205,
  	206,5,107,0,0,206,207,5,101,0,0,207,208,5,122,0,0,208,209,5,100,0,0,209,
  	210,5,106,0,0,210,211,5,252,0,0,211,212,5,107,0,0,212,52,1,0,0,0,213,
  	214,5,40,0,0,214,54,1,0,0,0,215,216,5,41,0,0,216,56,1,0,0,0,217,218,5,
  	101,0,0,218,219,5,114,0,0,219,220,5,101,0,0,220,221,5,100,0,0,221,222,
  	5,109,0,0,222,223,5,233,0,0,223,224,5,110,0,0,224,225,5,121,0,0,225,58,
  	1,0,0,0,226,227,5,116,0,0,227,228,5,101,0,0,228,229,5,114,0,0,229,230,
  	5,118,0,0,230,231,5,114,0,0,231,232,5,97,0,0,232,233,5,106,0,0,233,234,
  	5,122,0,0,234,235,5,97,0,0,235,60,1,0,0,0,236,237,5,109,0,0,237,238,5,
  	105,0,0,238,239,5,110,0,0,239,240,5,100,0,0,240,241,5,101,0,0,241,242,
  	5,110,0,0,242,62,1,0,0,0,243,244,5,45,0,0,244,245,5,114,0,0,245,250,5,
  	97,0,0,246,247,5,45,0,0,247,248,5,114,0,0,248,250,5,101,0,0,249,243,1,
  	0,0,0,249,246,1,0,0,0,250,64,1,0,0,0,251,252,5,45,0,0,252,253,5,116,0,
  	0,253,254,5,243,0,0,254,260,5,108,0,0,255,256,5,45,0,0,256,257,5,116,
  	0,0,257,258,5,337,0,0,258,260,5,108,0,0,259,251,1,0,0,0,259,255,1,0,0,
  	0,260,66,1,0,0,0,261,262,5,45,0,0,262,263,5,105,0,0,263,264,5,103,0,0,
  	264,68,1,0,0,0,265,266,5,45,0,0,266,267,3,97,48,0,267,268,5,115,0,0,268,
  	275,3,97,48,0,269,270,5,118,0,0,270,271,5,97,0,0,271,276,5,108,0,0,272,
  	273,5,118,0,0,273,274,5,101,0,0,274,276,5,108,0,0,275,269,1,0,0,0,275,
  	272,1,0,0,0,276,70,1,0,0,0,277,278,5,104,0,0,278,279,5,97,0,0,279,72,
  	1,0,0,0,280,281,5,97,0,0,281,282,5,109,0,0,282,283,5,250,0,0,283,284,
  	5,103,0,0,284,285,5,121,0,0,285,74,1,0,0,0,286,287,5,233,0,0,287,288,
  	5,115,0,0,288,76,1,0,0,0,289,290,5,118,0,0,290,291,5,97,0,0,291,292,5,
  	103,0,0,292,293,5,121,0,0,293,78,1,0,0,0,294,295,5,110,0,0,295,296,5,
  	101,0,0,296,297,5,109,0,0,297,80,1,0,0,0,298,300,7,1,0,0,299,298,1,0,
  	0,0,300,301,1,0,0,0,301,299,1,0,0,0,301,302,1,0,0,0,302,303,1,0,0,0,303,
  	304,6,40,1,0,304,82,1,0,0,0,305,306,5,32,0,0,306,307,1,0,0,0,307,308,
  	6,41,1,0,308,84,1,0,0,0,309,310,5,92,0,0,310,311,5,34,0,0,311,86,1,0,
  	0,0,312,313,5,105,0,0,313,314,5,103,0,0,314,315,5,97,0,0,315,316,5,122,
  	0,0,316,88,1,0,0,0,317,318,5,104,0,0,318,319,5,97,0,0,319,320,5,109,0,
  	0,320,321,5,105,0,0,321,322,5,115,0,0,322,90,1,0,0,0,323,325,7,2,0,0,
  	324,323,1,0,0,0,325,326,1,0,0,0,326,324,1,0,0,0,326,327,1,0,0,0,327,92,
  	1,0,0,0,328,333,5,34,0,0,329,332,3,85,42,0,330,332,9,0,0,0,331,329,1,
  	0,0,0,331,330,1,0,0,0,332,335,1,0,0,0,333,334,1,0,0,0,333,331,1,0,0,0,
  	334,336,1,0,0,0,335,333,1,0,0,0,336,337,5,34,0,0,337,94,1,0,0,0,338,340,
  	7,3,0,0,339,338,1,0,0,0,340,341,1,0,0,0,341,339,1,0,0,0,341,342,1,0,0,
  	0,342,346,1,0,0,0,343,345,7,4,0,0,344,343,1,0,0,0,345,348,1,0,0,0,346,
  	344,1,0,0,0,346,347,1,0,0,0,347,96,1,0,0,0,348,346,1,0,0,0,349,350,7,
  	5,0,0,350,98,1,0,0,0,13,0,103,185,201,249,259,275,301,326,331,333,341,
  	346,2,6,0,0,0,1,0
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  agglunyelvlexerLexerStaticData = staticData.release();
}

}

AggluNyelvLexer::AggluNyelvLexer(CharStream *input) : Lexer(input) {
  AggluNyelvLexer::initialize();
  _interpreter = new atn::LexerATNSimulator(this, *agglunyelvlexerLexerStaticData->atn, agglunyelvlexerLexerStaticData->decisionToDFA, agglunyelvlexerLexerStaticData->sharedContextCache);
}

AggluNyelvLexer::~AggluNyelvLexer() {
  delete _interpreter;
}

std::string AggluNyelvLexer::getGrammarFileName() const {
  return "AggluNyelv.g4";
}

const std::vector<std::string>& AggluNyelvLexer::getRuleNames() const {
  return agglunyelvlexerLexerStaticData->ruleNames;
}

const std::vector<std::string>& AggluNyelvLexer::getChannelNames() const {
  return agglunyelvlexerLexerStaticData->channelNames;
}

const std::vector<std::string>& AggluNyelvLexer::getModeNames() const {
  return agglunyelvlexerLexerStaticData->modeNames;
}

const dfa::Vocabulary& AggluNyelvLexer::getVocabulary() const {
  return agglunyelvlexerLexerStaticData->vocabulary;
}

antlr4::atn::SerializedATNView AggluNyelvLexer::getSerializedATN() const {
  return agglunyelvlexerLexerStaticData->serializedATN;
}

const atn::ATN& AggluNyelvLexer::getATN() const {
  return *agglunyelvlexerLexerStaticData->atn;
}




void AggluNyelvLexer::initialize() {
  std::call_once(agglunyelvlexerLexerOnceFlag, agglunyelvlexerLexerInitialize);
}
