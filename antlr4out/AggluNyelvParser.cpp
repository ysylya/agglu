
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1


#include "AggluNyelvListener.h"
#include "AggluNyelvVisitor.h"

#include "AggluNyelvParser.h"


using namespace antlrcpp;

using namespace antlr4;

namespace {

struct AggluNyelvParserStaticData final {
  AggluNyelvParserStaticData(std::vector<std::string> ruleNames,
                        std::vector<std::string> literalNames,
                        std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  AggluNyelvParserStaticData(const AggluNyelvParserStaticData&) = delete;
  AggluNyelvParserStaticData(AggluNyelvParserStaticData&&) = delete;
  AggluNyelvParserStaticData& operator=(const AggluNyelvParserStaticData&) = delete;
  AggluNyelvParserStaticData& operator=(AggluNyelvParserStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

std::once_flag agglunyelvParserOnceFlag;
AggluNyelvParserStaticData *agglunyelvParserStaticData = nullptr;

void agglunyelvParserInitialize() {
  assert(agglunyelvParserStaticData == nullptr);
  auto staticData = std::make_unique<AggluNyelvParserStaticData>(
    std::vector<std::string>{
      "prog", "structDefinition", "functionArgument", "functionCall", "functionDefinitionMain", 
      "functionDefinition", "functionParameters", "functionReturnProperties", 
      "functionReturn", "functionBlock", "block", "blockPart", "conditional", 
      "conditionalIf", "conditionalElif", "conditionalElse", "loop", "loopvarDeclaration", 
      "loopvarValueBeg", "loopvarValueEnd", "loopvarValueStep", "statementBody", 
      "statement", "logicalExpression", "opLogicalComparison", "opComparison", 
      "expression", "arrayElementAccess", "arrayElementAssignment", "variableDefinition", 
      "variableDeclaration", "variableDeclarationProperties", "variableAssignment", 
      "variableValue", "boolean", "text"
    },
    std::vector<std::string>{
      "", "", "'{'", "'}'", "':='", "','", "'...'", "'.'", "'eleme'", "'=='", 
      "'!='", "'<'", "'<='", "'>'", "'>='", "'^'", "'*'", "'/'", "'+'", 
      "'-'", "'%'", "';'", "'alap.'", "", "", "'!'", "'kezdj\\u00FCk'", 
      "'('", "')'", "'eredm\\u00E9ny'", "'tervrajza'", "'minden'", "", "", 
      "'-ig'", "", "'ha'", "'am\\u00FAgy'", "'\\u00E9s'", "'vagy'", "'nem'", 
      "", "' '", "'\\\"'", "'igaz'", "'hamis'"
    },
    std::vector<std::string>{
      "", "COMMENT", "BLOCK_OPEN", "BLOCK_CLOSE", "OP_ASSIGN", "OP_LISTING", 
      "OP_DEFAULT_VALUE", "OP_DOT", "OP_ARR_ELEM_SELECTOR_DECOR", "OP_COMP_EQUALS", 
      "OP_COMP_NOTEQUALS", "OP_COMP_LESS_THAN", "OP_COMP_LESS_OR_EQUAL_THAN", 
      "OP_COMP_GREATER_THAN", "OP_COMP_GREATER_OR_EQUAL_THAN", "OP_EXPONENTATION", 
      "OP_MULTIPLICATION", "OP_DIVISION", "OP_ADDITION", "OP_SUBTRACTION", 
      "OP_MODULO", "TERMINATOR", "PREFIX_STD", "TYPE", "CONSTNESS", "FUN_DECOR", 
      "FUN_MAIN_NAME", "PARENTHESIS_OPEN", "PARENTHESIS_CLOSE", "FUN_RETURN", 
      "STRUCT_DECLR", "LOOPVAR_VALUE_DEF_BEG", "LOOPVAR_VALUE_DEF_END", 
      "LOOPVAR_VALUE_BEG", "LOOPVAR_VALUE_END", "LOOPVAR_VALUE_STEP", "CONDITIONAL_IF", 
      "CONDITIONAL_ELSE", "LOGICAL_AND", "LOGICAL_OR", "LOGICAL_NOT", "NEWLINE", 
      "WHITESPACE", "ESC_QUOTATION_MARK", "BOOLEAN_TRUE", "BOOLEAN_FALSE", 
      "INTEGER", "TEXT", "SYMBOL_NAME"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,1,48,370,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,6,2,
  	7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,2,14,7,
  	14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,7,20,2,21,7,
  	21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,2,27,7,27,2,28,7,
  	28,2,29,7,29,2,30,7,30,2,31,7,31,2,32,7,32,2,33,7,33,2,34,7,34,2,35,7,
  	35,1,0,1,0,1,0,5,0,76,8,0,10,0,12,0,79,9,0,1,1,1,1,1,1,1,1,1,1,1,1,5,
  	1,87,8,1,10,1,12,1,90,9,1,1,1,1,1,1,2,1,2,3,2,96,8,2,1,3,3,3,99,8,3,1,
  	3,1,3,1,3,1,3,1,3,1,3,5,3,107,8,3,10,3,12,3,110,9,3,1,3,3,3,113,8,3,1,
  	3,1,3,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,1,5,
  	3,5,133,8,5,1,5,1,5,1,5,1,6,1,6,1,6,5,6,141,8,6,10,6,12,6,144,9,6,1,6,
  	3,6,147,8,6,1,7,3,7,150,8,7,1,8,1,8,1,8,1,8,1,8,5,8,157,8,8,10,8,12,8,
  	160,9,8,1,8,4,8,163,8,8,11,8,12,8,164,1,8,1,8,1,9,1,9,5,9,171,8,9,10,
  	9,12,9,174,9,9,1,9,3,9,177,8,9,1,9,1,9,1,10,1,10,5,10,183,8,10,10,10,
  	12,10,186,9,10,1,10,1,10,1,11,1,11,1,11,1,11,1,11,3,11,195,8,11,1,12,
  	1,12,5,12,199,8,12,10,12,12,12,202,9,12,1,12,3,12,205,8,12,1,13,1,13,
  	1,13,1,13,1,14,1,14,1,14,1,14,1,15,1,15,1,15,1,16,1,16,1,16,1,16,1,16,
  	1,16,1,17,1,17,1,17,1,17,1,17,1,17,1,17,1,18,1,18,1,18,1,19,1,19,1,19,
  	1,20,1,20,1,20,1,21,1,21,1,21,1,21,1,21,3,21,245,8,21,1,22,1,22,1,22,
  	1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,3,23,
  	263,8,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,1,23,5,23,275,8,
  	23,10,23,12,23,278,9,23,1,24,1,24,1,25,1,25,1,26,1,26,1,26,1,26,1,26,
  	1,26,1,26,1,26,1,26,1,26,1,26,3,26,295,8,26,1,26,1,26,1,26,1,26,1,26,
  	1,26,5,26,303,8,26,10,26,12,26,306,9,26,1,27,1,27,1,27,1,27,1,27,1,28,
  	1,28,1,28,1,28,1,29,1,29,1,29,1,29,1,29,5,29,322,8,29,10,29,12,29,325,
  	9,29,1,29,4,29,328,8,29,11,29,12,29,329,1,30,1,30,1,30,1,30,1,30,1,31,
  	3,31,338,8,31,1,31,1,31,1,31,1,31,1,31,1,32,1,32,1,32,1,32,1,32,5,32,
  	350,8,32,10,32,12,32,353,9,32,1,32,4,32,356,8,32,11,32,12,32,357,1,33,
  	1,33,1,33,1,33,3,33,364,8,33,1,34,1,34,1,35,1,35,1,35,0,2,46,52,36,0,
  	2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,
  	52,54,56,58,60,62,64,66,68,70,0,6,1,0,9,10,1,0,9,14,1,0,16,17,1,0,18,
  	19,2,0,23,23,48,48,1,0,44,45,382,0,77,1,0,0,0,2,80,1,0,0,0,4,95,1,0,0,
  	0,6,98,1,0,0,0,8,116,1,0,0,0,10,123,1,0,0,0,12,142,1,0,0,0,14,149,1,0,
  	0,0,16,151,1,0,0,0,18,168,1,0,0,0,20,180,1,0,0,0,22,194,1,0,0,0,24,196,
  	1,0,0,0,26,206,1,0,0,0,28,210,1,0,0,0,30,214,1,0,0,0,32,217,1,0,0,0,34,
  	223,1,0,0,0,36,230,1,0,0,0,38,233,1,0,0,0,40,236,1,0,0,0,42,244,1,0,0,
  	0,44,246,1,0,0,0,46,262,1,0,0,0,48,279,1,0,0,0,50,281,1,0,0,0,52,294,
  	1,0,0,0,54,307,1,0,0,0,56,312,1,0,0,0,58,316,1,0,0,0,60,331,1,0,0,0,62,
  	337,1,0,0,0,64,344,1,0,0,0,66,363,1,0,0,0,68,365,1,0,0,0,70,367,1,0,0,
  	0,72,76,3,10,5,0,73,76,3,8,4,0,74,76,3,2,1,0,75,72,1,0,0,0,75,73,1,0,
  	0,0,75,74,1,0,0,0,76,79,1,0,0,0,77,75,1,0,0,0,77,78,1,0,0,0,78,1,1,0,
  	0,0,79,77,1,0,0,0,80,81,5,48,0,0,81,82,5,30,0,0,82,88,5,2,0,0,83,84,3,
  	58,29,0,84,85,5,21,0,0,85,87,1,0,0,0,86,83,1,0,0,0,87,90,1,0,0,0,88,86,
  	1,0,0,0,88,89,1,0,0,0,89,91,1,0,0,0,90,88,1,0,0,0,91,92,5,3,0,0,92,3,
  	1,0,0,0,93,96,5,48,0,0,94,96,3,66,33,0,95,93,1,0,0,0,95,94,1,0,0,0,96,
  	5,1,0,0,0,97,99,5,22,0,0,98,97,1,0,0,0,98,99,1,0,0,0,99,100,1,0,0,0,100,
  	101,5,48,0,0,101,102,5,25,0,0,102,108,5,27,0,0,103,104,3,4,2,0,104,105,
  	5,5,0,0,105,107,1,0,0,0,106,103,1,0,0,0,107,110,1,0,0,0,108,106,1,0,0,
  	0,108,109,1,0,0,0,109,112,1,0,0,0,110,108,1,0,0,0,111,113,3,4,2,0,112,
  	111,1,0,0,0,112,113,1,0,0,0,113,114,1,0,0,0,114,115,5,28,0,0,115,7,1,
  	0,0,0,116,117,5,26,0,0,117,118,5,25,0,0,118,119,5,27,0,0,119,120,5,28,
  	0,0,120,121,5,4,0,0,121,122,3,18,9,0,122,9,1,0,0,0,123,124,5,48,0,0,124,
  	125,5,25,0,0,125,126,5,27,0,0,126,127,3,12,6,0,127,132,5,28,0,0,128,129,
  	5,2,0,0,129,130,3,14,7,0,130,131,5,3,0,0,131,133,1,0,0,0,132,128,1,0,
  	0,0,132,133,1,0,0,0,133,134,1,0,0,0,134,135,5,4,0,0,135,136,3,18,9,0,
  	136,11,1,0,0,0,137,138,3,60,30,0,138,139,5,5,0,0,139,141,1,0,0,0,140,
  	137,1,0,0,0,141,144,1,0,0,0,142,140,1,0,0,0,142,143,1,0,0,0,143,146,1,
  	0,0,0,144,142,1,0,0,0,145,147,3,60,30,0,146,145,1,0,0,0,146,147,1,0,0,
  	0,147,13,1,0,0,0,148,150,3,62,31,0,149,148,1,0,0,0,149,150,1,0,0,0,150,
  	15,1,0,0,0,151,152,5,29,0,0,152,158,5,4,0,0,153,154,3,66,33,0,154,155,
  	5,5,0,0,155,157,1,0,0,0,156,153,1,0,0,0,157,160,1,0,0,0,158,156,1,0,0,
  	0,158,159,1,0,0,0,159,162,1,0,0,0,160,158,1,0,0,0,161,163,3,66,33,0,162,
  	161,1,0,0,0,163,164,1,0,0,0,164,162,1,0,0,0,164,165,1,0,0,0,165,166,1,
  	0,0,0,166,167,5,21,0,0,167,17,1,0,0,0,168,172,5,2,0,0,169,171,3,22,11,
  	0,170,169,1,0,0,0,171,174,1,0,0,0,172,170,1,0,0,0,172,173,1,0,0,0,173,
  	176,1,0,0,0,174,172,1,0,0,0,175,177,3,16,8,0,176,175,1,0,0,0,176,177,
  	1,0,0,0,177,178,1,0,0,0,178,179,5,3,0,0,179,19,1,0,0,0,180,184,5,2,0,
  	0,181,183,3,22,11,0,182,181,1,0,0,0,183,186,1,0,0,0,184,182,1,0,0,0,184,
  	185,1,0,0,0,185,187,1,0,0,0,186,184,1,0,0,0,187,188,5,3,0,0,188,21,1,
  	0,0,0,189,195,3,44,22,0,190,195,3,20,10,0,191,195,3,24,12,0,192,195,3,
  	32,16,0,193,195,3,6,3,0,194,189,1,0,0,0,194,190,1,0,0,0,194,191,1,0,0,
  	0,194,192,1,0,0,0,194,193,1,0,0,0,195,23,1,0,0,0,196,200,3,26,13,0,197,
  	199,3,28,14,0,198,197,1,0,0,0,199,202,1,0,0,0,200,198,1,0,0,0,200,201,
  	1,0,0,0,201,204,1,0,0,0,202,200,1,0,0,0,203,205,3,30,15,0,204,203,1,0,
  	0,0,204,205,1,0,0,0,205,25,1,0,0,0,206,207,5,36,0,0,207,208,3,46,23,0,
  	208,209,3,20,10,0,209,27,1,0,0,0,210,211,5,37,0,0,211,212,5,5,0,0,212,
  	213,3,26,13,0,213,29,1,0,0,0,214,215,5,37,0,0,215,216,3,20,10,0,216,31,
  	1,0,0,0,217,218,3,34,17,0,218,219,3,36,18,0,219,220,3,38,19,0,220,221,
  	3,40,20,0,221,222,3,20,10,0,222,33,1,0,0,0,223,224,5,31,0,0,224,225,5,
  	48,0,0,225,226,5,32,0,0,226,227,5,2,0,0,227,228,3,62,31,0,228,229,5,3,
  	0,0,229,35,1,0,0,0,230,231,3,52,26,0,231,232,5,33,0,0,232,37,1,0,0,0,
  	233,234,3,52,26,0,234,235,5,34,0,0,235,39,1,0,0,0,236,237,3,52,26,0,237,
  	238,5,35,0,0,238,41,1,0,0,0,239,245,3,52,26,0,240,245,3,46,23,0,241,245,
  	3,64,32,0,242,245,3,58,29,0,243,245,3,56,28,0,244,239,1,0,0,0,244,240,
  	1,0,0,0,244,241,1,0,0,0,244,242,1,0,0,0,244,243,1,0,0,0,245,43,1,0,0,
  	0,246,247,3,42,21,0,247,248,5,21,0,0,248,45,1,0,0,0,249,250,6,23,-1,0,
  	250,251,5,40,0,0,251,263,3,46,23,8,252,263,3,68,34,0,253,263,3,6,3,0,
  	254,255,3,52,26,0,255,256,3,50,25,0,256,257,3,52,26,0,257,263,1,0,0,0,
  	258,259,5,27,0,0,259,260,3,46,23,0,260,261,5,28,0,0,261,263,1,0,0,0,262,
  	249,1,0,0,0,262,252,1,0,0,0,262,253,1,0,0,0,262,254,1,0,0,0,262,258,1,
  	0,0,0,263,276,1,0,0,0,264,265,10,7,0,0,265,266,5,39,0,0,266,275,3,46,
  	23,8,267,268,10,6,0,0,268,269,5,38,0,0,269,275,3,46,23,7,270,271,10,2,
  	0,0,271,272,3,48,24,0,272,273,3,46,23,3,273,275,1,0,0,0,274,264,1,0,0,
  	0,274,267,1,0,0,0,274,270,1,0,0,0,275,278,1,0,0,0,276,274,1,0,0,0,276,
  	277,1,0,0,0,277,47,1,0,0,0,278,276,1,0,0,0,279,280,7,0,0,0,280,49,1,0,
  	0,0,281,282,7,1,0,0,282,51,1,0,0,0,283,284,6,26,-1,0,284,285,5,19,0,0,
  	285,295,3,52,26,8,286,295,5,46,0,0,287,295,5,48,0,0,288,295,3,54,27,0,
  	289,295,3,6,3,0,290,291,5,27,0,0,291,292,3,52,26,0,292,293,5,28,0,0,293,
  	295,1,0,0,0,294,283,1,0,0,0,294,286,1,0,0,0,294,287,1,0,0,0,294,288,1,
  	0,0,0,294,289,1,0,0,0,294,290,1,0,0,0,295,304,1,0,0,0,296,297,10,7,0,
  	0,297,298,7,2,0,0,298,303,3,52,26,8,299,300,10,6,0,0,300,301,7,3,0,0,
  	301,303,3,52,26,7,302,296,1,0,0,0,302,299,1,0,0,0,303,306,1,0,0,0,304,
  	302,1,0,0,0,304,305,1,0,0,0,305,53,1,0,0,0,306,304,1,0,0,0,307,308,5,
  	48,0,0,308,309,5,46,0,0,309,310,5,7,0,0,310,311,5,8,0,0,311,55,1,0,0,
  	0,312,313,3,54,27,0,313,314,5,4,0,0,314,315,3,66,33,0,315,57,1,0,0,0,
  	316,317,3,60,30,0,317,323,5,4,0,0,318,319,3,66,33,0,319,320,5,5,0,0,320,
  	322,1,0,0,0,321,318,1,0,0,0,322,325,1,0,0,0,323,321,1,0,0,0,323,324,1,
  	0,0,0,324,327,1,0,0,0,325,323,1,0,0,0,326,328,3,66,33,0,327,326,1,0,0,
  	0,328,329,1,0,0,0,329,327,1,0,0,0,329,330,1,0,0,0,330,59,1,0,0,0,331,
  	332,5,48,0,0,332,333,5,2,0,0,333,334,3,62,31,0,334,335,5,3,0,0,335,61,
  	1,0,0,0,336,338,5,46,0,0,337,336,1,0,0,0,337,338,1,0,0,0,338,339,1,0,
  	0,0,339,340,7,4,0,0,340,341,1,0,0,0,341,342,5,5,0,0,342,343,5,24,0,0,
  	343,63,1,0,0,0,344,345,5,48,0,0,345,351,5,4,0,0,346,347,3,66,33,0,347,
  	348,5,5,0,0,348,350,1,0,0,0,349,346,1,0,0,0,350,353,1,0,0,0,351,349,1,
  	0,0,0,351,352,1,0,0,0,352,355,1,0,0,0,353,351,1,0,0,0,354,356,3,66,33,
  	0,355,354,1,0,0,0,356,357,1,0,0,0,357,355,1,0,0,0,357,358,1,0,0,0,358,
  	65,1,0,0,0,359,364,3,52,26,0,360,364,3,70,35,0,361,364,3,68,34,0,362,
  	364,5,6,0,0,363,359,1,0,0,0,363,360,1,0,0,0,363,361,1,0,0,0,363,362,1,
  	0,0,0,364,67,1,0,0,0,365,366,7,5,0,0,366,69,1,0,0,0,367,368,5,47,0,0,
  	368,71,1,0,0,0,32,75,77,88,95,98,108,112,132,142,146,149,158,164,172,
  	176,184,194,200,204,244,262,274,276,294,302,304,323,329,337,351,357,363
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  agglunyelvParserStaticData = staticData.release();
}

}

AggluNyelvParser::AggluNyelvParser(TokenStream *input) : AggluNyelvParser(input, antlr4::atn::ParserATNSimulatorOptions()) {}

AggluNyelvParser::AggluNyelvParser(TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options) : Parser(input) {
  AggluNyelvParser::initialize();
  _interpreter = new atn::ParserATNSimulator(this, *agglunyelvParserStaticData->atn, agglunyelvParserStaticData->decisionToDFA, agglunyelvParserStaticData->sharedContextCache, options);
}

AggluNyelvParser::~AggluNyelvParser() {
  delete _interpreter;
}

const atn::ATN& AggluNyelvParser::getATN() const {
  return *agglunyelvParserStaticData->atn;
}

std::string AggluNyelvParser::getGrammarFileName() const {
  return "AggluNyelv.g4";
}

const std::vector<std::string>& AggluNyelvParser::getRuleNames() const {
  return agglunyelvParserStaticData->ruleNames;
}

const dfa::Vocabulary& AggluNyelvParser::getVocabulary() const {
  return agglunyelvParserStaticData->vocabulary;
}

antlr4::atn::SerializedATNView AggluNyelvParser::getSerializedATN() const {
  return agglunyelvParserStaticData->serializedATN;
}


//----------------- ProgContext ------------------------------------------------------------------

AggluNyelvParser::ProgContext::ProgContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<AggluNyelvParser::FunctionDefinitionContext *> AggluNyelvParser::ProgContext::functionDefinition() {
  return getRuleContexts<AggluNyelvParser::FunctionDefinitionContext>();
}

AggluNyelvParser::FunctionDefinitionContext* AggluNyelvParser::ProgContext::functionDefinition(size_t i) {
  return getRuleContext<AggluNyelvParser::FunctionDefinitionContext>(i);
}

std::vector<AggluNyelvParser::FunctionDefinitionMainContext *> AggluNyelvParser::ProgContext::functionDefinitionMain() {
  return getRuleContexts<AggluNyelvParser::FunctionDefinitionMainContext>();
}

AggluNyelvParser::FunctionDefinitionMainContext* AggluNyelvParser::ProgContext::functionDefinitionMain(size_t i) {
  return getRuleContext<AggluNyelvParser::FunctionDefinitionMainContext>(i);
}

std::vector<AggluNyelvParser::StructDefinitionContext *> AggluNyelvParser::ProgContext::structDefinition() {
  return getRuleContexts<AggluNyelvParser::StructDefinitionContext>();
}

AggluNyelvParser::StructDefinitionContext* AggluNyelvParser::ProgContext::structDefinition(size_t i) {
  return getRuleContext<AggluNyelvParser::StructDefinitionContext>(i);
}


size_t AggluNyelvParser::ProgContext::getRuleIndex() const {
  return AggluNyelvParser::RuleProg;
}

void AggluNyelvParser::ProgContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProg(this);
}

void AggluNyelvParser::ProgContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProg(this);
}


std::any AggluNyelvParser::ProgContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitProg(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ProgContext* AggluNyelvParser::prog() {
  ProgContext *_localctx = _tracker.createInstance<ProgContext>(_ctx, getState());
  enterRule(_localctx, 0, AggluNyelvParser::RuleProg);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(77);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == AggluNyelvParser::FUN_MAIN_NAME

    || _la == AggluNyelvParser::SYMBOL_NAME) {
      setState(75);
      _errHandler->sync(this);
      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx)) {
      case 1: {
        setState(72);
        functionDefinition();
        break;
      }

      case 2: {
        setState(73);
        functionDefinitionMain();
        break;
      }

      case 3: {
        setState(74);
        structDefinition();
        break;
      }

      default:
        break;
      }
      setState(79);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StructDefinitionContext ------------------------------------------------------------------

AggluNyelvParser::StructDefinitionContext::StructDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::StructDefinitionContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::StructDefinitionContext::STRUCT_DECLR() {
  return getToken(AggluNyelvParser::STRUCT_DECLR, 0);
}

tree::TerminalNode* AggluNyelvParser::StructDefinitionContext::BLOCK_OPEN() {
  return getToken(AggluNyelvParser::BLOCK_OPEN, 0);
}

tree::TerminalNode* AggluNyelvParser::StructDefinitionContext::BLOCK_CLOSE() {
  return getToken(AggluNyelvParser::BLOCK_CLOSE, 0);
}

std::vector<AggluNyelvParser::VariableDefinitionContext *> AggluNyelvParser::StructDefinitionContext::variableDefinition() {
  return getRuleContexts<AggluNyelvParser::VariableDefinitionContext>();
}

AggluNyelvParser::VariableDefinitionContext* AggluNyelvParser::StructDefinitionContext::variableDefinition(size_t i) {
  return getRuleContext<AggluNyelvParser::VariableDefinitionContext>(i);
}

std::vector<tree::TerminalNode *> AggluNyelvParser::StructDefinitionContext::TERMINATOR() {
  return getTokens(AggluNyelvParser::TERMINATOR);
}

tree::TerminalNode* AggluNyelvParser::StructDefinitionContext::TERMINATOR(size_t i) {
  return getToken(AggluNyelvParser::TERMINATOR, i);
}


size_t AggluNyelvParser::StructDefinitionContext::getRuleIndex() const {
  return AggluNyelvParser::RuleStructDefinition;
}

void AggluNyelvParser::StructDefinitionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStructDefinition(this);
}

void AggluNyelvParser::StructDefinitionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStructDefinition(this);
}


std::any AggluNyelvParser::StructDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitStructDefinition(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::StructDefinitionContext* AggluNyelvParser::structDefinition() {
  StructDefinitionContext *_localctx = _tracker.createInstance<StructDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 2, AggluNyelvParser::RuleStructDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(80);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(81);
    match(AggluNyelvParser::STRUCT_DECLR);
    setState(82);
    match(AggluNyelvParser::BLOCK_OPEN);
    setState(88);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == AggluNyelvParser::SYMBOL_NAME) {
      setState(83);
      variableDefinition();
      setState(84);
      match(AggluNyelvParser::TERMINATOR);
      setState(90);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(91);
    match(AggluNyelvParser::BLOCK_CLOSE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionArgumentContext ------------------------------------------------------------------

AggluNyelvParser::FunctionArgumentContext::FunctionArgumentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::FunctionArgumentContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

AggluNyelvParser::VariableValueContext* AggluNyelvParser::FunctionArgumentContext::variableValue() {
  return getRuleContext<AggluNyelvParser::VariableValueContext>(0);
}


size_t AggluNyelvParser::FunctionArgumentContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionArgument;
}

void AggluNyelvParser::FunctionArgumentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionArgument(this);
}

void AggluNyelvParser::FunctionArgumentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionArgument(this);
}


std::any AggluNyelvParser::FunctionArgumentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionArgument(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionArgumentContext* AggluNyelvParser::functionArgument() {
  FunctionArgumentContext *_localctx = _tracker.createInstance<FunctionArgumentContext>(_ctx, getState());
  enterRule(_localctx, 4, AggluNyelvParser::RuleFunctionArgument);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(95);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(93);
      match(AggluNyelvParser::SYMBOL_NAME);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(94);
      variableValue();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionCallContext ------------------------------------------------------------------

AggluNyelvParser::FunctionCallContext::FunctionCallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::FunctionCallContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionCallContext::FUN_DECOR() {
  return getToken(AggluNyelvParser::FUN_DECOR, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionCallContext::PARENTHESIS_OPEN() {
  return getToken(AggluNyelvParser::PARENTHESIS_OPEN, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionCallContext::PARENTHESIS_CLOSE() {
  return getToken(AggluNyelvParser::PARENTHESIS_CLOSE, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionCallContext::PREFIX_STD() {
  return getToken(AggluNyelvParser::PREFIX_STD, 0);
}

std::vector<AggluNyelvParser::FunctionArgumentContext *> AggluNyelvParser::FunctionCallContext::functionArgument() {
  return getRuleContexts<AggluNyelvParser::FunctionArgumentContext>();
}

AggluNyelvParser::FunctionArgumentContext* AggluNyelvParser::FunctionCallContext::functionArgument(size_t i) {
  return getRuleContext<AggluNyelvParser::FunctionArgumentContext>(i);
}

std::vector<tree::TerminalNode *> AggluNyelvParser::FunctionCallContext::OP_LISTING() {
  return getTokens(AggluNyelvParser::OP_LISTING);
}

tree::TerminalNode* AggluNyelvParser::FunctionCallContext::OP_LISTING(size_t i) {
  return getToken(AggluNyelvParser::OP_LISTING, i);
}


size_t AggluNyelvParser::FunctionCallContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionCall;
}

void AggluNyelvParser::FunctionCallContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionCall(this);
}

void AggluNyelvParser::FunctionCallContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionCall(this);
}


std::any AggluNyelvParser::FunctionCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionCall(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionCallContext* AggluNyelvParser::functionCall() {
  FunctionCallContext *_localctx = _tracker.createInstance<FunctionCallContext>(_ctx, getState());
  enterRule(_localctx, 6, AggluNyelvParser::RuleFunctionCall);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(98);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == AggluNyelvParser::PREFIX_STD) {
      setState(97);
      match(AggluNyelvParser::PREFIX_STD);
    }
    setState(100);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(101);
    match(AggluNyelvParser::FUN_DECOR);
    setState(102);
    match(AggluNyelvParser::PARENTHESIS_OPEN);
    setState(108);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(103);
        functionArgument();
        setState(104);
        match(AggluNyelvParser::OP_LISTING); 
      }
      setState(110);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    }
    setState(112);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::OP_DEFAULT_VALUE)
      | (1ULL << AggluNyelvParser::OP_SUBTRACTION)
      | (1ULL << AggluNyelvParser::PREFIX_STD)
      | (1ULL << AggluNyelvParser::PARENTHESIS_OPEN)
      | (1ULL << AggluNyelvParser::BOOLEAN_TRUE)
      | (1ULL << AggluNyelvParser::BOOLEAN_FALSE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::TEXT)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0)) {
      setState(111);
      functionArgument();
    }
    setState(114);
    match(AggluNyelvParser::PARENTHESIS_CLOSE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionDefinitionMainContext ------------------------------------------------------------------

AggluNyelvParser::FunctionDefinitionMainContext::FunctionDefinitionMainContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionMainContext::FUN_MAIN_NAME() {
  return getToken(AggluNyelvParser::FUN_MAIN_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionMainContext::FUN_DECOR() {
  return getToken(AggluNyelvParser::FUN_DECOR, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionMainContext::PARENTHESIS_OPEN() {
  return getToken(AggluNyelvParser::PARENTHESIS_OPEN, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionMainContext::PARENTHESIS_CLOSE() {
  return getToken(AggluNyelvParser::PARENTHESIS_CLOSE, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionMainContext::OP_ASSIGN() {
  return getToken(AggluNyelvParser::OP_ASSIGN, 0);
}

AggluNyelvParser::FunctionBlockContext* AggluNyelvParser::FunctionDefinitionMainContext::functionBlock() {
  return getRuleContext<AggluNyelvParser::FunctionBlockContext>(0);
}


size_t AggluNyelvParser::FunctionDefinitionMainContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionDefinitionMain;
}

void AggluNyelvParser::FunctionDefinitionMainContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionDefinitionMain(this);
}

void AggluNyelvParser::FunctionDefinitionMainContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionDefinitionMain(this);
}


std::any AggluNyelvParser::FunctionDefinitionMainContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionDefinitionMain(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionDefinitionMainContext* AggluNyelvParser::functionDefinitionMain() {
  FunctionDefinitionMainContext *_localctx = _tracker.createInstance<FunctionDefinitionMainContext>(_ctx, getState());
  enterRule(_localctx, 8, AggluNyelvParser::RuleFunctionDefinitionMain);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(116);
    match(AggluNyelvParser::FUN_MAIN_NAME);
    setState(117);
    match(AggluNyelvParser::FUN_DECOR);
    setState(118);
    match(AggluNyelvParser::PARENTHESIS_OPEN);
    setState(119);
    match(AggluNyelvParser::PARENTHESIS_CLOSE);
    setState(120);
    match(AggluNyelvParser::OP_ASSIGN);
    setState(121);
    functionBlock();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionDefinitionContext ------------------------------------------------------------------

AggluNyelvParser::FunctionDefinitionContext::FunctionDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::FUN_DECOR() {
  return getToken(AggluNyelvParser::FUN_DECOR, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::PARENTHESIS_OPEN() {
  return getToken(AggluNyelvParser::PARENTHESIS_OPEN, 0);
}

AggluNyelvParser::FunctionParametersContext* AggluNyelvParser::FunctionDefinitionContext::functionParameters() {
  return getRuleContext<AggluNyelvParser::FunctionParametersContext>(0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::PARENTHESIS_CLOSE() {
  return getToken(AggluNyelvParser::PARENTHESIS_CLOSE, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::OP_ASSIGN() {
  return getToken(AggluNyelvParser::OP_ASSIGN, 0);
}

AggluNyelvParser::FunctionBlockContext* AggluNyelvParser::FunctionDefinitionContext::functionBlock() {
  return getRuleContext<AggluNyelvParser::FunctionBlockContext>(0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::BLOCK_OPEN() {
  return getToken(AggluNyelvParser::BLOCK_OPEN, 0);
}

AggluNyelvParser::FunctionReturnPropertiesContext* AggluNyelvParser::FunctionDefinitionContext::functionReturnProperties() {
  return getRuleContext<AggluNyelvParser::FunctionReturnPropertiesContext>(0);
}

tree::TerminalNode* AggluNyelvParser::FunctionDefinitionContext::BLOCK_CLOSE() {
  return getToken(AggluNyelvParser::BLOCK_CLOSE, 0);
}


size_t AggluNyelvParser::FunctionDefinitionContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionDefinition;
}

void AggluNyelvParser::FunctionDefinitionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionDefinition(this);
}

void AggluNyelvParser::FunctionDefinitionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionDefinition(this);
}


std::any AggluNyelvParser::FunctionDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionDefinition(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionDefinitionContext* AggluNyelvParser::functionDefinition() {
  FunctionDefinitionContext *_localctx = _tracker.createInstance<FunctionDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 10, AggluNyelvParser::RuleFunctionDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(123);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(124);
    match(AggluNyelvParser::FUN_DECOR);
    setState(125);
    match(AggluNyelvParser::PARENTHESIS_OPEN);
    setState(126);
    functionParameters();
    setState(127);
    match(AggluNyelvParser::PARENTHESIS_CLOSE);
    setState(132);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == AggluNyelvParser::BLOCK_OPEN) {
      setState(128);
      match(AggluNyelvParser::BLOCK_OPEN);
      setState(129);
      functionReturnProperties();
      setState(130);
      match(AggluNyelvParser::BLOCK_CLOSE);
    }
    setState(134);
    match(AggluNyelvParser::OP_ASSIGN);
    setState(135);
    functionBlock();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionParametersContext ------------------------------------------------------------------

AggluNyelvParser::FunctionParametersContext::FunctionParametersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<AggluNyelvParser::VariableDeclarationContext *> AggluNyelvParser::FunctionParametersContext::variableDeclaration() {
  return getRuleContexts<AggluNyelvParser::VariableDeclarationContext>();
}

AggluNyelvParser::VariableDeclarationContext* AggluNyelvParser::FunctionParametersContext::variableDeclaration(size_t i) {
  return getRuleContext<AggluNyelvParser::VariableDeclarationContext>(i);
}

std::vector<tree::TerminalNode *> AggluNyelvParser::FunctionParametersContext::OP_LISTING() {
  return getTokens(AggluNyelvParser::OP_LISTING);
}

tree::TerminalNode* AggluNyelvParser::FunctionParametersContext::OP_LISTING(size_t i) {
  return getToken(AggluNyelvParser::OP_LISTING, i);
}


size_t AggluNyelvParser::FunctionParametersContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionParameters;
}

void AggluNyelvParser::FunctionParametersContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionParameters(this);
}

void AggluNyelvParser::FunctionParametersContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionParameters(this);
}


std::any AggluNyelvParser::FunctionParametersContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionParameters(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionParametersContext* AggluNyelvParser::functionParameters() {
  FunctionParametersContext *_localctx = _tracker.createInstance<FunctionParametersContext>(_ctx, getState());
  enterRule(_localctx, 12, AggluNyelvParser::RuleFunctionParameters);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(142);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(137);
        variableDeclaration();
        setState(138);
        match(AggluNyelvParser::OP_LISTING); 
      }
      setState(144);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx);
    }
    setState(146);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == AggluNyelvParser::SYMBOL_NAME) {
      setState(145);
      variableDeclaration();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionReturnPropertiesContext ------------------------------------------------------------------

AggluNyelvParser::FunctionReturnPropertiesContext::FunctionReturnPropertiesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::VariableDeclarationPropertiesContext* AggluNyelvParser::FunctionReturnPropertiesContext::variableDeclarationProperties() {
  return getRuleContext<AggluNyelvParser::VariableDeclarationPropertiesContext>(0);
}


size_t AggluNyelvParser::FunctionReturnPropertiesContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionReturnProperties;
}

void AggluNyelvParser::FunctionReturnPropertiesContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionReturnProperties(this);
}

void AggluNyelvParser::FunctionReturnPropertiesContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionReturnProperties(this);
}


std::any AggluNyelvParser::FunctionReturnPropertiesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionReturnProperties(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionReturnPropertiesContext* AggluNyelvParser::functionReturnProperties() {
  FunctionReturnPropertiesContext *_localctx = _tracker.createInstance<FunctionReturnPropertiesContext>(_ctx, getState());
  enterRule(_localctx, 14, AggluNyelvParser::RuleFunctionReturnProperties);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(149);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::TYPE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0)) {
      setState(148);
      variableDeclarationProperties();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionReturnContext ------------------------------------------------------------------

AggluNyelvParser::FunctionReturnContext::FunctionReturnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::FunctionReturnContext::FUN_RETURN() {
  return getToken(AggluNyelvParser::FUN_RETURN, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionReturnContext::OP_ASSIGN() {
  return getToken(AggluNyelvParser::OP_ASSIGN, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionReturnContext::TERMINATOR() {
  return getToken(AggluNyelvParser::TERMINATOR, 0);
}

std::vector<AggluNyelvParser::VariableValueContext *> AggluNyelvParser::FunctionReturnContext::variableValue() {
  return getRuleContexts<AggluNyelvParser::VariableValueContext>();
}

AggluNyelvParser::VariableValueContext* AggluNyelvParser::FunctionReturnContext::variableValue(size_t i) {
  return getRuleContext<AggluNyelvParser::VariableValueContext>(i);
}

std::vector<tree::TerminalNode *> AggluNyelvParser::FunctionReturnContext::OP_LISTING() {
  return getTokens(AggluNyelvParser::OP_LISTING);
}

tree::TerminalNode* AggluNyelvParser::FunctionReturnContext::OP_LISTING(size_t i) {
  return getToken(AggluNyelvParser::OP_LISTING, i);
}


size_t AggluNyelvParser::FunctionReturnContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionReturn;
}

void AggluNyelvParser::FunctionReturnContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionReturn(this);
}

void AggluNyelvParser::FunctionReturnContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionReturn(this);
}


std::any AggluNyelvParser::FunctionReturnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionReturn(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionReturnContext* AggluNyelvParser::functionReturn() {
  FunctionReturnContext *_localctx = _tracker.createInstance<FunctionReturnContext>(_ctx, getState());
  enterRule(_localctx, 16, AggluNyelvParser::RuleFunctionReturn);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(151);
    match(AggluNyelvParser::FUN_RETURN);
    setState(152);
    match(AggluNyelvParser::OP_ASSIGN);
    setState(158);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(153);
        variableValue();
        setState(154);
        match(AggluNyelvParser::OP_LISTING); 
      }
      setState(160);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx);
    }
    setState(162); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(161);
      variableValue();
      setState(164); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::OP_DEFAULT_VALUE)
      | (1ULL << AggluNyelvParser::OP_SUBTRACTION)
      | (1ULL << AggluNyelvParser::PREFIX_STD)
      | (1ULL << AggluNyelvParser::PARENTHESIS_OPEN)
      | (1ULL << AggluNyelvParser::BOOLEAN_TRUE)
      | (1ULL << AggluNyelvParser::BOOLEAN_FALSE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::TEXT)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0));
    setState(166);
    match(AggluNyelvParser::TERMINATOR);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionBlockContext ------------------------------------------------------------------

AggluNyelvParser::FunctionBlockContext::FunctionBlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::FunctionBlockContext::BLOCK_OPEN() {
  return getToken(AggluNyelvParser::BLOCK_OPEN, 0);
}

tree::TerminalNode* AggluNyelvParser::FunctionBlockContext::BLOCK_CLOSE() {
  return getToken(AggluNyelvParser::BLOCK_CLOSE, 0);
}

std::vector<AggluNyelvParser::BlockPartContext *> AggluNyelvParser::FunctionBlockContext::blockPart() {
  return getRuleContexts<AggluNyelvParser::BlockPartContext>();
}

AggluNyelvParser::BlockPartContext* AggluNyelvParser::FunctionBlockContext::blockPart(size_t i) {
  return getRuleContext<AggluNyelvParser::BlockPartContext>(i);
}

AggluNyelvParser::FunctionReturnContext* AggluNyelvParser::FunctionBlockContext::functionReturn() {
  return getRuleContext<AggluNyelvParser::FunctionReturnContext>(0);
}


size_t AggluNyelvParser::FunctionBlockContext::getRuleIndex() const {
  return AggluNyelvParser::RuleFunctionBlock;
}

void AggluNyelvParser::FunctionBlockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionBlock(this);
}

void AggluNyelvParser::FunctionBlockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionBlock(this);
}


std::any AggluNyelvParser::FunctionBlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitFunctionBlock(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::FunctionBlockContext* AggluNyelvParser::functionBlock() {
  FunctionBlockContext *_localctx = _tracker.createInstance<FunctionBlockContext>(_ctx, getState());
  enterRule(_localctx, 18, AggluNyelvParser::RuleFunctionBlock);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(168);
    match(AggluNyelvParser::BLOCK_OPEN);
    setState(172);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::BLOCK_OPEN)
      | (1ULL << AggluNyelvParser::OP_SUBTRACTION)
      | (1ULL << AggluNyelvParser::PREFIX_STD)
      | (1ULL << AggluNyelvParser::PARENTHESIS_OPEN)
      | (1ULL << AggluNyelvParser::LOOPVAR_VALUE_DEF_BEG)
      | (1ULL << AggluNyelvParser::CONDITIONAL_IF)
      | (1ULL << AggluNyelvParser::LOGICAL_NOT)
      | (1ULL << AggluNyelvParser::BOOLEAN_TRUE)
      | (1ULL << AggluNyelvParser::BOOLEAN_FALSE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0)) {
      setState(169);
      blockPart();
      setState(174);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(176);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == AggluNyelvParser::FUN_RETURN) {
      setState(175);
      functionReturn();
    }
    setState(178);
    match(AggluNyelvParser::BLOCK_CLOSE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

AggluNyelvParser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::BlockContext::BLOCK_OPEN() {
  return getToken(AggluNyelvParser::BLOCK_OPEN, 0);
}

tree::TerminalNode* AggluNyelvParser::BlockContext::BLOCK_CLOSE() {
  return getToken(AggluNyelvParser::BLOCK_CLOSE, 0);
}

std::vector<AggluNyelvParser::BlockPartContext *> AggluNyelvParser::BlockContext::blockPart() {
  return getRuleContexts<AggluNyelvParser::BlockPartContext>();
}

AggluNyelvParser::BlockPartContext* AggluNyelvParser::BlockContext::blockPart(size_t i) {
  return getRuleContext<AggluNyelvParser::BlockPartContext>(i);
}


size_t AggluNyelvParser::BlockContext::getRuleIndex() const {
  return AggluNyelvParser::RuleBlock;
}

void AggluNyelvParser::BlockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBlock(this);
}

void AggluNyelvParser::BlockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBlock(this);
}


std::any AggluNyelvParser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::BlockContext* AggluNyelvParser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 20, AggluNyelvParser::RuleBlock);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(180);
    match(AggluNyelvParser::BLOCK_OPEN);
    setState(184);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::BLOCK_OPEN)
      | (1ULL << AggluNyelvParser::OP_SUBTRACTION)
      | (1ULL << AggluNyelvParser::PREFIX_STD)
      | (1ULL << AggluNyelvParser::PARENTHESIS_OPEN)
      | (1ULL << AggluNyelvParser::LOOPVAR_VALUE_DEF_BEG)
      | (1ULL << AggluNyelvParser::CONDITIONAL_IF)
      | (1ULL << AggluNyelvParser::LOGICAL_NOT)
      | (1ULL << AggluNyelvParser::BOOLEAN_TRUE)
      | (1ULL << AggluNyelvParser::BOOLEAN_FALSE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0)) {
      setState(181);
      blockPart();
      setState(186);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(187);
    match(AggluNyelvParser::BLOCK_CLOSE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockPartContext ------------------------------------------------------------------

AggluNyelvParser::BlockPartContext::BlockPartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::StatementContext* AggluNyelvParser::BlockPartContext::statement() {
  return getRuleContext<AggluNyelvParser::StatementContext>(0);
}

AggluNyelvParser::BlockContext* AggluNyelvParser::BlockPartContext::block() {
  return getRuleContext<AggluNyelvParser::BlockContext>(0);
}

AggluNyelvParser::ConditionalContext* AggluNyelvParser::BlockPartContext::conditional() {
  return getRuleContext<AggluNyelvParser::ConditionalContext>(0);
}

AggluNyelvParser::LoopContext* AggluNyelvParser::BlockPartContext::loop() {
  return getRuleContext<AggluNyelvParser::LoopContext>(0);
}

AggluNyelvParser::FunctionCallContext* AggluNyelvParser::BlockPartContext::functionCall() {
  return getRuleContext<AggluNyelvParser::FunctionCallContext>(0);
}


size_t AggluNyelvParser::BlockPartContext::getRuleIndex() const {
  return AggluNyelvParser::RuleBlockPart;
}

void AggluNyelvParser::BlockPartContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBlockPart(this);
}

void AggluNyelvParser::BlockPartContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBlockPart(this);
}


std::any AggluNyelvParser::BlockPartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitBlockPart(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::BlockPartContext* AggluNyelvParser::blockPart() {
  BlockPartContext *_localctx = _tracker.createInstance<BlockPartContext>(_ctx, getState());
  enterRule(_localctx, 22, AggluNyelvParser::RuleBlockPart);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(194);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(189);
      statement();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(190);
      block();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(191);
      conditional();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(192);
      loop();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(193);
      functionCall();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConditionalContext ------------------------------------------------------------------

AggluNyelvParser::ConditionalContext::ConditionalContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ConditionalIfContext* AggluNyelvParser::ConditionalContext::conditionalIf() {
  return getRuleContext<AggluNyelvParser::ConditionalIfContext>(0);
}

std::vector<AggluNyelvParser::ConditionalElifContext *> AggluNyelvParser::ConditionalContext::conditionalElif() {
  return getRuleContexts<AggluNyelvParser::ConditionalElifContext>();
}

AggluNyelvParser::ConditionalElifContext* AggluNyelvParser::ConditionalContext::conditionalElif(size_t i) {
  return getRuleContext<AggluNyelvParser::ConditionalElifContext>(i);
}

AggluNyelvParser::ConditionalElseContext* AggluNyelvParser::ConditionalContext::conditionalElse() {
  return getRuleContext<AggluNyelvParser::ConditionalElseContext>(0);
}


size_t AggluNyelvParser::ConditionalContext::getRuleIndex() const {
  return AggluNyelvParser::RuleConditional;
}

void AggluNyelvParser::ConditionalContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterConditional(this);
}

void AggluNyelvParser::ConditionalContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitConditional(this);
}


std::any AggluNyelvParser::ConditionalContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitConditional(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ConditionalContext* AggluNyelvParser::conditional() {
  ConditionalContext *_localctx = _tracker.createInstance<ConditionalContext>(_ctx, getState());
  enterRule(_localctx, 24, AggluNyelvParser::RuleConditional);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(196);
    conditionalIf();
    setState(200);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(197);
        conditionalElif(); 
      }
      setState(202);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx);
    }
    setState(204);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == AggluNyelvParser::CONDITIONAL_ELSE) {
      setState(203);
      conditionalElse();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConditionalIfContext ------------------------------------------------------------------

AggluNyelvParser::ConditionalIfContext::ConditionalIfContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::ConditionalIfContext::CONDITIONAL_IF() {
  return getToken(AggluNyelvParser::CONDITIONAL_IF, 0);
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::ConditionalIfContext::logicalExpression() {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(0);
}

AggluNyelvParser::BlockContext* AggluNyelvParser::ConditionalIfContext::block() {
  return getRuleContext<AggluNyelvParser::BlockContext>(0);
}


size_t AggluNyelvParser::ConditionalIfContext::getRuleIndex() const {
  return AggluNyelvParser::RuleConditionalIf;
}

void AggluNyelvParser::ConditionalIfContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterConditionalIf(this);
}

void AggluNyelvParser::ConditionalIfContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitConditionalIf(this);
}


std::any AggluNyelvParser::ConditionalIfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitConditionalIf(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ConditionalIfContext* AggluNyelvParser::conditionalIf() {
  ConditionalIfContext *_localctx = _tracker.createInstance<ConditionalIfContext>(_ctx, getState());
  enterRule(_localctx, 26, AggluNyelvParser::RuleConditionalIf);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(206);
    match(AggluNyelvParser::CONDITIONAL_IF);
    setState(207);
    logicalExpression(0);
    setState(208);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConditionalElifContext ------------------------------------------------------------------

AggluNyelvParser::ConditionalElifContext::ConditionalElifContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::ConditionalElifContext::CONDITIONAL_ELSE() {
  return getToken(AggluNyelvParser::CONDITIONAL_ELSE, 0);
}

tree::TerminalNode* AggluNyelvParser::ConditionalElifContext::OP_LISTING() {
  return getToken(AggluNyelvParser::OP_LISTING, 0);
}

AggluNyelvParser::ConditionalIfContext* AggluNyelvParser::ConditionalElifContext::conditionalIf() {
  return getRuleContext<AggluNyelvParser::ConditionalIfContext>(0);
}


size_t AggluNyelvParser::ConditionalElifContext::getRuleIndex() const {
  return AggluNyelvParser::RuleConditionalElif;
}

void AggluNyelvParser::ConditionalElifContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterConditionalElif(this);
}

void AggluNyelvParser::ConditionalElifContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitConditionalElif(this);
}


std::any AggluNyelvParser::ConditionalElifContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitConditionalElif(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ConditionalElifContext* AggluNyelvParser::conditionalElif() {
  ConditionalElifContext *_localctx = _tracker.createInstance<ConditionalElifContext>(_ctx, getState());
  enterRule(_localctx, 28, AggluNyelvParser::RuleConditionalElif);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(210);
    match(AggluNyelvParser::CONDITIONAL_ELSE);
    setState(211);
    match(AggluNyelvParser::OP_LISTING);
    setState(212);
    conditionalIf();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConditionalElseContext ------------------------------------------------------------------

AggluNyelvParser::ConditionalElseContext::ConditionalElseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::ConditionalElseContext::CONDITIONAL_ELSE() {
  return getToken(AggluNyelvParser::CONDITIONAL_ELSE, 0);
}

AggluNyelvParser::BlockContext* AggluNyelvParser::ConditionalElseContext::block() {
  return getRuleContext<AggluNyelvParser::BlockContext>(0);
}


size_t AggluNyelvParser::ConditionalElseContext::getRuleIndex() const {
  return AggluNyelvParser::RuleConditionalElse;
}

void AggluNyelvParser::ConditionalElseContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterConditionalElse(this);
}

void AggluNyelvParser::ConditionalElseContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitConditionalElse(this);
}


std::any AggluNyelvParser::ConditionalElseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitConditionalElse(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ConditionalElseContext* AggluNyelvParser::conditionalElse() {
  ConditionalElseContext *_localctx = _tracker.createInstance<ConditionalElseContext>(_ctx, getState());
  enterRule(_localctx, 30, AggluNyelvParser::RuleConditionalElse);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(214);
    match(AggluNyelvParser::CONDITIONAL_ELSE);
    setState(215);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LoopContext ------------------------------------------------------------------

AggluNyelvParser::LoopContext::LoopContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::LoopvarDeclarationContext* AggluNyelvParser::LoopContext::loopvarDeclaration() {
  return getRuleContext<AggluNyelvParser::LoopvarDeclarationContext>(0);
}

AggluNyelvParser::LoopvarValueBegContext* AggluNyelvParser::LoopContext::loopvarValueBeg() {
  return getRuleContext<AggluNyelvParser::LoopvarValueBegContext>(0);
}

AggluNyelvParser::LoopvarValueEndContext* AggluNyelvParser::LoopContext::loopvarValueEnd() {
  return getRuleContext<AggluNyelvParser::LoopvarValueEndContext>(0);
}

AggluNyelvParser::LoopvarValueStepContext* AggluNyelvParser::LoopContext::loopvarValueStep() {
  return getRuleContext<AggluNyelvParser::LoopvarValueStepContext>(0);
}

AggluNyelvParser::BlockContext* AggluNyelvParser::LoopContext::block() {
  return getRuleContext<AggluNyelvParser::BlockContext>(0);
}


size_t AggluNyelvParser::LoopContext::getRuleIndex() const {
  return AggluNyelvParser::RuleLoop;
}

void AggluNyelvParser::LoopContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLoop(this);
}

void AggluNyelvParser::LoopContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLoop(this);
}


std::any AggluNyelvParser::LoopContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLoop(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::LoopContext* AggluNyelvParser::loop() {
  LoopContext *_localctx = _tracker.createInstance<LoopContext>(_ctx, getState());
  enterRule(_localctx, 32, AggluNyelvParser::RuleLoop);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(217);
    loopvarDeclaration();
    setState(218);
    loopvarValueBeg();
    setState(219);
    loopvarValueEnd();
    setState(220);
    loopvarValueStep();
    setState(221);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LoopvarDeclarationContext ------------------------------------------------------------------

AggluNyelvParser::LoopvarDeclarationContext::LoopvarDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::LoopvarDeclarationContext::LOOPVAR_VALUE_DEF_BEG() {
  return getToken(AggluNyelvParser::LOOPVAR_VALUE_DEF_BEG, 0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarDeclarationContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarDeclarationContext::LOOPVAR_VALUE_DEF_END() {
  return getToken(AggluNyelvParser::LOOPVAR_VALUE_DEF_END, 0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarDeclarationContext::BLOCK_OPEN() {
  return getToken(AggluNyelvParser::BLOCK_OPEN, 0);
}

AggluNyelvParser::VariableDeclarationPropertiesContext* AggluNyelvParser::LoopvarDeclarationContext::variableDeclarationProperties() {
  return getRuleContext<AggluNyelvParser::VariableDeclarationPropertiesContext>(0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarDeclarationContext::BLOCK_CLOSE() {
  return getToken(AggluNyelvParser::BLOCK_CLOSE, 0);
}


size_t AggluNyelvParser::LoopvarDeclarationContext::getRuleIndex() const {
  return AggluNyelvParser::RuleLoopvarDeclaration;
}

void AggluNyelvParser::LoopvarDeclarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLoopvarDeclaration(this);
}

void AggluNyelvParser::LoopvarDeclarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLoopvarDeclaration(this);
}


std::any AggluNyelvParser::LoopvarDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLoopvarDeclaration(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::LoopvarDeclarationContext* AggluNyelvParser::loopvarDeclaration() {
  LoopvarDeclarationContext *_localctx = _tracker.createInstance<LoopvarDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 34, AggluNyelvParser::RuleLoopvarDeclaration);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(223);
    match(AggluNyelvParser::LOOPVAR_VALUE_DEF_BEG);
    setState(224);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(225);
    match(AggluNyelvParser::LOOPVAR_VALUE_DEF_END);
    setState(226);
    match(AggluNyelvParser::BLOCK_OPEN);
    setState(227);
    variableDeclarationProperties();
    setState(228);
    match(AggluNyelvParser::BLOCK_CLOSE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LoopvarValueBegContext ------------------------------------------------------------------

AggluNyelvParser::LoopvarValueBegContext::LoopvarValueBegContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::LoopvarValueBegContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarValueBegContext::LOOPVAR_VALUE_BEG() {
  return getToken(AggluNyelvParser::LOOPVAR_VALUE_BEG, 0);
}


size_t AggluNyelvParser::LoopvarValueBegContext::getRuleIndex() const {
  return AggluNyelvParser::RuleLoopvarValueBeg;
}

void AggluNyelvParser::LoopvarValueBegContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLoopvarValueBeg(this);
}

void AggluNyelvParser::LoopvarValueBegContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLoopvarValueBeg(this);
}


std::any AggluNyelvParser::LoopvarValueBegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLoopvarValueBeg(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::LoopvarValueBegContext* AggluNyelvParser::loopvarValueBeg() {
  LoopvarValueBegContext *_localctx = _tracker.createInstance<LoopvarValueBegContext>(_ctx, getState());
  enterRule(_localctx, 36, AggluNyelvParser::RuleLoopvarValueBeg);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(230);
    expression(0);
    setState(231);
    match(AggluNyelvParser::LOOPVAR_VALUE_BEG);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LoopvarValueEndContext ------------------------------------------------------------------

AggluNyelvParser::LoopvarValueEndContext::LoopvarValueEndContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::LoopvarValueEndContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarValueEndContext::LOOPVAR_VALUE_END() {
  return getToken(AggluNyelvParser::LOOPVAR_VALUE_END, 0);
}


size_t AggluNyelvParser::LoopvarValueEndContext::getRuleIndex() const {
  return AggluNyelvParser::RuleLoopvarValueEnd;
}

void AggluNyelvParser::LoopvarValueEndContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLoopvarValueEnd(this);
}

void AggluNyelvParser::LoopvarValueEndContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLoopvarValueEnd(this);
}


std::any AggluNyelvParser::LoopvarValueEndContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLoopvarValueEnd(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::LoopvarValueEndContext* AggluNyelvParser::loopvarValueEnd() {
  LoopvarValueEndContext *_localctx = _tracker.createInstance<LoopvarValueEndContext>(_ctx, getState());
  enterRule(_localctx, 38, AggluNyelvParser::RuleLoopvarValueEnd);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(233);
    expression(0);
    setState(234);
    match(AggluNyelvParser::LOOPVAR_VALUE_END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LoopvarValueStepContext ------------------------------------------------------------------

AggluNyelvParser::LoopvarValueStepContext::LoopvarValueStepContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::LoopvarValueStepContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

tree::TerminalNode* AggluNyelvParser::LoopvarValueStepContext::LOOPVAR_VALUE_STEP() {
  return getToken(AggluNyelvParser::LOOPVAR_VALUE_STEP, 0);
}


size_t AggluNyelvParser::LoopvarValueStepContext::getRuleIndex() const {
  return AggluNyelvParser::RuleLoopvarValueStep;
}

void AggluNyelvParser::LoopvarValueStepContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLoopvarValueStep(this);
}

void AggluNyelvParser::LoopvarValueStepContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLoopvarValueStep(this);
}


std::any AggluNyelvParser::LoopvarValueStepContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLoopvarValueStep(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::LoopvarValueStepContext* AggluNyelvParser::loopvarValueStep() {
  LoopvarValueStepContext *_localctx = _tracker.createInstance<LoopvarValueStepContext>(_ctx, getState());
  enterRule(_localctx, 40, AggluNyelvParser::RuleLoopvarValueStep);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(236);
    expression(0);
    setState(237);
    match(AggluNyelvParser::LOOPVAR_VALUE_STEP);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementBodyContext ------------------------------------------------------------------

AggluNyelvParser::StatementBodyContext::StatementBodyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::StatementBodyContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::StatementBodyContext::logicalExpression() {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(0);
}

AggluNyelvParser::VariableAssignmentContext* AggluNyelvParser::StatementBodyContext::variableAssignment() {
  return getRuleContext<AggluNyelvParser::VariableAssignmentContext>(0);
}

AggluNyelvParser::VariableDefinitionContext* AggluNyelvParser::StatementBodyContext::variableDefinition() {
  return getRuleContext<AggluNyelvParser::VariableDefinitionContext>(0);
}

AggluNyelvParser::ArrayElementAssignmentContext* AggluNyelvParser::StatementBodyContext::arrayElementAssignment() {
  return getRuleContext<AggluNyelvParser::ArrayElementAssignmentContext>(0);
}


size_t AggluNyelvParser::StatementBodyContext::getRuleIndex() const {
  return AggluNyelvParser::RuleStatementBody;
}

void AggluNyelvParser::StatementBodyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStatementBody(this);
}

void AggluNyelvParser::StatementBodyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStatementBody(this);
}


std::any AggluNyelvParser::StatementBodyContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitStatementBody(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::StatementBodyContext* AggluNyelvParser::statementBody() {
  StatementBodyContext *_localctx = _tracker.createInstance<StatementBodyContext>(_ctx, getState());
  enterRule(_localctx, 42, AggluNyelvParser::RuleStatementBody);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(244);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 19, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(239);
      expression(0);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(240);
      logicalExpression(0);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(241);
      variableAssignment();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(242);
      variableDefinition();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(243);
      arrayElementAssignment();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

AggluNyelvParser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::StatementBodyContext* AggluNyelvParser::StatementContext::statementBody() {
  return getRuleContext<AggluNyelvParser::StatementBodyContext>(0);
}

tree::TerminalNode* AggluNyelvParser::StatementContext::TERMINATOR() {
  return getToken(AggluNyelvParser::TERMINATOR, 0);
}


size_t AggluNyelvParser::StatementContext::getRuleIndex() const {
  return AggluNyelvParser::RuleStatement;
}

void AggluNyelvParser::StatementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStatement(this);
}

void AggluNyelvParser::StatementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStatement(this);
}


std::any AggluNyelvParser::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::StatementContext* AggluNyelvParser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 44, AggluNyelvParser::RuleStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(246);
    statementBody();
    setState(247);
    match(AggluNyelvParser::TERMINATOR);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LogicalExpressionContext ------------------------------------------------------------------

AggluNyelvParser::LogicalExpressionContext::LogicalExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t AggluNyelvParser::LogicalExpressionContext::getRuleIndex() const {
  return AggluNyelvParser::RuleLogicalExpression;
}

void AggluNyelvParser::LogicalExpressionContext::copyFrom(LogicalExpressionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- LogicExprNotContext ------------------------------------------------------------------

tree::TerminalNode* AggluNyelvParser::LogicExprNotContext::LOGICAL_NOT() {
  return getToken(AggluNyelvParser::LOGICAL_NOT, 0);
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::LogicExprNotContext::logicalExpression() {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(0);
}

AggluNyelvParser::LogicExprNotContext::LogicExprNotContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprNotContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprNot(this);
}
void AggluNyelvParser::LogicExprNotContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprNot(this);
}

std::any AggluNyelvParser::LogicExprNotContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprNot(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprBooleanContext ------------------------------------------------------------------

AggluNyelvParser::BooleanContext* AggluNyelvParser::LogicExprBooleanContext::boolean() {
  return getRuleContext<AggluNyelvParser::BooleanContext>(0);
}

AggluNyelvParser::LogicExprBooleanContext::LogicExprBooleanContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprBooleanContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprBoolean(this);
}
void AggluNyelvParser::LogicExprBooleanContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprBoolean(this);
}

std::any AggluNyelvParser::LogicExprBooleanContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprBoolean(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprCompExpressionsContext ------------------------------------------------------------------

std::vector<AggluNyelvParser::ExpressionContext *> AggluNyelvParser::LogicExprCompExpressionsContext::expression() {
  return getRuleContexts<AggluNyelvParser::ExpressionContext>();
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::LogicExprCompExpressionsContext::expression(size_t i) {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(i);
}

AggluNyelvParser::OpComparisonContext* AggluNyelvParser::LogicExprCompExpressionsContext::opComparison() {
  return getRuleContext<AggluNyelvParser::OpComparisonContext>(0);
}

AggluNyelvParser::LogicExprCompExpressionsContext::LogicExprCompExpressionsContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprCompExpressionsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprCompExpressions(this);
}
void AggluNyelvParser::LogicExprCompExpressionsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprCompExpressions(this);
}

std::any AggluNyelvParser::LogicExprCompExpressionsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprCompExpressions(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprCompareContext ------------------------------------------------------------------

std::vector<AggluNyelvParser::LogicalExpressionContext *> AggluNyelvParser::LogicExprCompareContext::logicalExpression() {
  return getRuleContexts<AggluNyelvParser::LogicalExpressionContext>();
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::LogicExprCompareContext::logicalExpression(size_t i) {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(i);
}

AggluNyelvParser::OpLogicalComparisonContext* AggluNyelvParser::LogicExprCompareContext::opLogicalComparison() {
  return getRuleContext<AggluNyelvParser::OpLogicalComparisonContext>(0);
}

AggluNyelvParser::LogicExprCompareContext::LogicExprCompareContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprCompareContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprCompare(this);
}
void AggluNyelvParser::LogicExprCompareContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprCompare(this);
}

std::any AggluNyelvParser::LogicExprCompareContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprCompare(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprAndContext ------------------------------------------------------------------

std::vector<AggluNyelvParser::LogicalExpressionContext *> AggluNyelvParser::LogicExprAndContext::logicalExpression() {
  return getRuleContexts<AggluNyelvParser::LogicalExpressionContext>();
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::LogicExprAndContext::logicalExpression(size_t i) {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(i);
}

tree::TerminalNode* AggluNyelvParser::LogicExprAndContext::LOGICAL_AND() {
  return getToken(AggluNyelvParser::LOGICAL_AND, 0);
}

AggluNyelvParser::LogicExprAndContext::LogicExprAndContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprAndContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprAnd(this);
}
void AggluNyelvParser::LogicExprAndContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprAnd(this);
}

std::any AggluNyelvParser::LogicExprAndContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprAnd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprInParenthesisContext ------------------------------------------------------------------

tree::TerminalNode* AggluNyelvParser::LogicExprInParenthesisContext::PARENTHESIS_OPEN() {
  return getToken(AggluNyelvParser::PARENTHESIS_OPEN, 0);
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::LogicExprInParenthesisContext::logicalExpression() {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(0);
}

tree::TerminalNode* AggluNyelvParser::LogicExprInParenthesisContext::PARENTHESIS_CLOSE() {
  return getToken(AggluNyelvParser::PARENTHESIS_CLOSE, 0);
}

AggluNyelvParser::LogicExprInParenthesisContext::LogicExprInParenthesisContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprInParenthesisContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprInParenthesis(this);
}
void AggluNyelvParser::LogicExprInParenthesisContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprInParenthesis(this);
}

std::any AggluNyelvParser::LogicExprInParenthesisContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprInParenthesis(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprOrContext ------------------------------------------------------------------

std::vector<AggluNyelvParser::LogicalExpressionContext *> AggluNyelvParser::LogicExprOrContext::logicalExpression() {
  return getRuleContexts<AggluNyelvParser::LogicalExpressionContext>();
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::LogicExprOrContext::logicalExpression(size_t i) {
  return getRuleContext<AggluNyelvParser::LogicalExpressionContext>(i);
}

tree::TerminalNode* AggluNyelvParser::LogicExprOrContext::LOGICAL_OR() {
  return getToken(AggluNyelvParser::LOGICAL_OR, 0);
}

AggluNyelvParser::LogicExprOrContext::LogicExprOrContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprOrContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprOr(this);
}
void AggluNyelvParser::LogicExprOrContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprOr(this);
}

std::any AggluNyelvParser::LogicExprOrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprOr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LogicExprFunContext ------------------------------------------------------------------

AggluNyelvParser::FunctionCallContext* AggluNyelvParser::LogicExprFunContext::functionCall() {
  return getRuleContext<AggluNyelvParser::FunctionCallContext>(0);
}

AggluNyelvParser::LogicExprFunContext::LogicExprFunContext(LogicalExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::LogicExprFunContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicExprFun(this);
}
void AggluNyelvParser::LogicExprFunContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicExprFun(this);
}

std::any AggluNyelvParser::LogicExprFunContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitLogicExprFun(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::logicalExpression() {
   return logicalExpression(0);
}

AggluNyelvParser::LogicalExpressionContext* AggluNyelvParser::logicalExpression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  AggluNyelvParser::LogicalExpressionContext *_localctx = _tracker.createInstance<LogicalExpressionContext>(_ctx, parentState);
  AggluNyelvParser::LogicalExpressionContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 46;
  enterRecursionRule(_localctx, 46, AggluNyelvParser::RuleLogicalExpression, precedence);

    

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(262);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<LogicExprNotContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(250);
      match(AggluNyelvParser::LOGICAL_NOT);
      setState(251);
      logicalExpression(8);
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<LogicExprBooleanContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(252);
      boolean();
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<LogicExprFunContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(253);
      functionCall();
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<LogicExprCompExpressionsContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(254);
      expression(0);
      setState(255);
      opComparison();
      setState(256);
      expression(0);
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<LogicExprInParenthesisContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(258);
      match(AggluNyelvParser::PARENTHESIS_OPEN);
      setState(259);
      logicalExpression(0);
      setState(260);
      match(AggluNyelvParser::PARENTHESIS_CLOSE);
      break;
    }

    default:
      break;
    }
    _ctx->stop = _input->LT(-1);
    setState(276);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(274);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<LogicExprOrContext>(_tracker.createInstance<LogicalExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleLogicalExpression);
          setState(264);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(265);
          match(AggluNyelvParser::LOGICAL_OR);
          setState(266);
          logicalExpression(8);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<LogicExprAndContext>(_tracker.createInstance<LogicalExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleLogicalExpression);
          setState(267);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(268);
          match(AggluNyelvParser::LOGICAL_AND);
          setState(269);
          logicalExpression(7);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<LogicExprCompareContext>(_tracker.createInstance<LogicalExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleLogicalExpression);
          setState(270);

          if (!(precpred(_ctx, 2))) throw FailedPredicateException(this, "precpred(_ctx, 2)");
          setState(271);
          opLogicalComparison();
          setState(272);
          logicalExpression(3);
          break;
        }

        default:
          break;
        } 
      }
      setState(278);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- OpLogicalComparisonContext ------------------------------------------------------------------

AggluNyelvParser::OpLogicalComparisonContext::OpLogicalComparisonContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::OpLogicalComparisonContext::OP_COMP_EQUALS() {
  return getToken(AggluNyelvParser::OP_COMP_EQUALS, 0);
}

tree::TerminalNode* AggluNyelvParser::OpLogicalComparisonContext::OP_COMP_NOTEQUALS() {
  return getToken(AggluNyelvParser::OP_COMP_NOTEQUALS, 0);
}


size_t AggluNyelvParser::OpLogicalComparisonContext::getRuleIndex() const {
  return AggluNyelvParser::RuleOpLogicalComparison;
}

void AggluNyelvParser::OpLogicalComparisonContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterOpLogicalComparison(this);
}

void AggluNyelvParser::OpLogicalComparisonContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitOpLogicalComparison(this);
}


std::any AggluNyelvParser::OpLogicalComparisonContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitOpLogicalComparison(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::OpLogicalComparisonContext* AggluNyelvParser::opLogicalComparison() {
  OpLogicalComparisonContext *_localctx = _tracker.createInstance<OpLogicalComparisonContext>(_ctx, getState());
  enterRule(_localctx, 48, AggluNyelvParser::RuleOpLogicalComparison);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(279);
    _la = _input->LA(1);
    if (!(_la == AggluNyelvParser::OP_COMP_EQUALS

    || _la == AggluNyelvParser::OP_COMP_NOTEQUALS)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- OpComparisonContext ------------------------------------------------------------------

AggluNyelvParser::OpComparisonContext::OpComparisonContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::OpComparisonContext::OP_COMP_EQUALS() {
  return getToken(AggluNyelvParser::OP_COMP_EQUALS, 0);
}

tree::TerminalNode* AggluNyelvParser::OpComparisonContext::OP_COMP_NOTEQUALS() {
  return getToken(AggluNyelvParser::OP_COMP_NOTEQUALS, 0);
}

tree::TerminalNode* AggluNyelvParser::OpComparisonContext::OP_COMP_LESS_THAN() {
  return getToken(AggluNyelvParser::OP_COMP_LESS_THAN, 0);
}

tree::TerminalNode* AggluNyelvParser::OpComparisonContext::OP_COMP_LESS_OR_EQUAL_THAN() {
  return getToken(AggluNyelvParser::OP_COMP_LESS_OR_EQUAL_THAN, 0);
}

tree::TerminalNode* AggluNyelvParser::OpComparisonContext::OP_COMP_GREATER_THAN() {
  return getToken(AggluNyelvParser::OP_COMP_GREATER_THAN, 0);
}

tree::TerminalNode* AggluNyelvParser::OpComparisonContext::OP_COMP_GREATER_OR_EQUAL_THAN() {
  return getToken(AggluNyelvParser::OP_COMP_GREATER_OR_EQUAL_THAN, 0);
}


size_t AggluNyelvParser::OpComparisonContext::getRuleIndex() const {
  return AggluNyelvParser::RuleOpComparison;
}

void AggluNyelvParser::OpComparisonContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterOpComparison(this);
}

void AggluNyelvParser::OpComparisonContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitOpComparison(this);
}


std::any AggluNyelvParser::OpComparisonContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitOpComparison(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::OpComparisonContext* AggluNyelvParser::opComparison() {
  OpComparisonContext *_localctx = _tracker.createInstance<OpComparisonContext>(_ctx, getState());
  enterRule(_localctx, 50, AggluNyelvParser::RuleOpComparison);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(281);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::OP_COMP_EQUALS)
      | (1ULL << AggluNyelvParser::OP_COMP_NOTEQUALS)
      | (1ULL << AggluNyelvParser::OP_COMP_LESS_THAN)
      | (1ULL << AggluNyelvParser::OP_COMP_LESS_OR_EQUAL_THAN)
      | (1ULL << AggluNyelvParser::OP_COMP_GREATER_THAN)
      | (1ULL << AggluNyelvParser::OP_COMP_GREATER_OR_EQUAL_THAN))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

AggluNyelvParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t AggluNyelvParser::ExpressionContext::getRuleIndex() const {
  return AggluNyelvParser::RuleExpression;
}

void AggluNyelvParser::ExpressionContext::copyFrom(ExpressionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ExprGrpIntContext ------------------------------------------------------------------

tree::TerminalNode* AggluNyelvParser::ExprGrpIntContext::INTEGER() {
  return getToken(AggluNyelvParser::INTEGER, 0);
}

AggluNyelvParser::ExprGrpIntContext::ExprGrpIntContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpIntContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpInt(this);
}
void AggluNyelvParser::ExprGrpIntContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpInt(this);
}

std::any AggluNyelvParser::ExprGrpIntContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpInt(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpFunContext ------------------------------------------------------------------

AggluNyelvParser::FunctionCallContext* AggluNyelvParser::ExprGrpFunContext::functionCall() {
  return getRuleContext<AggluNyelvParser::FunctionCallContext>(0);
}

AggluNyelvParser::ExprGrpFunContext::ExprGrpFunContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpFunContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpFun(this);
}
void AggluNyelvParser::ExprGrpFunContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpFun(this);
}

std::any AggluNyelvParser::ExprGrpFunContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpFun(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpNegContext ------------------------------------------------------------------

tree::TerminalNode* AggluNyelvParser::ExprGrpNegContext::OP_SUBTRACTION() {
  return getToken(AggluNyelvParser::OP_SUBTRACTION, 0);
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::ExprGrpNegContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

AggluNyelvParser::ExprGrpNegContext::ExprGrpNegContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpNegContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpNeg(this);
}
void AggluNyelvParser::ExprGrpNegContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpNeg(this);
}

std::any AggluNyelvParser::ExprGrpNegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpNeg(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpArrContext ------------------------------------------------------------------

AggluNyelvParser::ArrayElementAccessContext* AggluNyelvParser::ExprGrpArrContext::arrayElementAccess() {
  return getRuleContext<AggluNyelvParser::ArrayElementAccessContext>(0);
}

AggluNyelvParser::ExprGrpArrContext::ExprGrpArrContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpArrContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpArr(this);
}
void AggluNyelvParser::ExprGrpArrContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpArr(this);
}

std::any AggluNyelvParser::ExprGrpArrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpArr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpParContext ------------------------------------------------------------------

tree::TerminalNode* AggluNyelvParser::ExprGrpParContext::PARENTHESIS_OPEN() {
  return getToken(AggluNyelvParser::PARENTHESIS_OPEN, 0);
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::ExprGrpParContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

tree::TerminalNode* AggluNyelvParser::ExprGrpParContext::PARENTHESIS_CLOSE() {
  return getToken(AggluNyelvParser::PARENTHESIS_CLOSE, 0);
}

AggluNyelvParser::ExprGrpParContext::ExprGrpParContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpParContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpPar(this);
}
void AggluNyelvParser::ExprGrpParContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpPar(this);
}

std::any AggluNyelvParser::ExprGrpParContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpPar(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpAddContext ------------------------------------------------------------------

std::vector<AggluNyelvParser::ExpressionContext *> AggluNyelvParser::ExprGrpAddContext::expression() {
  return getRuleContexts<AggluNyelvParser::ExpressionContext>();
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::ExprGrpAddContext::expression(size_t i) {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(i);
}

tree::TerminalNode* AggluNyelvParser::ExprGrpAddContext::OP_ADDITION() {
  return getToken(AggluNyelvParser::OP_ADDITION, 0);
}

tree::TerminalNode* AggluNyelvParser::ExprGrpAddContext::OP_SUBTRACTION() {
  return getToken(AggluNyelvParser::OP_SUBTRACTION, 0);
}

AggluNyelvParser::ExprGrpAddContext::ExprGrpAddContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpAddContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpAdd(this);
}
void AggluNyelvParser::ExprGrpAddContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpAdd(this);
}

std::any AggluNyelvParser::ExprGrpAddContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpAdd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpSymContext ------------------------------------------------------------------

tree::TerminalNode* AggluNyelvParser::ExprGrpSymContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

AggluNyelvParser::ExprGrpSymContext::ExprGrpSymContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpSymContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpSym(this);
}
void AggluNyelvParser::ExprGrpSymContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpSym(this);
}

std::any AggluNyelvParser::ExprGrpSymContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpSym(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGrpMulContext ------------------------------------------------------------------

std::vector<AggluNyelvParser::ExpressionContext *> AggluNyelvParser::ExprGrpMulContext::expression() {
  return getRuleContexts<AggluNyelvParser::ExpressionContext>();
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::ExprGrpMulContext::expression(size_t i) {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(i);
}

tree::TerminalNode* AggluNyelvParser::ExprGrpMulContext::OP_MULTIPLICATION() {
  return getToken(AggluNyelvParser::OP_MULTIPLICATION, 0);
}

tree::TerminalNode* AggluNyelvParser::ExprGrpMulContext::OP_DIVISION() {
  return getToken(AggluNyelvParser::OP_DIVISION, 0);
}

AggluNyelvParser::ExprGrpMulContext::ExprGrpMulContext(ExpressionContext *ctx) { copyFrom(ctx); }

void AggluNyelvParser::ExprGrpMulContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExprGrpMul(this);
}
void AggluNyelvParser::ExprGrpMulContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExprGrpMul(this);
}

std::any AggluNyelvParser::ExprGrpMulContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitExprGrpMul(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::expression() {
   return expression(0);
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::expression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  AggluNyelvParser::ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, parentState);
  AggluNyelvParser::ExpressionContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 52;
  enterRecursionRule(_localctx, 52, AggluNyelvParser::RuleExpression, precedence);

    size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(294);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 23, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<ExprGrpNegContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(284);
      match(AggluNyelvParser::OP_SUBTRACTION);
      setState(285);
      expression(8);
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<ExprGrpIntContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(286);
      match(AggluNyelvParser::INTEGER);
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<ExprGrpSymContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(287);
      match(AggluNyelvParser::SYMBOL_NAME);
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<ExprGrpArrContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(288);
      arrayElementAccess();
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<ExprGrpFunContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(289);
      functionCall();
      break;
    }

    case 6: {
      _localctx = _tracker.createInstance<ExprGrpParContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(290);
      match(AggluNyelvParser::PARENTHESIS_OPEN);
      setState(291);
      expression(0);
      setState(292);
      match(AggluNyelvParser::PARENTHESIS_CLOSE);
      break;
    }

    default:
      break;
    }
    _ctx->stop = _input->LT(-1);
    setState(304);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 25, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(302);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<ExprGrpMulContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(296);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(297);
          _la = _input->LA(1);
          if (!(_la == AggluNyelvParser::OP_MULTIPLICATION

          || _la == AggluNyelvParser::OP_DIVISION)) {
          _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(298);
          expression(8);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<ExprGrpAddContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(299);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(300);
          _la = _input->LA(1);
          if (!(_la == AggluNyelvParser::OP_ADDITION

          || _la == AggluNyelvParser::OP_SUBTRACTION)) {
          _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(301);
          expression(7);
          break;
        }

        default:
          break;
        } 
      }
      setState(306);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 25, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- ArrayElementAccessContext ------------------------------------------------------------------

AggluNyelvParser::ArrayElementAccessContext::ArrayElementAccessContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::ArrayElementAccessContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::ArrayElementAccessContext::INTEGER() {
  return getToken(AggluNyelvParser::INTEGER, 0);
}

tree::TerminalNode* AggluNyelvParser::ArrayElementAccessContext::OP_DOT() {
  return getToken(AggluNyelvParser::OP_DOT, 0);
}

tree::TerminalNode* AggluNyelvParser::ArrayElementAccessContext::OP_ARR_ELEM_SELECTOR_DECOR() {
  return getToken(AggluNyelvParser::OP_ARR_ELEM_SELECTOR_DECOR, 0);
}


size_t AggluNyelvParser::ArrayElementAccessContext::getRuleIndex() const {
  return AggluNyelvParser::RuleArrayElementAccess;
}

void AggluNyelvParser::ArrayElementAccessContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArrayElementAccess(this);
}

void AggluNyelvParser::ArrayElementAccessContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArrayElementAccess(this);
}


std::any AggluNyelvParser::ArrayElementAccessContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitArrayElementAccess(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ArrayElementAccessContext* AggluNyelvParser::arrayElementAccess() {
  ArrayElementAccessContext *_localctx = _tracker.createInstance<ArrayElementAccessContext>(_ctx, getState());
  enterRule(_localctx, 54, AggluNyelvParser::RuleArrayElementAccess);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(307);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(308);
    match(AggluNyelvParser::INTEGER);
    setState(309);
    match(AggluNyelvParser::OP_DOT);
    setState(310);
    match(AggluNyelvParser::OP_ARR_ELEM_SELECTOR_DECOR);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArrayElementAssignmentContext ------------------------------------------------------------------

AggluNyelvParser::ArrayElementAssignmentContext::ArrayElementAssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ArrayElementAccessContext* AggluNyelvParser::ArrayElementAssignmentContext::arrayElementAccess() {
  return getRuleContext<AggluNyelvParser::ArrayElementAccessContext>(0);
}

tree::TerminalNode* AggluNyelvParser::ArrayElementAssignmentContext::OP_ASSIGN() {
  return getToken(AggluNyelvParser::OP_ASSIGN, 0);
}

AggluNyelvParser::VariableValueContext* AggluNyelvParser::ArrayElementAssignmentContext::variableValue() {
  return getRuleContext<AggluNyelvParser::VariableValueContext>(0);
}


size_t AggluNyelvParser::ArrayElementAssignmentContext::getRuleIndex() const {
  return AggluNyelvParser::RuleArrayElementAssignment;
}

void AggluNyelvParser::ArrayElementAssignmentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArrayElementAssignment(this);
}

void AggluNyelvParser::ArrayElementAssignmentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArrayElementAssignment(this);
}


std::any AggluNyelvParser::ArrayElementAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitArrayElementAssignment(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::ArrayElementAssignmentContext* AggluNyelvParser::arrayElementAssignment() {
  ArrayElementAssignmentContext *_localctx = _tracker.createInstance<ArrayElementAssignmentContext>(_ctx, getState());
  enterRule(_localctx, 56, AggluNyelvParser::RuleArrayElementAssignment);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(312);
    arrayElementAccess();
    setState(313);
    match(AggluNyelvParser::OP_ASSIGN);
    setState(314);
    variableValue();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDefinitionContext ------------------------------------------------------------------

AggluNyelvParser::VariableDefinitionContext::VariableDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::VariableDeclarationContext* AggluNyelvParser::VariableDefinitionContext::variableDeclaration() {
  return getRuleContext<AggluNyelvParser::VariableDeclarationContext>(0);
}

tree::TerminalNode* AggluNyelvParser::VariableDefinitionContext::OP_ASSIGN() {
  return getToken(AggluNyelvParser::OP_ASSIGN, 0);
}

std::vector<AggluNyelvParser::VariableValueContext *> AggluNyelvParser::VariableDefinitionContext::variableValue() {
  return getRuleContexts<AggluNyelvParser::VariableValueContext>();
}

AggluNyelvParser::VariableValueContext* AggluNyelvParser::VariableDefinitionContext::variableValue(size_t i) {
  return getRuleContext<AggluNyelvParser::VariableValueContext>(i);
}

std::vector<tree::TerminalNode *> AggluNyelvParser::VariableDefinitionContext::OP_LISTING() {
  return getTokens(AggluNyelvParser::OP_LISTING);
}

tree::TerminalNode* AggluNyelvParser::VariableDefinitionContext::OP_LISTING(size_t i) {
  return getToken(AggluNyelvParser::OP_LISTING, i);
}


size_t AggluNyelvParser::VariableDefinitionContext::getRuleIndex() const {
  return AggluNyelvParser::RuleVariableDefinition;
}

void AggluNyelvParser::VariableDefinitionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariableDefinition(this);
}

void AggluNyelvParser::VariableDefinitionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariableDefinition(this);
}


std::any AggluNyelvParser::VariableDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitVariableDefinition(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::VariableDefinitionContext* AggluNyelvParser::variableDefinition() {
  VariableDefinitionContext *_localctx = _tracker.createInstance<VariableDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 58, AggluNyelvParser::RuleVariableDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(316);
    variableDeclaration();
    setState(317);
    match(AggluNyelvParser::OP_ASSIGN);
    setState(323);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(318);
        variableValue();
        setState(319);
        match(AggluNyelvParser::OP_LISTING); 
      }
      setState(325);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    }
    setState(327); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(326);
      variableValue();
      setState(329); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::OP_DEFAULT_VALUE)
      | (1ULL << AggluNyelvParser::OP_SUBTRACTION)
      | (1ULL << AggluNyelvParser::PREFIX_STD)
      | (1ULL << AggluNyelvParser::PARENTHESIS_OPEN)
      | (1ULL << AggluNyelvParser::BOOLEAN_TRUE)
      | (1ULL << AggluNyelvParser::BOOLEAN_FALSE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::TEXT)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationContext ------------------------------------------------------------------

AggluNyelvParser::VariableDeclarationContext::VariableDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationContext::BLOCK_OPEN() {
  return getToken(AggluNyelvParser::BLOCK_OPEN, 0);
}

AggluNyelvParser::VariableDeclarationPropertiesContext* AggluNyelvParser::VariableDeclarationContext::variableDeclarationProperties() {
  return getRuleContext<AggluNyelvParser::VariableDeclarationPropertiesContext>(0);
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationContext::BLOCK_CLOSE() {
  return getToken(AggluNyelvParser::BLOCK_CLOSE, 0);
}


size_t AggluNyelvParser::VariableDeclarationContext::getRuleIndex() const {
  return AggluNyelvParser::RuleVariableDeclaration;
}

void AggluNyelvParser::VariableDeclarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariableDeclaration(this);
}

void AggluNyelvParser::VariableDeclarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariableDeclaration(this);
}


std::any AggluNyelvParser::VariableDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitVariableDeclaration(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::VariableDeclarationContext* AggluNyelvParser::variableDeclaration() {
  VariableDeclarationContext *_localctx = _tracker.createInstance<VariableDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 60, AggluNyelvParser::RuleVariableDeclaration);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(331);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(332);
    match(AggluNyelvParser::BLOCK_OPEN);
    setState(333);
    variableDeclarationProperties();
    setState(334);
    match(AggluNyelvParser::BLOCK_CLOSE);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationPropertiesContext ------------------------------------------------------------------

AggluNyelvParser::VariableDeclarationPropertiesContext::VariableDeclarationPropertiesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationPropertiesContext::OP_LISTING() {
  return getToken(AggluNyelvParser::OP_LISTING, 0);
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationPropertiesContext::CONSTNESS() {
  return getToken(AggluNyelvParser::CONSTNESS, 0);
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationPropertiesContext::TYPE() {
  return getToken(AggluNyelvParser::TYPE, 0);
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationPropertiesContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::VariableDeclarationPropertiesContext::INTEGER() {
  return getToken(AggluNyelvParser::INTEGER, 0);
}


size_t AggluNyelvParser::VariableDeclarationPropertiesContext::getRuleIndex() const {
  return AggluNyelvParser::RuleVariableDeclarationProperties;
}

void AggluNyelvParser::VariableDeclarationPropertiesContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariableDeclarationProperties(this);
}

void AggluNyelvParser::VariableDeclarationPropertiesContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariableDeclarationProperties(this);
}


std::any AggluNyelvParser::VariableDeclarationPropertiesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitVariableDeclarationProperties(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::VariableDeclarationPropertiesContext* AggluNyelvParser::variableDeclarationProperties() {
  VariableDeclarationPropertiesContext *_localctx = _tracker.createInstance<VariableDeclarationPropertiesContext>(_ctx, getState());
  enterRule(_localctx, 62, AggluNyelvParser::RuleVariableDeclarationProperties);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(337);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == AggluNyelvParser::INTEGER) {
      setState(336);
      match(AggluNyelvParser::INTEGER);
    }
    setState(339);
    _la = _input->LA(1);
    if (!(_la == AggluNyelvParser::TYPE

    || _la == AggluNyelvParser::SYMBOL_NAME)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(341);
    match(AggluNyelvParser::OP_LISTING);

    setState(342);
    match(AggluNyelvParser::CONSTNESS);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableAssignmentContext ------------------------------------------------------------------

AggluNyelvParser::VariableAssignmentContext::VariableAssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::VariableAssignmentContext::SYMBOL_NAME() {
  return getToken(AggluNyelvParser::SYMBOL_NAME, 0);
}

tree::TerminalNode* AggluNyelvParser::VariableAssignmentContext::OP_ASSIGN() {
  return getToken(AggluNyelvParser::OP_ASSIGN, 0);
}

std::vector<AggluNyelvParser::VariableValueContext *> AggluNyelvParser::VariableAssignmentContext::variableValue() {
  return getRuleContexts<AggluNyelvParser::VariableValueContext>();
}

AggluNyelvParser::VariableValueContext* AggluNyelvParser::VariableAssignmentContext::variableValue(size_t i) {
  return getRuleContext<AggluNyelvParser::VariableValueContext>(i);
}

std::vector<tree::TerminalNode *> AggluNyelvParser::VariableAssignmentContext::OP_LISTING() {
  return getTokens(AggluNyelvParser::OP_LISTING);
}

tree::TerminalNode* AggluNyelvParser::VariableAssignmentContext::OP_LISTING(size_t i) {
  return getToken(AggluNyelvParser::OP_LISTING, i);
}


size_t AggluNyelvParser::VariableAssignmentContext::getRuleIndex() const {
  return AggluNyelvParser::RuleVariableAssignment;
}

void AggluNyelvParser::VariableAssignmentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariableAssignment(this);
}

void AggluNyelvParser::VariableAssignmentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariableAssignment(this);
}


std::any AggluNyelvParser::VariableAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitVariableAssignment(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::VariableAssignmentContext* AggluNyelvParser::variableAssignment() {
  VariableAssignmentContext *_localctx = _tracker.createInstance<VariableAssignmentContext>(_ctx, getState());
  enterRule(_localctx, 64, AggluNyelvParser::RuleVariableAssignment);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(344);
    match(AggluNyelvParser::SYMBOL_NAME);
    setState(345);
    match(AggluNyelvParser::OP_ASSIGN);
    setState(351);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 29, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(346);
        variableValue();
        setState(347);
        match(AggluNyelvParser::OP_LISTING); 
      }
      setState(353);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 29, _ctx);
    }
    setState(355); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(354);
      variableValue();
      setState(357); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << AggluNyelvParser::OP_DEFAULT_VALUE)
      | (1ULL << AggluNyelvParser::OP_SUBTRACTION)
      | (1ULL << AggluNyelvParser::PREFIX_STD)
      | (1ULL << AggluNyelvParser::PARENTHESIS_OPEN)
      | (1ULL << AggluNyelvParser::BOOLEAN_TRUE)
      | (1ULL << AggluNyelvParser::BOOLEAN_FALSE)
      | (1ULL << AggluNyelvParser::INTEGER)
      | (1ULL << AggluNyelvParser::TEXT)
      | (1ULL << AggluNyelvParser::SYMBOL_NAME))) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableValueContext ------------------------------------------------------------------

AggluNyelvParser::VariableValueContext::VariableValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

AggluNyelvParser::ExpressionContext* AggluNyelvParser::VariableValueContext::expression() {
  return getRuleContext<AggluNyelvParser::ExpressionContext>(0);
}

AggluNyelvParser::TextContext* AggluNyelvParser::VariableValueContext::text() {
  return getRuleContext<AggluNyelvParser::TextContext>(0);
}

AggluNyelvParser::BooleanContext* AggluNyelvParser::VariableValueContext::boolean() {
  return getRuleContext<AggluNyelvParser::BooleanContext>(0);
}

tree::TerminalNode* AggluNyelvParser::VariableValueContext::OP_DEFAULT_VALUE() {
  return getToken(AggluNyelvParser::OP_DEFAULT_VALUE, 0);
}


size_t AggluNyelvParser::VariableValueContext::getRuleIndex() const {
  return AggluNyelvParser::RuleVariableValue;
}

void AggluNyelvParser::VariableValueContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariableValue(this);
}

void AggluNyelvParser::VariableValueContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariableValue(this);
}


std::any AggluNyelvParser::VariableValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitVariableValue(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::VariableValueContext* AggluNyelvParser::variableValue() {
  VariableValueContext *_localctx = _tracker.createInstance<VariableValueContext>(_ctx, getState());
  enterRule(_localctx, 66, AggluNyelvParser::RuleVariableValue);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(363);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case AggluNyelvParser::OP_SUBTRACTION:
      case AggluNyelvParser::PREFIX_STD:
      case AggluNyelvParser::PARENTHESIS_OPEN:
      case AggluNyelvParser::INTEGER:
      case AggluNyelvParser::SYMBOL_NAME: {
        enterOuterAlt(_localctx, 1);
        setState(359);
        expression(0);
        break;
      }

      case AggluNyelvParser::TEXT: {
        enterOuterAlt(_localctx, 2);
        setState(360);
        text();
        break;
      }

      case AggluNyelvParser::BOOLEAN_TRUE:
      case AggluNyelvParser::BOOLEAN_FALSE: {
        enterOuterAlt(_localctx, 3);
        setState(361);
        boolean();
        break;
      }

      case AggluNyelvParser::OP_DEFAULT_VALUE: {
        enterOuterAlt(_localctx, 4);
        setState(362);
        match(AggluNyelvParser::OP_DEFAULT_VALUE);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BooleanContext ------------------------------------------------------------------

AggluNyelvParser::BooleanContext::BooleanContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::BooleanContext::BOOLEAN_TRUE() {
  return getToken(AggluNyelvParser::BOOLEAN_TRUE, 0);
}

tree::TerminalNode* AggluNyelvParser::BooleanContext::BOOLEAN_FALSE() {
  return getToken(AggluNyelvParser::BOOLEAN_FALSE, 0);
}


size_t AggluNyelvParser::BooleanContext::getRuleIndex() const {
  return AggluNyelvParser::RuleBoolean;
}

void AggluNyelvParser::BooleanContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBoolean(this);
}

void AggluNyelvParser::BooleanContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBoolean(this);
}


std::any AggluNyelvParser::BooleanContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitBoolean(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::BooleanContext* AggluNyelvParser::boolean() {
  BooleanContext *_localctx = _tracker.createInstance<BooleanContext>(_ctx, getState());
  enterRule(_localctx, 68, AggluNyelvParser::RuleBoolean);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(365);
    _la = _input->LA(1);
    if (!(_la == AggluNyelvParser::BOOLEAN_TRUE

    || _la == AggluNyelvParser::BOOLEAN_FALSE)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TextContext ------------------------------------------------------------------

AggluNyelvParser::TextContext::TextContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* AggluNyelvParser::TextContext::TEXT() {
  return getToken(AggluNyelvParser::TEXT, 0);
}


size_t AggluNyelvParser::TextContext::getRuleIndex() const {
  return AggluNyelvParser::RuleText;
}

void AggluNyelvParser::TextContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterText(this);
}

void AggluNyelvParser::TextContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<AggluNyelvListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitText(this);
}


std::any AggluNyelvParser::TextContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<AggluNyelvVisitor*>(visitor))
    return parserVisitor->visitText(this);
  else
    return visitor->visitChildren(this);
}

AggluNyelvParser::TextContext* AggluNyelvParser::text() {
  TextContext *_localctx = _tracker.createInstance<TextContext>(_ctx, getState());
  enterRule(_localctx, 70, AggluNyelvParser::RuleText);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(367);
    match(AggluNyelvParser::TEXT);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool AggluNyelvParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 23: return logicalExpressionSempred(antlrcpp::downCast<LogicalExpressionContext *>(context), predicateIndex);
    case 26: return expressionSempred(antlrcpp::downCast<ExpressionContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool AggluNyelvParser::logicalExpressionSempred(LogicalExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 7);
    case 1: return precpred(_ctx, 6);
    case 2: return precpred(_ctx, 2);

  default:
    break;
  }
  return true;
}

bool AggluNyelvParser::expressionSempred(ExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 3: return precpred(_ctx, 7);
    case 4: return precpred(_ctx, 6);

  default:
    break;
  }
  return true;
}

void AggluNyelvParser::initialize() {
  std::call_once(agglunyelvParserOnceFlag, agglunyelvParserInitialize);
}
