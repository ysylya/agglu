
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1

#pragma once


#include "antlr4-runtime.h"




class  AggluNyelvLexer : public antlr4::Lexer {
public:
  enum {
    COMMENT = 1, BLOCK_OPEN = 2, BLOCK_CLOSE = 3, OP_ASSIGN = 4, OP_LISTING = 5, 
    OP_DEFAULT_VALUE = 6, OP_DOT = 7, OP_ARR_ELEM_SELECTOR_DECOR = 8, OP_COMP_EQUALS = 9, 
    OP_COMP_NOTEQUALS = 10, OP_COMP_LESS_THAN = 11, OP_COMP_LESS_OR_EQUAL_THAN = 12, 
    OP_COMP_GREATER_THAN = 13, OP_COMP_GREATER_OR_EQUAL_THAN = 14, OP_EXPONENTATION = 15, 
    OP_MULTIPLICATION = 16, OP_DIVISION = 17, OP_ADDITION = 18, OP_SUBTRACTION = 19, 
    OP_MODULO = 20, TERMINATOR = 21, PREFIX_STD = 22, TYPE = 23, CONSTNESS = 24, 
    FUN_DECOR = 25, FUN_MAIN_NAME = 26, PARENTHESIS_OPEN = 27, PARENTHESIS_CLOSE = 28, 
    FUN_RETURN = 29, STRUCT_DECLR = 30, LOOPVAR_VALUE_DEF_BEG = 31, LOOPVAR_VALUE_DEF_END = 32, 
    LOOPVAR_VALUE_BEG = 33, LOOPVAR_VALUE_END = 34, LOOPVAR_VALUE_STEP = 35, 
    CONDITIONAL_IF = 36, CONDITIONAL_ELSE = 37, LOGICAL_AND = 38, LOGICAL_OR = 39, 
    LOGICAL_NOT = 40, NEWLINE = 41, WHITESPACE = 42, ESC_QUOTATION_MARK = 43, 
    BOOLEAN_TRUE = 44, BOOLEAN_FALSE = 45, INTEGER = 46, TEXT = 47, SYMBOL_NAME = 48
  };

  explicit AggluNyelvLexer(antlr4::CharStream *input);

  ~AggluNyelvLexer() override;


  std::string getGrammarFileName() const override;

  const std::vector<std::string>& getRuleNames() const override;

  const std::vector<std::string>& getChannelNames() const override;

  const std::vector<std::string>& getModeNames() const override;

  const antlr4::dfa::Vocabulary& getVocabulary() const override;

  antlr4::atn::SerializedATNView getSerializedATN() const override;

  const antlr4::atn::ATN& getATN() const override;

  // By default the static state used to implement the lexer is lazily initialized during the first
  // call to the constructor. You can call this function if you wish to initialize the static state
  // ahead of time.
  static void initialize();

private:

  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

};

