
// Generated from /home/suli/Documents/szakdoga/agglunyelv/AggluNyelv.g4 by ANTLR 4.10.1

#pragma once


#include "antlr4-runtime.h"




class  AggluNyelvParser : public antlr4::Parser {
public:
  enum {
    COMMENT = 1, BLOCK_OPEN = 2, BLOCK_CLOSE = 3, OP_ASSIGN = 4, OP_LISTING = 5, 
    OP_DEFAULT_VALUE = 6, OP_DOT = 7, OP_ARR_ELEM_SELECTOR_DECOR = 8, OP_COMP_EQUALS = 9, 
    OP_COMP_NOTEQUALS = 10, OP_COMP_LESS_THAN = 11, OP_COMP_LESS_OR_EQUAL_THAN = 12, 
    OP_COMP_GREATER_THAN = 13, OP_COMP_GREATER_OR_EQUAL_THAN = 14, OP_EXPONENTATION = 15, 
    OP_MULTIPLICATION = 16, OP_DIVISION = 17, OP_ADDITION = 18, OP_SUBTRACTION = 19, 
    OP_MODULO = 20, TERMINATOR = 21, PREFIX_STD = 22, TYPE = 23, CONSTNESS = 24, 
    FUN_DECOR = 25, FUN_MAIN_NAME = 26, PARENTHESIS_OPEN = 27, PARENTHESIS_CLOSE = 28, 
    FUN_RETURN = 29, STRUCT_DECLR = 30, LOOPVAR_VALUE_DEF_BEG = 31, LOOPVAR_VALUE_DEF_END = 32, 
    LOOPVAR_VALUE_BEG = 33, LOOPVAR_VALUE_END = 34, LOOPVAR_VALUE_STEP = 35, 
    CONDITIONAL_IF = 36, CONDITIONAL_ELSE = 37, LOGICAL_AND = 38, LOGICAL_OR = 39, 
    LOGICAL_NOT = 40, NEWLINE = 41, WHITESPACE = 42, ESC_QUOTATION_MARK = 43, 
    BOOLEAN_TRUE = 44, BOOLEAN_FALSE = 45, INTEGER = 46, TEXT = 47, SYMBOL_NAME = 48
  };

  enum {
    RuleProg = 0, RuleStructDefinition = 1, RuleFunctionArgument = 2, RuleFunctionCall = 3, 
    RuleFunctionDefinitionMain = 4, RuleFunctionDefinition = 5, RuleFunctionParameters = 6, 
    RuleFunctionReturnProperties = 7, RuleFunctionReturn = 8, RuleFunctionBlock = 9, 
    RuleBlock = 10, RuleBlockPart = 11, RuleConditional = 12, RuleConditionalIf = 13, 
    RuleConditionalElif = 14, RuleConditionalElse = 15, RuleLoop = 16, RuleLoopvarDeclaration = 17, 
    RuleLoopvarValueBeg = 18, RuleLoopvarValueEnd = 19, RuleLoopvarValueStep = 20, 
    RuleStatementBody = 21, RuleStatement = 22, RuleLogicalExpression = 23, 
    RuleOpLogicalComparison = 24, RuleOpComparison = 25, RuleExpression = 26, 
    RuleArrayElementAccess = 27, RuleArrayElementAssignment = 28, RuleVariableDefinition = 29, 
    RuleVariableDeclaration = 30, RuleVariableDeclarationProperties = 31, 
    RuleVariableAssignment = 32, RuleVariableValue = 33, RuleBoolean = 34, 
    RuleText = 35
  };

  explicit AggluNyelvParser(antlr4::TokenStream *input);

  AggluNyelvParser(antlr4::TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options);

  ~AggluNyelvParser() override;

  std::string getGrammarFileName() const override;

  const antlr4::atn::ATN& getATN() const override;

  const std::vector<std::string>& getRuleNames() const override;

  const antlr4::dfa::Vocabulary& getVocabulary() const override;

  antlr4::atn::SerializedATNView getSerializedATN() const override;


  class ProgContext;
  class StructDefinitionContext;
  class FunctionArgumentContext;
  class FunctionCallContext;
  class FunctionDefinitionMainContext;
  class FunctionDefinitionContext;
  class FunctionParametersContext;
  class FunctionReturnPropertiesContext;
  class FunctionReturnContext;
  class FunctionBlockContext;
  class BlockContext;
  class BlockPartContext;
  class ConditionalContext;
  class ConditionalIfContext;
  class ConditionalElifContext;
  class ConditionalElseContext;
  class LoopContext;
  class LoopvarDeclarationContext;
  class LoopvarValueBegContext;
  class LoopvarValueEndContext;
  class LoopvarValueStepContext;
  class StatementBodyContext;
  class StatementContext;
  class LogicalExpressionContext;
  class OpLogicalComparisonContext;
  class OpComparisonContext;
  class ExpressionContext;
  class ArrayElementAccessContext;
  class ArrayElementAssignmentContext;
  class VariableDefinitionContext;
  class VariableDeclarationContext;
  class VariableDeclarationPropertiesContext;
  class VariableAssignmentContext;
  class VariableValueContext;
  class BooleanContext;
  class TextContext; 

  class  ProgContext : public antlr4::ParserRuleContext {
  public:
    ProgContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<FunctionDefinitionContext *> functionDefinition();
    FunctionDefinitionContext* functionDefinition(size_t i);
    std::vector<FunctionDefinitionMainContext *> functionDefinitionMain();
    FunctionDefinitionMainContext* functionDefinitionMain(size_t i);
    std::vector<StructDefinitionContext *> structDefinition();
    StructDefinitionContext* structDefinition(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ProgContext* prog();

  class  StructDefinitionContext : public antlr4::ParserRuleContext {
  public:
    StructDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *STRUCT_DECLR();
    antlr4::tree::TerminalNode *BLOCK_OPEN();
    antlr4::tree::TerminalNode *BLOCK_CLOSE();
    std::vector<VariableDefinitionContext *> variableDefinition();
    VariableDefinitionContext* variableDefinition(size_t i);
    std::vector<antlr4::tree::TerminalNode *> TERMINATOR();
    antlr4::tree::TerminalNode* TERMINATOR(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StructDefinitionContext* structDefinition();

  class  FunctionArgumentContext : public antlr4::ParserRuleContext {
  public:
    FunctionArgumentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    VariableValueContext *variableValue();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionArgumentContext* functionArgument();

  class  FunctionCallContext : public antlr4::ParserRuleContext {
  public:
    FunctionCallContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *FUN_DECOR();
    antlr4::tree::TerminalNode *PARENTHESIS_OPEN();
    antlr4::tree::TerminalNode *PARENTHESIS_CLOSE();
    antlr4::tree::TerminalNode *PREFIX_STD();
    std::vector<FunctionArgumentContext *> functionArgument();
    FunctionArgumentContext* functionArgument(size_t i);
    std::vector<antlr4::tree::TerminalNode *> OP_LISTING();
    antlr4::tree::TerminalNode* OP_LISTING(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionCallContext* functionCall();

  class  FunctionDefinitionMainContext : public antlr4::ParserRuleContext {
  public:
    FunctionDefinitionMainContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *FUN_MAIN_NAME();
    antlr4::tree::TerminalNode *FUN_DECOR();
    antlr4::tree::TerminalNode *PARENTHESIS_OPEN();
    antlr4::tree::TerminalNode *PARENTHESIS_CLOSE();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    FunctionBlockContext *functionBlock();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionDefinitionMainContext* functionDefinitionMain();

  class  FunctionDefinitionContext : public antlr4::ParserRuleContext {
  public:
    FunctionDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *FUN_DECOR();
    antlr4::tree::TerminalNode *PARENTHESIS_OPEN();
    FunctionParametersContext *functionParameters();
    antlr4::tree::TerminalNode *PARENTHESIS_CLOSE();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    FunctionBlockContext *functionBlock();
    antlr4::tree::TerminalNode *BLOCK_OPEN();
    FunctionReturnPropertiesContext *functionReturnProperties();
    antlr4::tree::TerminalNode *BLOCK_CLOSE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionDefinitionContext* functionDefinition();

  class  FunctionParametersContext : public antlr4::ParserRuleContext {
  public:
    FunctionParametersContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<VariableDeclarationContext *> variableDeclaration();
    VariableDeclarationContext* variableDeclaration(size_t i);
    std::vector<antlr4::tree::TerminalNode *> OP_LISTING();
    antlr4::tree::TerminalNode* OP_LISTING(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionParametersContext* functionParameters();

  class  FunctionReturnPropertiesContext : public antlr4::ParserRuleContext {
  public:
    FunctionReturnPropertiesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    VariableDeclarationPropertiesContext *variableDeclarationProperties();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionReturnPropertiesContext* functionReturnProperties();

  class  FunctionReturnContext : public antlr4::ParserRuleContext {
  public:
    FunctionReturnContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *FUN_RETURN();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    antlr4::tree::TerminalNode *TERMINATOR();
    std::vector<VariableValueContext *> variableValue();
    VariableValueContext* variableValue(size_t i);
    std::vector<antlr4::tree::TerminalNode *> OP_LISTING();
    antlr4::tree::TerminalNode* OP_LISTING(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionReturnContext* functionReturn();

  class  FunctionBlockContext : public antlr4::ParserRuleContext {
  public:
    FunctionBlockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *BLOCK_OPEN();
    antlr4::tree::TerminalNode *BLOCK_CLOSE();
    std::vector<BlockPartContext *> blockPart();
    BlockPartContext* blockPart(size_t i);
    FunctionReturnContext *functionReturn();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionBlockContext* functionBlock();

  class  BlockContext : public antlr4::ParserRuleContext {
  public:
    BlockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *BLOCK_OPEN();
    antlr4::tree::TerminalNode *BLOCK_CLOSE();
    std::vector<BlockPartContext *> blockPart();
    BlockPartContext* blockPart(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlockContext* block();

  class  BlockPartContext : public antlr4::ParserRuleContext {
  public:
    BlockPartContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    StatementContext *statement();
    BlockContext *block();
    ConditionalContext *conditional();
    LoopContext *loop();
    FunctionCallContext *functionCall();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlockPartContext* blockPart();

  class  ConditionalContext : public antlr4::ParserRuleContext {
  public:
    ConditionalContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ConditionalIfContext *conditionalIf();
    std::vector<ConditionalElifContext *> conditionalElif();
    ConditionalElifContext* conditionalElif(size_t i);
    ConditionalElseContext *conditionalElse();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConditionalContext* conditional();

  class  ConditionalIfContext : public antlr4::ParserRuleContext {
  public:
    ConditionalIfContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CONDITIONAL_IF();
    LogicalExpressionContext *logicalExpression();
    BlockContext *block();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConditionalIfContext* conditionalIf();

  class  ConditionalElifContext : public antlr4::ParserRuleContext {
  public:
    ConditionalElifContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CONDITIONAL_ELSE();
    antlr4::tree::TerminalNode *OP_LISTING();
    ConditionalIfContext *conditionalIf();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConditionalElifContext* conditionalElif();

  class  ConditionalElseContext : public antlr4::ParserRuleContext {
  public:
    ConditionalElseContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CONDITIONAL_ELSE();
    BlockContext *block();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConditionalElseContext* conditionalElse();

  class  LoopContext : public antlr4::ParserRuleContext {
  public:
    LoopContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LoopvarDeclarationContext *loopvarDeclaration();
    LoopvarValueBegContext *loopvarValueBeg();
    LoopvarValueEndContext *loopvarValueEnd();
    LoopvarValueStepContext *loopvarValueStep();
    BlockContext *block();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LoopContext* loop();

  class  LoopvarDeclarationContext : public antlr4::ParserRuleContext {
  public:
    LoopvarDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LOOPVAR_VALUE_DEF_BEG();
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *LOOPVAR_VALUE_DEF_END();
    antlr4::tree::TerminalNode *BLOCK_OPEN();
    VariableDeclarationPropertiesContext *variableDeclarationProperties();
    antlr4::tree::TerminalNode *BLOCK_CLOSE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LoopvarDeclarationContext* loopvarDeclaration();

  class  LoopvarValueBegContext : public antlr4::ParserRuleContext {
  public:
    LoopvarValueBegContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *LOOPVAR_VALUE_BEG();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LoopvarValueBegContext* loopvarValueBeg();

  class  LoopvarValueEndContext : public antlr4::ParserRuleContext {
  public:
    LoopvarValueEndContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *LOOPVAR_VALUE_END();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LoopvarValueEndContext* loopvarValueEnd();

  class  LoopvarValueStepContext : public antlr4::ParserRuleContext {
  public:
    LoopvarValueStepContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *LOOPVAR_VALUE_STEP();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LoopvarValueStepContext* loopvarValueStep();

  class  StatementBodyContext : public antlr4::ParserRuleContext {
  public:
    StatementBodyContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExpressionContext *expression();
    LogicalExpressionContext *logicalExpression();
    VariableAssignmentContext *variableAssignment();
    VariableDefinitionContext *variableDefinition();
    ArrayElementAssignmentContext *arrayElementAssignment();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatementBodyContext* statementBody();

  class  StatementContext : public antlr4::ParserRuleContext {
  public:
    StatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    StatementBodyContext *statementBody();
    antlr4::tree::TerminalNode *TERMINATOR();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatementContext* statement();

  class  LogicalExpressionContext : public antlr4::ParserRuleContext {
  public:
    LogicalExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    LogicalExpressionContext() = default;
    void copyFrom(LogicalExpressionContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  LogicExprNotContext : public LogicalExpressionContext {
  public:
    LogicExprNotContext(LogicalExpressionContext *ctx);

    antlr4::tree::TerminalNode *LOGICAL_NOT();
    LogicalExpressionContext *logicalExpression();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprBooleanContext : public LogicalExpressionContext {
  public:
    LogicExprBooleanContext(LogicalExpressionContext *ctx);

    BooleanContext *boolean();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprCompExpressionsContext : public LogicalExpressionContext {
  public:
    LogicExprCompExpressionsContext(LogicalExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    OpComparisonContext *opComparison();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprCompareContext : public LogicalExpressionContext {
  public:
    LogicExprCompareContext(LogicalExpressionContext *ctx);

    std::vector<LogicalExpressionContext *> logicalExpression();
    LogicalExpressionContext* logicalExpression(size_t i);
    OpLogicalComparisonContext *opLogicalComparison();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprAndContext : public LogicalExpressionContext {
  public:
    LogicExprAndContext(LogicalExpressionContext *ctx);

    std::vector<LogicalExpressionContext *> logicalExpression();
    LogicalExpressionContext* logicalExpression(size_t i);
    antlr4::tree::TerminalNode *LOGICAL_AND();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprInParenthesisContext : public LogicalExpressionContext {
  public:
    LogicExprInParenthesisContext(LogicalExpressionContext *ctx);

    antlr4::tree::TerminalNode *PARENTHESIS_OPEN();
    LogicalExpressionContext *logicalExpression();
    antlr4::tree::TerminalNode *PARENTHESIS_CLOSE();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprOrContext : public LogicalExpressionContext {
  public:
    LogicExprOrContext(LogicalExpressionContext *ctx);

    std::vector<LogicalExpressionContext *> logicalExpression();
    LogicalExpressionContext* logicalExpression(size_t i);
    antlr4::tree::TerminalNode *LOGICAL_OR();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LogicExprFunContext : public LogicalExpressionContext {
  public:
    LogicExprFunContext(LogicalExpressionContext *ctx);

    FunctionCallContext *functionCall();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  LogicalExpressionContext* logicalExpression();
  LogicalExpressionContext* logicalExpression(int precedence);
  class  OpLogicalComparisonContext : public antlr4::ParserRuleContext {
  public:
    OpLogicalComparisonContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *OP_COMP_EQUALS();
    antlr4::tree::TerminalNode *OP_COMP_NOTEQUALS();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  OpLogicalComparisonContext* opLogicalComparison();

  class  OpComparisonContext : public antlr4::ParserRuleContext {
  public:
    OpComparisonContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *OP_COMP_EQUALS();
    antlr4::tree::TerminalNode *OP_COMP_NOTEQUALS();
    antlr4::tree::TerminalNode *OP_COMP_LESS_THAN();
    antlr4::tree::TerminalNode *OP_COMP_LESS_OR_EQUAL_THAN();
    antlr4::tree::TerminalNode *OP_COMP_GREATER_THAN();
    antlr4::tree::TerminalNode *OP_COMP_GREATER_OR_EQUAL_THAN();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  OpComparisonContext* opComparison();

  class  ExpressionContext : public antlr4::ParserRuleContext {
  public:
    ExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ExpressionContext() = default;
    void copyFrom(ExpressionContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ExprGrpIntContext : public ExpressionContext {
  public:
    ExprGrpIntContext(ExpressionContext *ctx);

    antlr4::tree::TerminalNode *INTEGER();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpFunContext : public ExpressionContext {
  public:
    ExprGrpFunContext(ExpressionContext *ctx);

    FunctionCallContext *functionCall();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpNegContext : public ExpressionContext {
  public:
    ExprGrpNegContext(ExpressionContext *ctx);

    antlr4::tree::TerminalNode *OP_SUBTRACTION();
    ExpressionContext *expression();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpArrContext : public ExpressionContext {
  public:
    ExprGrpArrContext(ExpressionContext *ctx);

    ArrayElementAccessContext *arrayElementAccess();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpParContext : public ExpressionContext {
  public:
    ExprGrpParContext(ExpressionContext *ctx);

    antlr4::tree::TerminalNode *PARENTHESIS_OPEN();
    ExpressionContext *expression();
    antlr4::tree::TerminalNode *PARENTHESIS_CLOSE();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpAddContext : public ExpressionContext {
  public:
    ExprGrpAddContext(ExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_ADDITION();
    antlr4::tree::TerminalNode *OP_SUBTRACTION();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpSymContext : public ExpressionContext {
  public:
    ExprGrpSymContext(ExpressionContext *ctx);

    antlr4::tree::TerminalNode *SYMBOL_NAME();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGrpMulContext : public ExpressionContext {
  public:
    ExprGrpMulContext(ExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);
    antlr4::tree::TerminalNode *OP_MULTIPLICATION();
    antlr4::tree::TerminalNode *OP_DIVISION();
    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ExpressionContext* expression();
  ExpressionContext* expression(int precedence);
  class  ArrayElementAccessContext : public antlr4::ParserRuleContext {
  public:
    ArrayElementAccessContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *INTEGER();
    antlr4::tree::TerminalNode *OP_DOT();
    antlr4::tree::TerminalNode *OP_ARR_ELEM_SELECTOR_DECOR();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ArrayElementAccessContext* arrayElementAccess();

  class  ArrayElementAssignmentContext : public antlr4::ParserRuleContext {
  public:
    ArrayElementAssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ArrayElementAccessContext *arrayElementAccess();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    VariableValueContext *variableValue();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ArrayElementAssignmentContext* arrayElementAssignment();

  class  VariableDefinitionContext : public antlr4::ParserRuleContext {
  public:
    VariableDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    VariableDeclarationContext *variableDeclaration();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    std::vector<VariableValueContext *> variableValue();
    VariableValueContext* variableValue(size_t i);
    std::vector<antlr4::tree::TerminalNode *> OP_LISTING();
    antlr4::tree::TerminalNode* OP_LISTING(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableDefinitionContext* variableDefinition();

  class  VariableDeclarationContext : public antlr4::ParserRuleContext {
  public:
    VariableDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *BLOCK_OPEN();
    VariableDeclarationPropertiesContext *variableDeclarationProperties();
    antlr4::tree::TerminalNode *BLOCK_CLOSE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableDeclarationContext* variableDeclaration();

  class  VariableDeclarationPropertiesContext : public antlr4::ParserRuleContext {
  public:
    VariableDeclarationPropertiesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *OP_LISTING();
    antlr4::tree::TerminalNode *CONSTNESS();
    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *INTEGER();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableDeclarationPropertiesContext* variableDeclarationProperties();

  class  VariableAssignmentContext : public antlr4::ParserRuleContext {
  public:
    VariableAssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SYMBOL_NAME();
    antlr4::tree::TerminalNode *OP_ASSIGN();
    std::vector<VariableValueContext *> variableValue();
    VariableValueContext* variableValue(size_t i);
    std::vector<antlr4::tree::TerminalNode *> OP_LISTING();
    antlr4::tree::TerminalNode* OP_LISTING(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableAssignmentContext* variableAssignment();

  class  VariableValueContext : public antlr4::ParserRuleContext {
  public:
    VariableValueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExpressionContext *expression();
    TextContext *text();
    BooleanContext *boolean();
    antlr4::tree::TerminalNode *OP_DEFAULT_VALUE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableValueContext* variableValue();

  class  BooleanContext : public antlr4::ParserRuleContext {
  public:
    BooleanContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *BOOLEAN_TRUE();
    antlr4::tree::TerminalNode *BOOLEAN_FALSE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BooleanContext* boolean();

  class  TextContext : public antlr4::ParserRuleContext {
  public:
    TextContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *TEXT();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TextContext* text();


  bool sempred(antlr4::RuleContext *_localctx, size_t ruleIndex, size_t predicateIndex) override;

  bool logicalExpressionSempred(LogicalExpressionContext *_localctx, size_t predicateIndex);
  bool expressionSempred(ExpressionContext *_localctx, size_t predicateIndex);

  // By default the static state used to implement the parser is lazily initialized during the first
  // call to the constructor. You can call this function if you wish to initialize the static state
  // ahead of time.
  static void initialize();

private:
};

