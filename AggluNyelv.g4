grammar AggluNyelv;

prog:
    (
          functionDefinition
        | functionDefinitionMain
        | structDefinition
    )*
;

structDefinition:
    SYMBOL_NAME
    STRUCT_DECLR
    BLOCK_OPEN (variableDefinition TERMINATOR)* BLOCK_CLOSE
;

functionArgument: SYMBOL_NAME | variableValue;

functionCall:
    PREFIX_STD? SYMBOL_NAME
    FUN_DECOR
    PARENTHESIS_OPEN (functionArgument OP_LISTING)* functionArgument? PARENTHESIS_CLOSE
;

functionDefinitionMain:
    FUN_MAIN_NAME
    FUN_DECOR
    PARENTHESIS_OPEN PARENTHESIS_CLOSE
    OP_ASSIGN
    functionBlock
;

functionDefinition:
    SYMBOL_NAME
    FUN_DECOR
    PARENTHESIS_OPEN functionParameters PARENTHESIS_CLOSE
    (BLOCK_OPEN functionReturnProperties BLOCK_CLOSE)?
    OP_ASSIGN
    functionBlock
;

functionParameters:
    (variableDeclaration OP_LISTING)* (variableDeclaration)?
;

functionReturnProperties:
    (variableDeclarationProperties)?
;

functionReturn: FUN_RETURN OP_ASSIGN (variableValue OP_LISTING)* variableValue+ TERMINATOR;

functionBlock: BLOCK_OPEN (blockPart)* functionReturn? BLOCK_CLOSE;

block: BLOCK_OPEN (blockPart)* BLOCK_CLOSE;

blockPart: statement | block | conditional | loop | functionCall;

conditional:
    conditionalIf
    conditionalElif*
    conditionalElse?
;

conditionalIf: CONDITIONAL_IF logicalExpression block;

conditionalElif: CONDITIONAL_ELSE OP_LISTING conditionalIf;

conditionalElse: CONDITIONAL_ELSE block;

loop:
    loopvarDeclaration
    loopvarValueBeg
    loopvarValueEnd
    loopvarValueStep
    block
;

loopvarDeclaration:
    LOOPVAR_VALUE_DEF_BEG SYMBOL_NAME LOOPVAR_VALUE_DEF_END
    BLOCK_OPEN
        variableDeclarationProperties
    BLOCK_CLOSE
;
loopvarValueBeg:  expression LOOPVAR_VALUE_BEG;
loopvarValueEnd:  expression LOOPVAR_VALUE_END;
loopvarValueStep: expression LOOPVAR_VALUE_STEP;

statementBody:
      expression
    | logicalExpression
    | variableAssignment
    | variableDefinition
    | arrayElementAssignment
;

statement: statementBody TERMINATOR;

logicalExpression:
      LOGICAL_NOT logicalExpression                            # logicExprNot
    | logicalExpression LOGICAL_OR logicalExpression           # logicExprOr
    | logicalExpression LOGICAL_AND logicalExpression          # logicExprAnd
    | boolean                                                  # logicExprBoolean
    | functionCall                                             # logicExprFun
    | expression opComparison expression                       # logicExprCompExpressions
    | logicalExpression opLogicalComparison logicalExpression  # logicExprCompare
    | PARENTHESIS_OPEN logicalExpression PARENTHESIS_CLOSE     # logicExprInParenthesis
;

opLogicalComparison:
      OP_COMP_EQUALS
    | OP_COMP_NOTEQUALS
;

opComparison:
      OP_COMP_EQUALS
    | OP_COMP_NOTEQUALS
    | OP_COMP_LESS_THAN
    | OP_COMP_LESS_OR_EQUAL_THAN
    | OP_COMP_GREATER_THAN
    | OP_COMP_GREATER_OR_EQUAL_THAN
;

expression:
//      <assoc=right> expression OP_EXPONENTATION expression
      OP_SUBTRACTION expression                               # exprGrpNeg
    | expression (OP_MULTIPLICATION | OP_DIVISION) expression # exprGrpMul
    | expression (OP_ADDITION | OP_SUBTRACTION) expression    # exprGrpAdd
    | INTEGER                                                 # exprGrpInt
    | SYMBOL_NAME                                             # exprGrpSym
    | arrayElementAccess                                      # exprGrpArr
    | functionCall                                            # exprGrpFun
    | PARENTHESIS_OPEN expression PARENTHESIS_CLOSE           # exprGrpPar
;

arrayElementAccess: SYMBOL_NAME INTEGER OP_DOT OP_ARR_ELEM_SELECTOR_DECOR;

arrayElementAssignment: arrayElementAccess OP_ASSIGN variableValue;

variableDefinition:
    variableDeclaration
    OP_ASSIGN
    (variableValue OP_LISTING)* variableValue+
;

variableDeclaration:
    SYMBOL_NAME
    BLOCK_OPEN
        variableDeclarationProperties
    BLOCK_CLOSE
;

variableDeclarationProperties:
    (INTEGER? (TYPE | SYMBOL_NAME))
    OP_LISTING
    (CONSTNESS)
;

variableAssignment:
    SYMBOL_NAME
    OP_ASSIGN
    (variableValue OP_LISTING)* variableValue+
;

variableValue: expression | text | boolean | OP_DEFAULT_VALUE;

boolean: BOOLEAN_TRUE | BOOLEAN_FALSE;

text: TEXT;


COMMENT: '#' ~'\n'* -> skip;

BLOCK_OPEN:  '{';
BLOCK_CLOSE: '}';

OP_ASSIGN:  ':=';
OP_LISTING: ',';
OP_DEFAULT_VALUE: '...';

OP_DOT: '.';

OP_ARR_ELEM_SELECTOR_DECOR: 'eleme';

OP_COMP_EQUALS:                '==';
OP_COMP_NOTEQUALS:             '!=';
OP_COMP_LESS_THAN:             '<';
OP_COMP_LESS_OR_EQUAL_THAN:    '<=';
OP_COMP_GREATER_THAN:          '>';
OP_COMP_GREATER_OR_EQUAL_THAN: '>=';

OP_EXPONENTATION:  '^';
OP_MULTIPLICATION: '*';
OP_DIVISION:       '/';
OP_ADDITION:       '+';
OP_SUBTRACTION:    '-';
OP_MODULO:         '%';

TERMINATOR: ';';

PREFIX_STD: 'alap.';
TYPE: 'egész' | 'szöveg' | 'igazhamis';
CONSTNESS: 'változó' | 'állandó';

FUN_DECOR: '!';
FUN_MAIN_NAME: 'kezdjük';
PARENTHESIS_OPEN:  '(';
PARENTHESIS_CLOSE: ')';
FUN_RETURN: 'eredmény';

STRUCT_DECLR: 'tervrajza';

LOOPVAR_VALUE_DEF_BEG: 'minden';
LOOPVAR_VALUE_DEF_END: '-ra' | '-re';
LOOPVAR_VALUE_BEG: '-tól' | '-től';
LOOPVAR_VALUE_END: '-ig';
LOOPVAR_VALUE_STEP: '-' VOWEL_SMALL 's' VOWEL_SMALL ('val' | 'vel');

CONDITIONAL_IF: 'ha';
CONDITIONAL_ELSE: 'amúgy';

LOGICAL_AND: 'és';
LOGICAL_OR:  'vagy';
LOGICAL_NOT: 'nem';

NEWLINE: [\r\n]+ -> channel(HIDDEN);
WHITESPACE: ' ' -> channel(HIDDEN);
ESC_QUOTATION_MARK: '\\"';

BOOLEAN_TRUE:  'igaz';
BOOLEAN_FALSE: 'hamis';
INTEGER: [0-9]+;
TEXT: '"' (ESC_QUOTATION_MARK | .)*?  '"';
SYMBOL_NAME: [a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ_]+[a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ_0-9]*;

fragment VOWEL_SMALL: [aáeéiíoóöőuúüű];
//SZO: [a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ]+ ;
//SZO                : ~[\])]+ ;
