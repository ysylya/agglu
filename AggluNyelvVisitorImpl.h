#ifndef AGGLUNYELV_AGGLUNYELVVISITORIMPL_H
#define AGGLUNYELV_AGGLUNYELVVISITORIMPL_H

#include "AggluNyelvBaseVisitor.h"
#include "SymbolTable.h"
#include "CodeGenerator.h"
#include "common.h"

#include <iostream>

namespace agglunyelv
{
    enum class Phase
    {
        CONSTANT_FOLDING,
        FUN_COLLECTING,
        COMPILING,
    };
    
    struct ExpressionResult
    {
        std::string result;
    
        // results are only valid if bad_bit == false
        bool bad_bit { false };
    };
    
    class AggluNyelvVisitorImpl : public AggluNyelvBaseVisitor
    {
    public:
        virtual std::any visitProg(AggluNyelvParser::ProgContext * ctx) override;
    
    protected:
        SymbolTable symbol_table;

    };
    
} // agglunyelv

#endif //AGGLUNYELV_AGGLUNYELVVISITORIMPL_H
