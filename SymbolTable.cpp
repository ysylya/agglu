#include "SymbolTable.h"
#include "common.h"

#include <iostream>

namespace agglunyelv
{
    int Scope::size() const
    {
        int count { 0 };
        
        for (const Symbol & symbol : m_stack)
        {
            count += symbol.arr_size;
        }
        
        return count;
    }
    
    bool Scope::push(const Symbol & symbol)
    {
        bool success { false };
        
        // symbol.name.empty() is valid for placeholder symbols
        // TODO maybe nullptr if someone wants to find these placeholders
        if (symbol.name.empty() || findByName(symbol.name) == nullptr)
        {
            m_stack.push_back(symbol);
            success = true;
        }
        
        return success;
    }
    
//    bool Scope::find(const Symbol & symbol)
//    {
//        bool found { false };
//
//        for (const Symbol & m_symbol : m_stack_scopes)
//        {
//            if (m_symbol.name == symbol.name)
//            {
//                found = true;
//            }
//        }
//
//        return found;
//    }
    
    Symbol * Scope::findByName(const std::string & name)
    {
        Symbol * found { nullptr };
        
        for (Symbol & m_symbol : m_stack)
        {
            if (m_symbol.name == name)
            {
                found = &m_symbol;
            }
        }
    
        return found;
    }
    
    void Scope::pop()
    {
        m_stack.pop_back();
    }
    
    const Symbol * Scope::top() const
    {
        return m_stack.empty() ? nullptr : &m_stack.back();
    }
    
    Symbol * Scope::top()
    {
        return m_stack.empty() ? nullptr : &m_stack.back();
    }
    
    int Scope::getOffsetInCurrentScope(const Symbol * const symbol, const int idx_agglu) const
    {
        int result { -1 };
        
        int count { 0 };
        for (size_t i { 0 }; i < m_stack.size(); ++i)
        {
            if (symbol == &m_stack[i])
            {
                result = count + idx_agglu - 1;
                break;
            }
            
            count += m_stack[i].arr_size;
        }
        
        return result;
    }
    
    void Scope::print(const size_t indentLevel) const
    {
        constexpr size_t indentSize { 4 };
        std::string indent(indentLevel * indentSize, ' ');
        
        std::cout << indent << "--------------------------\n";
        std::cout << indent << "Scope (" << (m_reachable ? "reachable" : "unreachable") << "):\n";
        for (const Symbol & m_symbol : m_stack)
        {
            std::cout << indent << "--------------------------\n";
            std::cout << indent << "Name:    " << m_symbol.name << "\n";
            std::cout << indent << "Type:    " << m_symbol.type << "\n";
            std::cout << indent << "Mutable: " << (m_symbol.is_mutable ? "true" : "false") << "\n";
            std::cout << indent << "Arr #:   " << m_symbol.arr_size << "\n";
            std::cout << indent << "Param #: " << m_symbol.num_of_params << "\n";
        }
        std::cout << indent << "--------------------------\n";
    
        std::cout.flush();
    }
    
    void Scope::setReachableStatus(const bool is_reachable)
    {
        m_reachable = is_reachable;
    }
    
    bool Scope::isReachable() const
    {
        return m_reachable;
    }
    
    SymbolTable::SymbolTable()
    {
        enterScope(); // global scope
    }
    
    void SymbolTable::enterScope()
    {
        m_stack_scopes.emplace_back();
    }
    
    void SymbolTable::exitScope()
    {
        m_stack_scopes.pop_back();
    }
    
    bool SymbolTable::pushToCurrentScope(const Symbol & symbol)
    {
        bool success { false };
        
        if (m_stack_scopes.empty() == false)
        {
            success = m_stack_scopes.back().push(symbol);
        }
        
        return success;
    }
    
    Symbol * SymbolTable::findByName(const std::string & name)
    {
        Symbol * found { nullptr };
        
        for (auto it { m_stack_scopes.rbegin() }; it != m_stack_scopes.rend(); ++it )
        {
            found = it->findByName(name);
            if (found != nullptr)
            {
                break;
            }
        }
        
        return found;
    }
    
    Symbol * SymbolTable::findByNameInCurrentScope(const std::string & name)
    {
        return m_stack_scopes.empty() ? nullptr : m_stack_scopes.back().findByName(name);
    }
    
    const Symbol * SymbolTable::top() const
    {
        const Symbol * symbol { nullptr };
        for (auto it { m_stack_scopes.rbegin() }; it != m_stack_scopes.rend(); ++it )
        {
            symbol = it->top();
            if (symbol != nullptr)
            {
                break;
            }
        }

        return symbol;
    }
    
    Symbol * SymbolTable::top()
    {
        Symbol * symbol { nullptr };
        for (auto it { m_stack_scopes.rbegin() }; it != m_stack_scopes.rend(); ++it )
        {
            symbol = it->top();
            if (symbol != nullptr)
            {
                break;
            }
        }
    
        return symbol;
    }
    
    int SymbolTable::getOffsetInCurrentScope(const Symbol * const symbol) const
    {
        return m_stack_scopes.empty() ? -1 : m_stack_scopes.back().getOffsetInCurrentScope(symbol);
    }
    
    int SymbolTable::getOffset(const Symbol * const symbol, const int idx_agglu) const
    {
        if (m_stack_scopes.empty())
        {
            return -1;
        }
        
        int offset { -1 };
        
        for (size_t i { m_stack_scopes.size() - 1 }; i <= m_stack_scopes.size() - 1; --i)
        {
            if (m_stack_scopes[i].isReachable() == false)
            {
                break;
            }
            
            int offset_current_scope { m_stack_scopes[i].getOffsetInCurrentScope(symbol, idx_agglu) };
            
            // found it
            if (offset_current_scope != -1)
            {
                offset = offset_current_scope;
                // offsets from prev scopes
                for (size_t j { i - 1 }; j < i; --j)
                {
                    if (m_stack_scopes[j].isReachable())
                    {
                        offset += m_stack_scopes[j].size();
                    }
                    else
                    {
                        break;
                    }
                }
                
                break;
            }
        }
//        print();
//        std::cout << symbol->name << " " << offset << std::endl;
        return offset;
    }
    
    void SymbolTable::print() const
    {
        std::cout << "----------------------------------------------------\n";
        std::cout << "Symbol table:\n";
        for (size_t i { 0 }; i < m_stack_scopes.size(); ++i)
        {
            const Scope & scope {m_stack_scopes[i] };
            scope.print(i);
        }
        std::cout << "----------------------------------------------------\n";
    }
    
    const int SymbolTable::getNumOfScopelocals()
    {
        return m_stack_scopes.empty() ? 0 : m_stack_scopes.back().size();
    }
    
    const Symbol * SymbolTable::topFromParentScope()
    {
        Symbol * parent { nullptr };
        
        if (m_stack_scopes.size() >= 2)
        {
            parent = m_stack_scopes[m_stack_scopes.size() - 2].top();
        }
        
        return parent;
    }
    
    void SymbolTable::flagParentScopeUnreachable()
    {
        if (m_stack_scopes.size() < 2)
        {
            return;
        }
        
        m_stack_scopes[m_stack_scopes.size() - 2].setReachableStatus(false);
    }
    
    void SymbolTable::flagParentScopeReachable()
    {
        if (m_stack_scopes.size() < 2)
        {
            return;
        }
    
        m_stack_scopes[m_stack_scopes.size() - 2].setReachableStatus(true);
    }
    
} // agglunyelv