#ifndef AGGLUNYELV_AGGLUNYELVVISITORIMPL_COMPILING_H
#define AGGLUNYELV_AGGLUNYELVVISITORIMPL_COMPILING_H

#include "AggluNyelvVisitorImpl.h"

namespace agglunyelv
{
    
    class AggluNyelvVisitorImpl_Compiling : public AggluNyelvVisitorImpl
    {
    public:
        explicit AggluNyelvVisitorImpl_Compiling(const SymbolTable & symbol_table_every_function);
        
        virtual std::any visitProg(AggluNyelvParser::ProgContext * ctx) override;

        virtual std::any visitVariableDefinition(AggluNyelvParser::VariableDefinitionContext * ctx) override;
        virtual std::any visitVariableDeclaration(AggluNyelvParser::VariableDeclarationContext *ctx) override;
        virtual std::any visitVariableDeclarationProperties(AggluNyelvParser::VariableDeclarationPropertiesContext * ctx) override;
        virtual std::any visitVariableAssignment(AggluNyelvParser::VariableAssignmentContext * ctx) override;
    
        virtual std::any visitVariableValue(AggluNyelvParser::VariableValueContext * ctx) override;
   
        virtual std::any visitArrayElementAccess(AggluNyelvParser::ArrayElementAccessContext * ctx) override;
        virtual std::any visitArrayElementAssignment(AggluNyelvParser::ArrayElementAssignmentContext * ctx) override;
    
        virtual std::any visitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext * ctx) override;
        virtual std::any visitExprGrpMul(AggluNyelvParser::ExprGrpMulContext * ctx) override;
        virtual std::any visitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext * ctx) override;
        virtual std::any visitExprGrpInt(AggluNyelvParser::ExprGrpIntContext * ctx) override;
        virtual std::any visitExprGrpFun(AggluNyelvParser::ExprGrpFunContext * ctx) override;
        virtual std::any visitExprGrpArr(AggluNyelvParser::ExprGrpArrContext * ctx) override;
        virtual std::any visitExprGrpSym(AggluNyelvParser::ExprGrpSymContext * ctx) override;
        virtual std::any visitExprGrpPar(AggluNyelvParser::ExprGrpParContext * ctx) override;
    
        virtual std::any visitFunctionDefinition(AggluNyelvParser::FunctionDefinitionContext * ctx) override;
        virtual std::any visitFunctionDefinitionMain(AggluNyelvParser::FunctionDefinitionMainContext * ctx) override;
        virtual std::any visitFunctionParameters(AggluNyelvParser::FunctionParametersContext * ctx) override;
        virtual std::any visitFunctionReturn(AggluNyelvParser::FunctionReturnContext * ctx) override;
        virtual std::any visitFunctionBlock(AggluNyelvParser::FunctionBlockContext * ctx) override;
    
        virtual std::any visitFunctionArgument(AggluNyelvParser::FunctionArgumentContext * ctx) override;
        virtual std::any visitFunctionCall(AggluNyelvParser::FunctionCallContext * ctx) override;
    
        virtual std::any visitStructDefinition(AggluNyelvParser::StructDefinitionContext * ctx) override;
    
        virtual std::any visitStatement(AggluNyelvParser::StatementContext * ctx) override;
    
        virtual std::any visitBlockPart(AggluNyelvParser::BlockPartContext * ctx) override;
        
        virtual std::any visitBlock(AggluNyelvParser::BlockContext * ctx) override;
    
        virtual std::any visitBoolean(AggluNyelvParser::BooleanContext * ctx) override;
        virtual std::any visitLogicExprCompare(AggluNyelvParser::LogicExprCompareContext * ctx) override;
        virtual std::any visitLogicExprInParenthesis(AggluNyelvParser::LogicExprInParenthesisContext * ctx) override;
        virtual std::any visitLogicExprCompExpressions(AggluNyelvParser::LogicExprCompExpressionsContext * ctx) override;
    
        virtual std::any visitLogicExprNot(AggluNyelvParser::LogicExprNotContext * ctx) override;
        virtual std::any visitLogicExprOr(AggluNyelvParser::LogicExprOrContext * ctx) override;
        virtual std::any visitLogicExprAnd(AggluNyelvParser::LogicExprAndContext * ctx) override;
    
        virtual std::any visitConditional(AggluNyelvParser::ConditionalContext * ctx) override;
        virtual std::any visitConditionalIf(AggluNyelvParser::ConditionalIfContext * ctx) override;
        virtual std::any visitConditionalElif(AggluNyelvParser::ConditionalElifContext * ctx) override;
        virtual std::any visitConditionalElse(AggluNyelvParser::ConditionalElseContext * ctx) override;
    
        virtual std::any visitLoop(AggluNyelvParser::LoopContext * ctx) override;
        virtual std::any visitLoopvarDeclaration(AggluNyelvParser::LoopvarDeclarationContext * ctx) override;
        virtual std::any visitLoopvarValueBeg(AggluNyelvParser::LoopvarValueBegContext * ctx) override;
        virtual std::any visitLoopvarValueEnd(AggluNyelvParser::LoopvarValueEndContext * ctx) override;
        virtual std::any visitLoopvarValueStep(AggluNyelvParser::LoopvarValueStepContext * ctx) override;
        
        virtual std::any visitText(AggluNyelvParser::TextContext * ctx) override;

    private:
        SymbolTable symbol_table_every_function;
        CodeGeneratorTargetMips codegen_mips;
    };
    
} // agglunyelv

#endif //AGGLUNYELV_AGGLUNYELVVISITORIMPL_COMPILING_H
