#include "AggluNyelvVisitorImpl_ConstFolding.h"

namespace agglunyelv
{
    AggluNyelvVisitorImpl_ConstFolding::AggluNyelvVisitorImpl_ConstFolding(antlr4::TokenStream * tokens)
        : rewriter { tokens }
    {
    }
    
    std::any AggluNyelvVisitorImpl_ConstFolding::visitProg(AggluNyelvParser::ProgContext * ctx)
    {
        visitChildren(ctx);

        return rewriter.getText();
    }
    
    std::any AggluNyelvVisitorImpl_ConstFolding::visitExprGrpNeg(AggluNyelvParser::ExprGrpNegContext * ctx)
    {
        try
        {
            const int left{std::any_cast<int>(visit(ctx->expression()))};
            return -left;
        }
        catch (const std::bad_any_cast & exc)
        {
            // ignoring symbols for now
            return defaultResult();
        }
    }
    
    std::any AggluNyelvVisitorImpl_ConstFolding::visitExprGrpMul(AggluNyelvParser::ExprGrpMulContext * ctx)
    {
        try
        {
            int solution;
        
            const int left  { std::any_cast<int>(visit(ctx->expression(0))) };
            const int right { std::any_cast<int>(visit(ctx->expression(1))) };
        
            if (ctx->OP_MULTIPLICATION())
            {
                solution = left * right;
            }
            else if (ctx->OP_DIVISION())
            {
                solution = left / right;
            }
        
            rewriter.replace(ctx->start, ctx->stop, std::to_string(solution));
        
            return solution;
        }
        catch (const std::bad_any_cast & exc)
        {
            // ignoring symbols for now
            return defaultResult();
        }
    }
    
    std::any AggluNyelvVisitorImpl_ConstFolding::visitExprGrpAdd(AggluNyelvParser::ExprGrpAddContext * ctx)
    {
        try
        {
            int solution;
    
            const int left{std::any_cast<int>(visit(ctx->expression(0)))};
            const int right{std::any_cast<int>(visit(ctx->expression(1)))};
    
            if (ctx->OP_ADDITION())
            {
                solution = left + right;
            }
            else if (ctx->OP_SUBTRACTION())
            {
                solution = left - right;
            }
    
            rewriter.replace(ctx->start, ctx->stop, std::to_string(solution));
    
            return solution;
        }
        catch (const std::bad_any_cast & exc)
        {
            // ignoring symbols for now
            return defaultResult();
        }
    }
    
    std::any AggluNyelvVisitorImpl_ConstFolding::visitExprGrpInt(AggluNyelvParser::ExprGrpIntContext * ctx)
    {
        return std::stoi(ctx->INTEGER()->toString());
    }
    
    std::any AggluNyelvVisitorImpl_ConstFolding::visitExprGrpPar(AggluNyelvParser::ExprGrpParContext * ctx)
    {
        return visit(ctx->expression());
    }
    
    
} // agglunyelv