#include "antlr4-runtime.h"

#include "AggluNyelvLexer.h"
#include "AggluNyelvParser.h"
#include "SymbolTable.h"
#include "AggluNyelvVisitorImpl_ConstFolding.h"
#include "AggluNyelvVisitorImpl_FunCollecting.h"
#include "AggluNyelvVisitorImpl_Compiling.h"

#include <string>


std::string genConstFoldedAggluCode(const std::string & agglu_code);
agglunyelv::SymbolTable genSymbolTableWithEveryFunction(const std::string & agglu_code);
std::string genCompiledCode(const std::string & agglu_code, const agglunyelv::SymbolTable & symbols_every_function);

int main(int argc, char * argv[])
{
    // PHASE 1 - const folding
    
    std::string phase_const_folding { genConstFoldedAggluCode(argv[1]) };
//    log_line(phase_const_folding);

    // PHASE 2 - function collecting

    agglunyelv::SymbolTable symbols_every_function { genSymbolTableWithEveryFunction(phase_const_folding) };
//    symbols_every_function.print();
    
    // PHASE 3 - compiling
    
    std::string result { genCompiledCode(phase_const_folding, symbols_every_function) };
//    log_line(result);
    
    return 0;
}


class InputStreamProcessor
{
public:
    explicit InputStreamProcessor(std::string_view input)
            : input_stream { input }
            , input_stream_processor { input_stream }
    {
    }
    
    explicit InputStreamProcessor(std::istream & stream)
            : input_stream { stream }
            , input_stream_processor { input_stream }
    {
    }
    
    antlr4::tree::ParseTree * getTree()
    {
        return input_stream_processor.getTree();
    }
    
    antlr4::CommonTokenStream & getTokens()
    {
        return input_stream_processor.getTokens();
    }

private:
    class ANTLRInputStreamProcessor
    {
    public:
        explicit ANTLRInputStreamProcessor(antlr4::ANTLRInputStream & input)
            : lexer  { &input }
            , tokens { &lexer }
            , parser { &tokens }
        {
        }
        
        antlr4::tree::ParseTree * getTree()
        {
            return parser.prog();
        }
        
        antlr4::CommonTokenStream & getTokens()
        {
            return tokens;
        }
    
    private:
        AggluNyelvLexer lexer;
        antlr4::CommonTokenStream tokens;
        AggluNyelvParser parser;
    };
    
    antlr4::ANTLRInputStream input_stream;
    ANTLRInputStreamProcessor input_stream_processor;
};

std::string genConstFoldedAggluCode(const std::string & agglu_code)
{
    std::ifstream ifs { agglu_code };
    InputStreamProcessor isp { ifs };
    agglunyelv::AggluNyelvVisitorImpl_ConstFolding visitor { &isp.getTokens() };
    
    return std::any_cast<std::string>(visitor.visit(isp.getTree()));
}

agglunyelv::SymbolTable genSymbolTableWithEveryFunction(const std::string & agglu_code)
{
    InputStreamProcessor isp { agglu_code };
    agglunyelv::AggluNyelvVisitorImpl_FunCollecting visitor {};
    
    return std::any_cast<agglunyelv::SymbolTable>(visitor.visit(isp.getTree()));
}

std::string genCompiledCode(const std::string & agglu_code, const agglunyelv::SymbolTable & symbols_every_function)
{
    InputStreamProcessor isp { agglu_code };
    agglunyelv::AggluNyelvVisitorImpl_Compiling visitor { symbols_every_function };
    
    return std::any_cast<std::string>(visitor.visit(isp.getTree()));
}
